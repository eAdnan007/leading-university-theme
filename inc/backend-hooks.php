<?php

remove_filter( 'the_content', 'wpautop' );
remove_filter('the_content', 'do_shortcode', 11);
add_filter( 'the_content', 'wpautop' , 99);
add_filter('the_content', 'do_shortcode', 100); 
add_filter( 'the_content', 'shortcode_unautop', 110 );
add_filter( 'jetpack_enable_open_graph', '__return_false' );

/**
 * Add rewrite rule for pretty ics class schedule feed url.
 */
add_action( 'init', function(){
	 add_rewrite_rule('^([0-9a-zA-Z]+)\.ics?', 'wp-admin/admin-ajax.php?action=get-ical-feed&user=$1', 'top');
});

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
add_action( 'widgets_init', function() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'leading-university' ),
		'id'            => 'sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Widgets', 'leading-university' ),
		'id'            => 'footer',
		'description'   => __( 'Add exactly four widgets to display in footer', 'leading-university' ),
		'before_widget' => '<aside id="%1$s" class="widget col-md-3 col-sm-6 %2$s">',
		// 'before_widget' => '<aside id="%1$s" class="widget col-md-12 %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
});

/**
 * Enqueue scripts and styles in the admin area.
 */
add_action( 'admin_enqueue_scripts', function(){
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/css/jquery-ui.min.css' );
	wp_enqueue_style( 'jquery-ui-structure', get_template_directory_uri() . '/css/jquery-ui.structure.min.css', array( 'jquery-ui' ) );
	wp_enqueue_style( 'jquery-ui-theme', get_template_directory_uri() . '/css/jquery-ui.theme.min.css', array( 'jquery-ui-structure' ) );
	
	wp_enqueue_style( 'jquery-timepicker', get_template_directory_uri() . '/css/jquery.timepicker.css', array(), '0.4.8' );
	
	wp_enqueue_style( 'angular-ui-select', get_template_directory_uri() . '/css/select.min.css', array(), '0.12.0' );

    wp_enqueue_style( 'select2', get_stylesheet_directory_uri() . '/css/select2.css' );
	
	wp_enqueue_style( 'leading-university-admin', get_template_directory_uri() . '/css/admin.css' );
	
	
	wp_register_script( 'lu-helper', get_template_directory_uri() . '/js/lu-helper.js', array( 'jquery' ) );
	
	$helper_data = array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
	if( in_array( lu_screen_name(), array( 'edit_syllabus', 'add_syllabus' ) ) ) $helper_data['courses'] = lu_get_syllabus_courses();
	if( in_array( lu_screen_name(), array( 'edit_registration', 'add_registration' ) ) ) $helper_data['change']  = current_user_can( 'change_registration_meta' );
	if( in_array( lu_screen_name(), array( 'edit_program', 'add_program' ) ) ){
		global $post;
		$helper_data['offering'] = lu_get_program_offerings();
		$helper_data['courses']  = lu_get_program_courses();
		$helper_data['rooms']    = lu_get_program_rooms();
		$helper_data['schedule'] = lu_get_program_schedule();
		
		$fees = get_post_meta( $post->ID, '_fees', true );
		$helper_data['settings'] = array(
			'fees' => is_array($fees) ? $fees : array(),
			'registration_open' => get_post_meta( $post->ID, '_registration_open', true ),
		);
		
		$helper_data['teachers'] = array_map( 
			function( $t ){
				$d = get_term( get_user_meta( $t->ID, 'department', true ), 'department' );

				return (object) array(
					'ID' => $t->ID,
					'name' => "$t->first_name $t->last_name",
					'designation' => get_user_meta( $t->ID, 'designation', true ),
					'department' => ( !isset( $d->errors ) || empty($d->errors) ) && null != $d ? 
						array( 'ID' => $d->term_id, 'name' => $d->name ) : 
						array( 'ID' => 0, 'name' => __( 'Undefined Department', 'leading-university' ) )
				);
			},
			get_users( array( 'role' => 'teacher' ) ) );
		
		
	}
	if( lu_screen_name() == 'evaluation_report' ){
	    $helper_data['teachers'] = array_map(
            function( $t ){
                $d = get_term( get_user_meta( $t->ID, 'department', true ), 'department' );

                return (object) array(
                    'ID' => $t->ID,
                    'name' => "$t->first_name $t->last_name",
                    'designation' => get_user_meta( $t->ID, 'designation', true ),
                    'department' => ( !isset( $d->errors ) || empty($d->errors) ) && null != $d ?
                        array( 'ID' => $d->term_id, 'name' => $d->name ) :
                        array( 'ID' => 0, 'name' => __( 'Undefined Department', 'leading-university' ) )
                );
            },
            get_users( array( 'role' => 'teacher' ) ));

	    $helper_data['semesters'] = get_terms( array(
            'taxonomy' => 'semester',
            'hide_empty' => false,
        ) );
    }

	wp_localize_script( 'lu-helper', 'luh', $helper_data );
	wp_register_script( 'angular', get_template_directory_uri() . '/js/angular.min.js', array(), '1.4.0' );
	wp_register_script( 'angular-sanitize', get_template_directory_uri() . '/js/ng-apps/angular-sanitize.min.js', array( 'angular' ), '1.4.0' );
	
	wp_register_script( 'angular-ui-select', get_template_directory_uri() . '/js/ng-apps/select.min.js', array( 'angular' ), '0.12.0' );
	
	wp_enqueue_script( 'jquery-timepicker', get_template_directory_uri() . '/js/jquery.timepicker.min.js', array( 'jquery' ), '0.4.8' );
	wp_enqueue_script( 'timepickerdirective', get_template_directory_uri() . '/js/timepickerdirective.min.js', array( 'jquery-timepicker', 'angular' ) );
	wp_enqueue_script( 'angular-ui-date', get_template_directory_uri() . '/js/date.js', array( 'jquery-ui-datepicker', 'angular' ) );
	
	wp_register_script( 'edit-syllabus', get_template_directory_uri() . '/js/ng-apps/editSyllabus.js', array( 'angular', 'angular-ui-select', 'angular-sanitize', 'lu-helper' ), '1.0.0' );
	wp_register_script( 'edit-program', get_template_directory_uri() . '/js/ng-apps/editProgram.js', array( 'angular', 'angular-ui-select', 'angular-sanitize', 'lu-helper', 'timepickerdirective', 'jquery-ui-accordion', 'angular-ui-date' ), '1.0.0' );
	wp_register_script( 'edit-registration', get_template_directory_uri() . '/js/ng-apps/editRegistration.js', array( 'angular', 'lu-helper' ), '1.0.0' );

	wp_register_script( 'lu-admin', get_template_directory_uri() . '/js/admin.js', array('jquery'));
	
	
	/**
	 * Ng app of registration export.
	 */
	 wp_register_script( 'export-registration', get_template_directory_uri() . '/js/ng-apps/registrationExport.js', array( 'angular', 'angular-sanitize', 'angular-ui-select' ) );
	 wp_localize_script( 'export-registration', 'luh', $helper_data );
});

/**
 * Recommend/Require plugins for the themes to install
 */
add_action( 'tgmpa_register', function() {
	
	/**
	* List of plugins to load.
	*/
	$plugins = array(
		
		// Replace the default wp taxonomy description field with beautiful tinymce editor.
		array(
			'name'      => 'Taxonomy TinyMCE',
			'slug'      => 'taxonomy-tinymce',
			'required'  => false
		)
	);
	
	tgmpa( $plugins );
});

/**
 * Register post types and taxonomies required for the theme.
 */
add_action( 'init', function(){
	$labels = array(
		'name'               => _x( 'News', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'News', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'News', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'News', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'news', 'leading-university' ),
		'add_new_item'       => __( 'Add New News', 'leading-university' ),
		'new_item'           => __( 'New News', 'leading-university' ),
		'edit_item'          => __( 'Edit News', 'leading-university' ),
		'view_item'          => __( 'View News', 'leading-university' ),
		'all_items'          => __( 'All News', 'leading-university' ),
		'search_items'       => __( 'Search News', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent News:', 'leading-university' ),
		'not_found'          => __( 'No news found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No news found in Trash.', 'leading-university' )
	);

	$capabilities = array(
		'edit_post'           => 'edit_news',
		'read_post'           => 'read_news',
		'delete_post'         => 'delete_news',
		'delete_posts'        => 'delete_newses',
		'delete_others_posts' => 'delete_others_newses',
		'edit_posts'          => 'edit_newses',
		'edit_others_posts'   => 'edit_others_newses',
		'publish_posts'       => 'publish_newses',
		'read_private_posts'  => 'read_private_newses'
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'news' ),
		'capabilities'       => $capabilities,
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-media-text',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
		'taxonomies'         => array( 'category', 'post_tag' )
	);

	register_post_type( 'news', $args );
	
	
	$labels = array(
		'name'               => _x( 'Notices', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'Notice', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'Notice', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'Notice', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'notice', 'leading-university' ),
		'add_new_item'       => __( 'Add New Notice', 'leading-university' ),
		'new_item'           => __( 'New Notice', 'leading-university' ),
		'edit_item'          => __( 'Edit Notice', 'leading-university' ),
		'view_item'          => __( 'View Notice', 'leading-university' ),
		'all_items'          => __( 'All Notices', 'leading-university' ),
		'search_items'       => __( 'Search Notices', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent Notice:', 'leading-university' ),
		'not_found'          => __( 'No notice found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No notice found in Trash.', 'leading-university' )
	);

	$capabilities = array(
		'edit_post'           => 'edit_notice',
		'read_post'           => 'read_notice',
		'delete_post'         => 'delete_notice',
		'delete_posts'        => 'delete_notices',
		'delete_others_posts' => 'delete_others_notices',
		'edit_posts'          => 'edit_notices',
		'edit_others_posts'   => 'edit_others_notices',
		'publish_posts'       => 'publish_notices',
		'read_private_posts'  => 'read_private_notices'
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'notice' ),
		'capabilities'       => $capabilities,
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-pressthis',
		'supports'           => array( 'title', 'editor' ),
		'taxonomies'         => array( 'category', 'post_tag' )
	);


	register_post_type( 'notice', $args );
	
	
	$labels = array(
		'name'               => _x( 'Publications', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'Publication', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'Publications', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'Publication', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'notice', 'leading-university' ),
		'add_new_item'       => __( 'Add New Publication', 'leading-university' ),
		'new_item'           => __( 'New Publication', 'leading-university' ),
		'edit_item'          => __( 'Edit Publication', 'leading-university' ),
		'view_item'          => __( 'View Publication', 'leading-university' ),
		'all_items'          => __( 'All Publications', 'leading-university' ),
		'search_items'       => __( 'Search Publications', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent Publication:', 'leading-university' ),
		'not_found'          => __( 'No publication found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No publication found in Trash.', 'leading-university' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'publications' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-analytics',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
		'taxonomies'         => array( 'category', 'post_tag' )
	);

	register_post_type( 'publication', $args );
	
	
	$labels = array(
		'name'               => _x( 'Programs', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'Program', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'Programs', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'Program', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'Program', 'leading-university' ),
		'add_new_item'       => __( 'Add New Program', 'leading-university' ),
		'new_item'           => __( 'New Program', 'leading-university' ),
		'edit_item'          => __( 'Edit Program', 'leading-university' ),
		'view_item'          => __( 'View Program', 'leading-university' ),
		'all_items'          => __( 'All Programs', 'leading-university' ),
		'search_items'       => __( 'Search Programs', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent Program:', 'leading-university' ),
		'not_found'          => __( 'No syllabus found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No syllabus found in Trash.', 'leading-university' )
	);
	
	$capabilities = array(
		'edit_post'           => 'edit_program',
		'read_post'           => 'read_program',
		'delete_post'         => 'delete_program',
		'delete_posts'        => 'delete_programs',
		'delete_others_posts' => 'delete_others_programs',
		'edit_posts'          => 'edit_programs',
		'edit_others_posts'   => 'edit_others_programs',
		'publish_posts'       => 'publish_programs',
		'read_private_posts'  => 'read_private_programs'
	);
	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => false,
		'capabilities'       => $capabilities,
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-welcome-learn-more',
		'supports'           => array( 'title' )
	);

	register_post_type( 'program', $args );
	
	
	$labels = array(
		'name'               => _x( 'Syllabuses', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'Syllabus', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'Syllabuses', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'Syllabus', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'Syllabus', 'leading-university' ),
		'add_new_item'       => __( 'Add New Syllabus', 'leading-university' ),
		'new_item'           => __( 'New Syllabus', 'leading-university' ),
		'edit_item'          => __( 'Edit Syllabus', 'leading-university' ),
		'view_item'          => __( 'View Syllabus', 'leading-university' ),
		'all_items'          => __( 'Syllabuses', 'leading-university' ),
		'search_items'       => __( 'Search Syllabuses', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent Syllabus:', 'leading-university' ),
		'not_found'          => __( 'No syllabus found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No syllabus found in Trash.', 'leading-university' )
	);
	
	$capabilities = array(
		'edit_post'           => 'edit_syllabus',
		'read_post'           => 'read_syllabus',
		'delete_post'         => 'delete_syllabus',
		'delete_posts'        => 'delete_syllabuses',
		'delete_others_posts' => 'delete_others_syllabuses',
		'edit_posts'          => 'edit_syllabuses',
		'edit_others_posts'   => 'edit_others_syllabuses',
		'publish_posts'       => 'publish_syllabuses',
		'read_private_posts'  => 'read_private_syllabuses'
	);
	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php?post_type=program',
		'query_var'          => false,
		'capabilities'       => $capabilities,
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-book-alt',
		'supports'           => array( 'title' )
	);

	register_post_type( 'syllabus', $args );
	
	
	$labels = array(
		'name'               => _x( 'Registrations', 'post type general name', 'leading-university' ),
		'singular_name'      => _x( 'Registration', 'post type singular name', 'leading-university' ),
		'menu_name'          => _x( 'Registrations', 'admin menu', 'leading-university' ),
		'name_admin_bar'     => _x( 'Registration', 'add new on admin bar', 'leading-university' ),
		'add_new'            => _x( 'Add New', 'Registration', 'leading-university' ),
		'add_new_item'       => __( 'Add New Registration', 'leading-university' ),
		'new_item'           => __( 'New Registration', 'leading-university' ),
		'edit_item'          => __( 'Edit Registration', 'leading-university' ),
		'view_item'          => __( 'View Registration', 'leading-university' ),
		'all_items'          => __( 'Registrations', 'leading-university' ),
		'search_items'       => __( 'Search Registrations', 'leading-university' ),
		'parent_item_colon'  => __( 'Parent Registration:', 'leading-university' ),
		'not_found'          => __( 'No registration found.', 'leading-university' ),
		'not_found_in_trash' => __( 'No registration found in Trash.', 'leading-university' )
	);
	
	

	$capabilities = array(
		'edit_post'           => 'edit_registration',
		'read_post'           => 'read_registration',
		'delete_post'         => 'delete_registration',
		'delete_posts'        => 'delete_registrations',
		'delete_others_posts' => 'delete_others_registrations',
		'edit_posts'          => 'edit_registrations',
		'edit_others_posts'   => 'edit_others_registrations',
		'publish_posts'       => 'publish_registrations',
		'read_private_posts'  => 'read_private_registrations'
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => current_user_can( 'edit_programs' ) ? 'edit.php?post_type=program' : true,
		'query_var'          => false,
		'capabilities'       => $capabilities,
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_icon'          => 'dashicons-clipboard',
		'supports'           => array( null ) // Array must contain a value to remove the default fields
	);

	register_post_type( 'registration', $args );
	
	$labels = array(
		'name'              => _x( 'Rooms', 'taxonomy general name', 'leading-university' ),
		'singular_name'     => _x( 'Room', 'taxonomy singular name', 'leading-university' ),
		'search_items'      => __( 'Search Rooms', 'leading-university' ),
		'all_items'         => __( 'All Rooms', 'leading-university' ),
		'parent_item'       => __( 'Parent Room', 'leading-university' ),
		'parent_item_colon' => __( 'Parent Room:', 'leading-university' ),
		'edit_item'         => __( 'Edit Room', 'leading-university' ),
		'update_item'       => __( 'Update Room', 'leading-university' ),
		'add_new_item'      => __( 'Add New Room', 'leading-university' ),
		'new_item_name'     => __( 'New Room Name', 'leading-university' ),
		'menu_name'         => __( 'Rooms', 'leading-university' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'room' ),
	);

	register_taxonomy( 'room', array( 'program' ), $args );
	
	$labels = array(
		'name'              => _x( 'Departments', 'taxonomy general name', 'leading-university' ),
		'singular_name'     => _x( 'Department', 'taxonomy singular name', 'leading-university' ),
		'search_items'      => __( 'Search Departments', 'leading-university' ),
		'all_items'         => __( 'All Departments', 'leading-university' ),
		'parent_item'       => __( 'Parent Department', 'leading-university' ),
		'parent_item_colon' => __( 'Parent Department:', 'leading-university' ),
		'edit_item'         => __( 'Edit Department', 'leading-university' ),
		'update_item'       => __( 'Update Department', 'leading-university' ),
		'add_new_item'      => __( 'Add New Department', 'leading-university' ),
		'new_item_name'     => __( 'New Department Name', 'leading-university' ),
		'menu_name'         => __( 'Departments', 'leading-university' ),
	);

	$args = array(
		'hierarchical'      => false,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'department' ),
	);

	register_taxonomy( 'department', array( 'program' ), $args );
	
	$labels = array(
		'name'              => _x( 'Semesters', 'taxonomy general name', 'leading-university' ),
		'singular_name'     => _x( 'Semester', 'taxonomy singular name', 'leading-university' ),
		'search_items'      => __( 'Search Semesters', 'leading-university' ),
		'all_items'         => __( 'All Semesters', 'leading-university' ),
		'parent_item'       => __( 'Parent Semester', 'leading-university' ),
		'parent_item_colon' => __( 'Parent Semester:', 'leading-university' ),
		'edit_item'         => __( 'Edit Semester', 'leading-university' ),
		'update_item'       => __( 'Update Semester', 'leading-university' ),
		'add_new_item'      => __( 'Add New Semester', 'leading-university' ),
		'new_item_name'     => __( 'New Semester Name', 'leading-university' ),
		'menu_name'         => __( 'Semesters', 'leading-university' ),
	);

	$args = array(
		'hierarchical'      => false,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'semester' ),
	);

	register_taxonomy( 'semester', array( 'program' ), $args );
	
	register_taxonomy_for_object_type( 'department', 'news' );
	register_taxonomy_for_object_type( 'department', 'notice' );
	
	add_post_type_support( 'page', 'excerpt' );
});

/**
 * Register Custom Status
 */
add_action( 'init', function() {

	$args = array(
		'label'                     => _x( 'Submitted', 'Status General Name', 'leading-university' ),
		'label_count'               => _n_noop( 'Submitted (%s)',  'Submitted (%s)', 'leading-university' ), 
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'submitted', $args );

	$args = array(
		'label'                     => _x( 'Under Review', 'Status General Name', 'leading-university' ),
		'label_count'               => _n_noop( 'Review (%s)',  'Review (%s)', 'leading-university' ), 
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'review', $args );

	$args = array(
		'label'                     => _x( 'Approved', 'Status General Name', 'leading-university' ),
		'label_count'               => _n_noop( 'Approved (%s)',  'Approved (%s)', 'leading-university' ), 
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'approved', $args );

	$args = array(
		'label'                     => _x( 'Registered', 'Status General Name', 'leading-university' ),
		'label_count'               => _n_noop( 'Registered (%s)',  'Registered (%s)', 'leading-university' ), 
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'registered', $args );

});

/**
 * Handle the fancy permissions.
 */
add_filter( 'map_meta_cap', function( $caps, $cap, $user_id, $args ) {
	global $dept_based_cap_post_types;
	
	if( isset( $args[0] ) ){
		$post = get_post( $args[0] );
		if( isset( $post->post_type ) && in_array( $post->post_type, $dept_based_cap_post_types ) ){
			$edit   = "edit_$post->post_type";
			$delete = "delete_$post->post_type";
			$read   = "read_$post->post_type";
			
			$post_belongs_to_users_department = lu_post_under_users_dept( $user_id, $post->ID );
		}
		else {
			return $caps;
		}
	}
	else {
		return $caps;
	}

	/* If editing, deleting, or reading a post, get the post and post type object. */
	if ( $edit == $cap || $delete == $cap || $read == $cap ) {
		$post_type = get_post_type_object( $post->post_type );

		/* Set an empty array for the caps. */
		$caps = array();
	}

	/* If editing a post, assign the required capability. */
	if ( $edit == $cap ) {
		if ( ( $post_belongs_to_users_department || ( $user_id == $post->post_author && 'draft' == $post->post_status ) ) && !( !user_can( $user_id, 'confirm_registration' ) && 'registered' == $post->post_status ) )
			$caps[] = $post_type->cap->edit_posts;
		else
			$caps[] = $post_type->cap->edit_others_posts;
	}

	/* If deleting a post, assign the required capability. */
	elseif ( $delete == $cap ) {
		if ( ( $post_belongs_to_users_department || ( $user_id == $post->post_author && 'draft' == $post->post_status ) ) && !( !user_can( $user_id, 'confirm_registration' ) && 'registered' == $post->post_status ) )
			$caps[] = $post_type->cap->delete_posts;
		else
			$caps[] = $post_type->cap->delete_others_posts;
	}

	/* If reading a private post, assign the required capability. */
	elseif ( $read == $cap ) {

		if ( 'private' != $post->post_status )
			$caps[] = 'read';
		elseif ( $post_belongs_to_users_department || ( $user_id == $post->post_author && 'draft' == $post->post_status ) )
			$caps[] = 'read';
		else
			$caps[] = $post_type->cap->read_private_posts;
	}
		
	return $caps;
}, 10, 4 );

/**
 * Assigns all custom capabilities of lu to admin.
 */
add_action( 'admin_init', function(){
	
	$role = get_role( 'administrator' );
	
	$caps = array(
		'view_results',
		'debug_result',
		'change_departments',
		'change_user_designation',
		'change_user_order',
		'change_user_area_of_study',
        'manage_evaluation_questions',
        'view_evaluation_report',

		'delete_programs',
		'delete_others_programs',
		'edit_programs',
		'edit_others_programs',
		'publish_programs',
		'read_private_programs',

		'edit_syllabuses',
		'edit_others_syllabuses',
		'publish_syllabuses',
		'read_private_syllabuses',
		'delete_syllabuses',
		'delete_others_syllabuses',

		'edit_newses',
		'edit_others_newses',
		'publish_newses',
		'read_private_newses',
		'delete_newses',
		'delete_others_newses',

		'edit_notices',
		'edit_others_notices',
		'publish_notices',
		'read_private_notices',
		'delete_notices',
		'delete_others_notices',

		'edit_registrations',
		'edit_others_registrations',
		'publish_registrations',
		'read_private_registrations',
		'delete_registrations',
		'delete_others_registrations',
		'change_registration_meta',
		'approve_registration',
		'confirm_registration',
		'export_registration'
	);
	
	foreach( $caps as $cap )
		$role->add_cap( $cap ); 
});

/**
 * Add ng-app attribute in the form elements.
 */
add_action( 'post_edit_form_tag' , function() {
	$screen_name = lu_screen_name();
	
	switch( $screen_name ){
		case 'add_syllabus':
		case 'edit_syllabus':
			echo ' ng-app="editSyllabusApp"';
			wp_enqueue_script('edit-syllabus');
			break;
		case 'add_program':
		case 'edit_program':
			echo ' ng-app="editProgramApp"';
			wp_enqueue_script('edit-program');
			break;
		case 'add_registration':
		case 'edit_registration':
			echo ' ng-app="editRegistrationApp"';
			echo ' ng-controller="RegistrationCTRL"';
			wp_enqueue_script('edit-registration');
			break;
		
	}
});

/**
 * Remove the department metabox from program to allow only one department per program
 */
add_action( 'admin_menu', function(){
	remove_meta_box( 'tagsdiv-department', 'program', 'normal' );
	remove_meta_box( 'tagsdiv-semester', 'program', 'normal' );
});

/**
 * Prepare the database and user roles when theme is activated
 */
add_action( 'after_switch_theme', function(){
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	
	$sql = "
	CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}courses` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`code` varchar(10) NOT NULL,
		`title` varchar(150) NOT NULL,
		`prerequisites` text,
		`credit` double NOT NULL,
		`syllabus` int(11) NOT NULL,
		`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`)
	) $charset_collate;";
	dbDelta( $sql );	
	
	$sql = "
	CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}program_schedule` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`semester_section` varchar(20) NOT NULL,
		`event_name` varchar(100),
		`course` int(11) NOT NULL,
		`start_time` int(11) NOT NULL,
		`duration` int(11),
		`faculty` int(11),
		`room` int(11) NOT NULL,
		`disable` BOOLEAN NOT NULL,
		`day` varchar(10) NOT NULL,
		`date` date,
		`program` int(11) NOT NULL,
		`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`)
	) $charset_collate;";
	dbDelta( $sql );

	$sql = "
	CREATE TABLE `{$wpdb->prefix}registered_courses` (
        `ID` int(11) NOT NULL AUTO_INCREMENT,
        `registration_id` int(11) NOT NULL,
        `course_id` int(11) NOT NULL,
        `semester_id` int(11) NOT NULL,
        `program_id` int(11) NOT NULL,
        `student_id` int(11) NOT NULL,
        `section` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
        `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
        `status` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`ID`)
    ) $charset_collate;";
	dbDelta( $sql );

	$sql = "CREATE TABLE `{$wpdb->prefix}evaluation_questions` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`name` text NOT NULL,
		`weight` double NOT NULL,
		`type` varchar(50) NOT NULL,
		`parent` int(11) NOT NULL,
		`index` int(11) NOT NULL,
		`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (`ID`)
	) $charset_collate;";
	dbDelta( $sql );

	$sql = "CREATE TABLE `{$wpdb->prefix}evaluation_score` (
        `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `question_id` int(11) DEFAULT NULL,
        `score` double DEFAULT NULL,
        `user_id` int(11) DEFAULT NULL,
        `course_id` int(11) DEFAULT NULL,
        `teacher_id` int(11) DEFAULT NULL,
        `semester_id` int(11) DEFAULT NULL,
        `section` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`ID`)
    ) $charset_collate;";
	dbDelta($sql);

	dbDelta( "ALTER TABLE  `{$wpdb->prefix}program_schedule` ADD  `disable` BOOLEAN NOT NULL AFTER  `room` ;" );
	
	add_role( 'student', __( 'Student', 'leading-university' ), array( 'read' => false ) );
	add_role( 'teacher', __( 'Teacher', 'leading-university' ), array( 
		'view_results' => true,
		'delete_posts' => true,
		'delete_published_posts' => true,
		'edit_posts' => true,
		'edit_published_posts' => true,
		'publish_posts' => true,
		'read' => true,
		'upload_files' => true,
		'view_others_results' => true,
		'change_user_designation' => true,
		'change_user_area_of_study' => true
	) );
	add_role( 'staff', __( 'Staff', 'leading-university' ), array( 'read' => true ) );
});
add_action( 'after_setup_theme', function() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Leading University, use a find and replace
	 * to change 'leading-university' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'leading-university', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'leading-university' ),
		'footer' => __( 'Footer Menu', 'leading-university' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Disable the admin bar for users without read previlege (Students)
	 */
	if (!current_user_can('read')) {
		show_admin_bar(false);
	}
	
	/*
	 * Register image sizes required throughout the template
	 */
	add_image_size( 'single-feat-image', 750, 400, true );
	add_image_size( 'archive-feat-image-lg', 240, 245, true );
	add_image_size( 'archive-feat-image-md', 196, 265, true );
	add_image_size( 'archive-feat-image-xs', 768, 300, true );
	add_image_size( 'event-card-thumb', 735, 460, true );
	add_image_size( 'campus-map-thumb', 285, 140, true );
});

/**
 * Registers a wp widget
 */
function lu_register_widget( $widget_name ){
 	require_once( dirname( __FILE__ ) . "/widgets/$widget_name.php" );
 	register_widget( "{$widget_name}_Widget" );
}

/**
 * Register the custom widgets that are available with the theme.
 */
add_action( 'widgets_init', function(){
	lu_register_widget( 'Spotlight' );
	lu_register_widget( 'NoticeBoard' );
});

/**
 * Echo content for custom user field specific for lu.
 */
function lu_custom_user_fields( $user ) {
	$permissions = array(
		'change_departments',
		'change_user_designation',
		'change_user_order',
		'change_user_area_of_study' );
	
	$p_count = 0;
	foreach( $permissions as $permission ){
		if( !current_user_can( $permission ) ) $p_count++;
	}
	if( $p_count > 0 );
	?>
	<h3><?php _e( 'Position in University', 'leading-university' ); ?></h3>
	
	<table class="form-table">
		<tbody>
			<?php if( current_user_can( $permissions[0] ) ): ?>
			<tr>
				<th><label for="department"><?php _e( 'Department', 'leading-university' ); ?></label></th>
				<td>
					<?php lu_department_select_dropdown( 'department', esc_attr( get_the_author_meta( 'department', $user->ID ) ), 'department' ); ?>
				</td>
			</tr>
			<?php endif; ?>
			<?php if( current_user_can( $permissions[1] ) ): ?>
			<tr>
				<th><label for="designation"><?php _e( 'Designation', 'leading-university' ); ?></label></th>
				<td>
					<input type="text" name="designation" id="designation" value="<?php echo esc_attr( get_the_author_meta( 'designation', $user->ID ) ); ?>" class="regular-text" />
				</td>
			</tr>
			<tr>
				<th><label for="additional-role"><?php _e( 'Additional Role', 'leading-university' ); ?></label></th>
				<td>
					<input type="text" name="additional_role" id="additional-role" value="<?php echo esc_attr( get_the_author_meta( 'additional_role', $user->ID ) ); ?>" class="regular-text" />
				</td>
			</tr>
			<?php endif; ?>
			<?php if( current_user_can( $permissions[2] ) ): ?>
			<tr>
				<th><label for="order"><?php _e( 'Order', 'leading-university' ); ?></label></th>
				<td>
					<input type="text" name="order" id="order" value="<?php echo esc_attr( get_the_author_meta( 'order', $user->ID ) ); ?>" class="regular-text" />
				</td>
			</tr>
			<?php endif; ?>
			<?php if( current_user_can( $permissions[3] ) ): ?>
			<tr>
				<th><label for="area-of-study"><?php _e( 'Area of Study', 'leading-university' ); ?></label></th>
				<td>
					<?php
					wp_editor( get_the_author_meta( 'area_of_study', $user->ID ), 
						'area-of-study',
						array( 
							'textarea_name' => 'area_of_study',
							'editor_class'  => 'regular-text' ) );
					?>
				</td>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
	<?php
}
add_action( 'show_user_profile', 'lu_custom_user_fields' );
add_action( 'edit_user_profile', 'lu_custom_user_fields' );

/**
 * Add custom contact methods.
 */
add_filter( 'user_contactmethods', function( $fields ){
	$fields[ 'phone' ] = __( 'Phone', 'leading-university' );
	$fields[ 'cell' ] = __( 'Cell', 'leading-university' );
	
	return $fields;
});

/**
 * Save custom user meta related to users position in university.
 */
function lu_update_profile( $user_id ) {
	
	$permissions = array(
		'change_departments',
		'change_user_designation',
		'change_user_order',
		'change_user_area_of_study' );
	
	if( current_user_can( $permissions[0] ) ) update_user_meta( $user_id, 'department', sanitize_text_field( $_POST['department'] ) );
	if( current_user_can( $permissions[1] ) ) update_user_meta( $user_id, 'designation', $_POST['designation'] );
	if( current_user_can( $permissions[1] ) ) update_user_meta( $user_id, 'additional_role', $_POST['additional_role'] );
	if( current_user_can( $permissions[2] ) ) update_user_meta( $user_id, 'order', sprintf('%03d', sanitize_text_field( $_POST['order'] ) ) );
	if( current_user_can( $permissions[3] ) ) update_user_meta( $user_id, 'area_of_study', $_POST['area_of_study'] );
}
add_action( 'personal_options_update', 'lu_update_profile' );
add_action( 'edit_user_profile_update', 'lu_update_profile' );

/**
 * Change the login url.
 */
add_filter( 'login_url', function( $login_url, $redirect = '' ) {
	return home_url( '/login/' . ( '' != $redirect ? '?redirect=' . urlencode( $redirect ) : '' ) );
}, 10, 2 );

/**
 * Change the login url.
 */
add_filter( 'register_url', function( $register_url ) {
	$register_url = site_url( '/student-registration' );
	return $register_url;
});

/**
 * Change the forgot password url.
 */
add_action( 'login_form_lostpassword', function(){
	if( 'GET' == $_SERVER['REQUEST_METHOD'] )
		wp_redirect( home_url( 'forgot-password' ) );
});

/**
 * Process the lost password request.
 */
add_action( 'login_form_lostpassword', function(){
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$errors = retrieve_password();
		$redirect_url = home_url( 'forgot-password' );

		if ( is_wp_error( $errors ) ) {
			// Errors found
			$redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
		} else {
			// Email sent
			$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
		}
		
		wp_redirect( $redirect_url );
		exit;
	}
});

/**
 * Redirect to custom reset password page.
 */
function lu_redirect_reset_password(){
	if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
		
		$_REQUEST['login'] = str_replace( '>', '', $_REQUEST['login'] );
		
		// Verify key / login combo
		$user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
		if ( ! $user || is_wp_error( $user ) ) {
			if ( $user && $user->get_error_code() === 'expired_key' ) {
				wp_redirect( home_url( '/forgot-password?errors=expiredkey' ) );
			} else {
				wp_redirect( home_url( '/forgot-password?errors=invalidkey' ) );
			}
			exit;
		}
		
		$redirect_url = home_url( 'reset-password' );
		$redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
		$redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
		
		wp_redirect( $redirect_url );
		exit;
	}
}
add_action( 'login_form_rp', 'lu_redirect_reset_password' );
add_action( 'login_form_resetpass', 'lu_redirect_reset_password' );

/**
 * Reset the password.
 */
function lu_reset_password(){
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$rp_key = $_REQUEST['rp_key'];
		$rp_login = $_REQUEST['rp_login'];
		
		$user = check_password_reset_key( $rp_key, $rp_login );
		if ( ! $user || is_wp_error( $user ) ) {
			if ( $user && $user->get_error_code() === 'expired_key' ) {
				wp_redirect( home_url( '/forgot-password?errors=expiredkey' ) );
			} else {
				wp_redirect( home_url( '/forgot-password?errors=invalidkey' ) );
			}
			exit;
		}
		
		if ( isset( $_POST['pass1'] ) ) {
			if ( $_POST['pass1'] != $_POST['pass2'] ) {
				// Passwords don't match
				$redirect_url = home_url( 'reset-password' );
				
				$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
				$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
				$redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
				
				wp_redirect( $redirect_url );
				exit;
			}
			
			if ( empty( $_POST['pass1'] ) ) {
				// Password is empty
				$redirect_url = home_url( 'reset-password' );
				
				$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
				$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
				$redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
				
				wp_redirect( $redirect_url );
				exit;
			}
			
			// Parameter checks OK, reset password
			reset_password( $user, $_POST['pass1'] );
			update_user_meta( $user->ID, 'lu_account_status', 'active' );
			wp_redirect( home_url( 'login' ) );
		} else {
			echo __( "Invalid request.", 'leading-university' );
		}
		
		exit;
	}
}
add_action( 'login_form_rp', 'lu_reset_password' );
add_action( 'login_form_resetpass', 'lu_reset_password' );

/**
 * Cleans db garbage after a post is deleted.
 */
add_action( 'delete_post', function( $post_id ){
	global $wpdb;
	$wpdb->query( $wpdb->prepare( "DELETE FROM `{$wpdb->prefix}registered_courses` WHERE registration_id = %d", $post_id ) );
	$wpdb->query( $wpdb->prepare( "DELETE FROM `{$wpdb->prefix}courses` WHERE syllabus = %d", $post_id ) );
});

/**
 * Change WP mail sender email.
 */
add_filter('wp_mail_from', function( $data ){
	return get_bloginfo( 'admin_email' );
});

/**
 * Change wp mail sender name.
 */
add_filter('wp_mail_from_name', function( $data ){
	return get_bloginfo( 'name' );
});

/**
 * Change logout url.
 */
add_filter( 'logout_url', function( $logout_url, $redirect ) {
    if( '' == $redirect ) $redirect = home_url( 'login' );

    return add_query_arg( array( 'redirect_to' => urlencode( $redirect ) ), $logout_url );
}, 10, 2 );

/**
 * Add js code to admin foot.
 */
add_action( 'in_admin_footer', function(){
	?>
	<script>
	jQuery(document).ready(function($){
		<?php if( isset( $_GET['role'] ) ) : ?>
			$('#user-search-input').after('<input type="hidden" name="role" value="<?php echo $_GET['role'];?>"/>');
		<?php endif; ?>
	})
	</script>
	<?php
});

/**
 * Map the shortcodes provided with this theme.
 */
add_action( 'vc_before_init', function() {
	vc_map( array(
		'name' => __( 'Class Schedule', 'leading-university' ),
		'base' => 'lu_class_schedule',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Display the filterable class schedule.', 'leading-university' ),
		'contact_element' => false,
		'show_settings_on_create' => false
	) );
	vc_map( array(
		'name' => __( 'Upcoming Events', 'leading-university' ),
		'base' => 'lu_upcoming_events',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Display upcoming events carousal.', 'leading-university' ),
		'contact_element' => false,
		'show_settings_on_create' => true,
		'params' => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Columns PC", "leading-university" ),
				"param_name" => "cols_md",
				"value" => '',
				"description" => __( "Number of columns to show at a time in Computer.", "leading-university" )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Columns Tab", "leading-university" ),
				"param_name" => "cols_sm",
				"value" => '',
				"description" => __( "Number of columns to show at a time in Tablet.", "leading-university" )
			)
		)
	) );
	vc_map( array(
		'name' => __( 'Campus Map', 'leading-university' ),
		'base' => 'lu_campus_map',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Use this only ones(in a page) to populate campus map.', 'leading-university' ),
		'content_element' => true,
		'as_parent' => array( 'only' => 'lu_campus' ),
		'js_view' => 'VcColumnView',
		'show_settings_on_create' => false
	) );
	vc_map( array(
		'name' => __( 'Campus', 'leading-university' ),
		'base' => 'lu_campus',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Details of a single campus for the map.', 'leading-university' ),
		'content_element' => true,
		'as_child' => array( 'only' => 'lu_campus_map' ),
		'show_settings_on_create' => true,
		'params' => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Campus Title", "leading-university" ),
				"param_name" => "title",
				"value" => '',
				"description" => __( "Name of the campus.", "leading-university" )
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Address", "leading-university" ),
				"param_name" => "content",
				"value" => '',
				"description" => __( "Address or any detail you want to show about the campus.", "leading-university" )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Latitude", "leading-university" ),
				"param_name" => "latitude",
				"value" => '',
				"description" => __( "GPS latitude of the campus location.", "leading-university" )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Longitude", "leading-university" ),
				"param_name" => "longitude",
				"value" => '',
				"description" => __( "GPS longitude of the campus location.", "leading-university" )
			),
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Campus Image", "leading-university" ),
				"param_name" => "image_id",
				"value" => '',
				"description" => __( "An image of the campus.", "leading-university" )
			)
		)
	) );
	vc_map( array(
		'name' => __( 'Latest News Widget', 'leading-university' ),
		'base' => 'lu_latest_news',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Display the last posts in news.', 'leading-university' ),
		'contact_element' => false,
		'show_settings_on_create' => true,
		'params' => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "News Count", "leading-university" ),
				"param_name" => "news_count",
				"value" => '5',
				"description" => __( 'Number of news you would like to show.', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Tags", "leading-university" ),
				"param_name" => "tags",
				"value" => '',
				"description" => __( 'Example: t1, t2<br>Enter multiple tags separated by comma..', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Categories", "leading-university" ),
				"param_name" => "categories",
				"value" => '',
				"description" => __( 'Example: c1, c2<br>Enter multiple categories separated by comma..', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Departments", "leading-university" ),
				"param_name" => "departments",
				"value" => '',
				"description" => __( 'Example: d1, d2<br>Enter multiple departments separated by comma..', 'leading-university' )
			)
		)
	) );
	vc_map( array(
		'name' => __( 'Latest Notice Widget', 'leading-university' ),
		'base' => 'lu_latest_notice',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Display the last posts in notice.', 'leading-university' ),
		'contact_element' => false,
		'show_settings_on_create' => true,
		'params' => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Notice Count", "leading-university" ),
				"param_name" => "notice_count",
				"value" => '5',
				"description" => __( 'Number of notices you would like to show.', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Tags", "leading-university" ),
				"param_name" => "tags",
				"value" => '',
				"description" => __( 'Example: t1, t2<br>Enter multiple tags separated by comma..', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Categories", "leading-university" ),
				"param_name" => "categories",
				"value" => '',
				"description" => __( 'Example: c1, c2<br>Enter multiple categories separated by comma..', 'leading-university' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Departments", "leading-university" ),
				"param_name" => "departments",
				"value" => '',
				"description" => __( 'Example: d1, d2<br>Enter multiple departments separated by comma..', 'leading-university' )
			)
		)
	) );
	vc_map( array(
		'name' => __( 'Campus Facility', 'leading-university' ),
		'base' => 'lu_campus_facility',
		'category' => __( 'Leading University', 'leading-university' ),
		'description' => __( 'Display tile for campus facility', 'leading-university' ),
		'content_element' => true,
		'show_settings_on_create' => true,
		'params' => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Icon Name", "leading-university" ),
				"param_name" => "icon",
				"value" => 'star',
				"description" => sprintf(
					__( "Get icon name from %s.", "leading-university" ),
					'<a href="http://fontawesome.github.io/Font-Awesome/icons/">Fontawesome</a>' )
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Facility Title", "leading-university" ),
				"param_name" => "title",
				"value" => '',
				"description" => __( "The facility you are presenting.", "leading-university" )
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Description", "leading-university" ),
				"param_name" => "content",
				"value" => '',
				"description" => __( "Brief details on the facility.", "leading-university" )
			)
		)
	) );
});


/**
 * Adds the necessery admin pages.
 */
add_action( 'admin_menu', function() {
	add_submenu_page(
		'edit.php?post_type=program',
		__( 'Export Registrations', 'leading-university' ),
		__( 'Export Registrations', 'leading-university' ), 
		'export_registration',
		'export_registration',
		function(){
			?>
			<div class="wrap" ng-app="registration-export">
				<h1><?php _e( 'Export Registration', 'leading-university' ); ?></h1>
				<form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" ng-controller="ExportCTRL">
					<table class="form-table">
						<tbody>
							<tr>
								<th scope="row">
									<label for="program"><?php _e( 'Program', 'leading-university' ); ?></label>
								</th>
								<td>
									<?php
									$programs = get_posts( array(
										'post_type'      => 'program',
										'post_status'    => 'publish',
										'posts_per_page' => -1 ) );
										
									$options = array( );
									foreach( $programs as $program ){
										$options[$program->ID] = $program->post_title;
									}
									
									lu_create_select_field( $options, 0, array( 'id' => 'program', 'name' => 'program_id', 'ng-model' => 'program', 'ng-change' => 'program_changed()' ), true );
									?>
								</td>
							</tr>
							<tr>
								<th scope="row">
									<label for="semester"><?php _e( 'Semester', 'leading-university' ); ?></label>
								</th>
								<td>
									<?php lu_semester_select_dropdown( 0, array( 'id' => 'semester', 'name' => 'semester_id' ) ); ?>
								</td>
							</tr>
							<tr>
								<th scope="row">
									<label for="status"><?php _e( 'Registration Status', 'leading-university' ); ?></label></th>
								<td>
									<?php lu_create_select_field( 
										array(
											'submitted',
											'review',
											'approved',
											'registered'
											),
										'registered',
										array( 'id' => 'status', 'name' => 'status' ) ); ?>
								</td>
							</tr>
							<tr>
								<th scope="row">
									<label for="courses"><?php _e( 'Courses', 'leading-university' ); ?></label>
								</th>
								<td>
									<ui-select multiple ng-model="t.selected_courses" theme="select2" style="width:400px">
										<ui-select-match placeholder="Select courses...">{{$item.code}} ({{$item.syllabus_name}})</ui-select-match>
										<ui-select-choices repeat="ccode.ID as ccode in courses | filter: $select.search">
											<div>{{ccode.code}}</div>
											<small>
												<?php _e( 'Title:', 'leading-university' );?> {{ccode.title}}<br>
												<?php _e( 'Syllabus:', 'leading-university' );?> {{ccode.syllabus_name}}<br>
												<?php _e( 'Credit:', 'leading-university' );?> <span ng-bind-html="''+ccode.credit | highlight: $select.search"></span>
											</small>
										</ui-select-choices>
									</ui-select>
									<input type="hidden" name="courses" value="{{t.selected_courses}}">
								</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td><button type="submit" class="button button-primary"><?php _e( 'Submit', 'leading-university' ); ?></button></td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="action" value="export-registration">
					<?php wp_nonce_field( 'export_registration', 'lu_nonce' ); ?>
				</form>
			</div>
			<?php
			wp_enqueue_script( 'export-registration' );
		});

    /**
     * Add menu page to view evaluation report.
     */
    add_menu_page(__('Evaluation Report', 'leading-university'),
        __('Evaluation Report', 'leading-university'),
        'view_evaluation_report',
        'evaluation-report',
        function(){
            do_action( 'add_meta_boxes_evaluation_report' );
            wp_enqueue_script( 'select2', get_template_directory_uri() . '/js/select2.full.js', array( 'jquery' ) );
            wp_enqueue_script( 'evaluation-report', get_template_directory_uri() . '/js/ng-apps/evaluationReport.js',
                array( 'jquery', 'angular', 'angular-ui-select', 'angular-sanitize', 'lu-helper' ) );
            wp_localize_script('evaluation-report', 'luer', array(
                'nonce' => wp_create_nonce('evaluation_report')
            ));
            ?>
            <div class="wrap" id="evaluation-report" ng-app="evaluationSummeryApp"  ng-controller="FilterCtrl">
                <h1>Evaluation Report</h1>
                <hr class="wp-header-end">
                <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-2">
                        <div id="post-body-content" style="position: relative;">
                            <?php do_meta_boxes('evaluation_report', 'normal', null); ?>
                        </div>
                        <div id="postbox-container-1" class="postbox-container">
                            <?php do_meta_boxes('evaluation_report', 'side', null); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        },
        'dashicons-forms');

    /**
     * Add submenu page to set up evaluation questions.
     */
	add_submenu_page('evaluation-report',
         __('Evaluation Questions', 'leading-university'),
         __('Evaluation Questions', 'leading-university'),
        'manage_evaluation_questions',
        'evaluation-questions',
        function(){
	         ?>
            <div class="wrap" id="evaluation-questions">
                <h1>Evaluation Questions</h1>
                <hr class="wp-header-end">

                <?php if(isset($_GET['message']) && $_GET['message'] == 1):?>
                <div id="message" class="updated notice notice-success is-dismissible">
                    <p>Changes saved.</p>
                    <button type="button" class="notice-dismiss">
                        <span class="screen-reader-text">Dismiss this notice.</span>
                    </button>
                </div>
                <?php endif; ?>

                <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
                    <div class="aspects">
                        <?php
                        $aspects = lute_aspects();

                        foreach($aspects as $i => $aspect) {
                            ?>
                            <div class="aspect">
                                <div class="aspect-header">
                                    <span class="dashicons-move dashicons aspect-move ui-sortable-handle"></span>
                                    <input type="text" name="aspect_weight[]" value="<?php echo $aspect->weight; ?>"
                                           class="aspect-weight" placeholder="Weight">
                                    <input type="text" name="aspect_name[]" class="aspect-name"
                                           value="<?php echo $aspect->name; ?>"
                                           placeholder="Specify a name for the aspect...">

                                    <button type="button" class="button button-default aspect-remove">
                                        <span class="dashicons dashicons-trash"></span>
                                    </button>
                                    <input type="hidden" name="aspect_removed[]" value="no">
                                    <input type="hidden" name="aspect_index[]" value="<?php echo $i; ?>">
                                    <input type="hidden" name="aspect_id[]" value="<?php echo $aspect->ID; ?>">
                                </div>
                                <div class="aspect-body wp-clearfix">
                                    <div class="questions">
                                        <?php
                                        $questions = $aspect->questions;
                                        foreach($questions as $j => $question) {
                                            ?>
                                            <div class="question">
                                                <span class="dashicons-move dashicons question-move"></span>
                                                <input type="text" name="question_weight[]" value="<?php echo $question->weight; ?>"
                                                       class="question-weight" placeholder="Weight">
                                                <input type="text" name="question_name[]" class="question-name"
                                                       placeholder="The question..." value="<?php echo $question->name; ?>">
                                                <button type="button" class="button button-default question-remove">
                                                    <span class="dashicons dashicons-trash"></span>
                                                </button>
                                                <input type="hidden" name="question_removed[]" value="no">
                                                <input type="hidden" name="question_aspect_index[]" value="<?php echo $i; ?>">
                                                <input type="hidden" name="question_index[]" value="<?php echo $j; ?>">
                                                <input type="hidden" name="question_id[]" value="<?php echo $question->ID; ?>">
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <button type="button" class="button button-default add-question">Add Question</button>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <button type="button" class="button button-default" id="add-aspect">Add Aspect</button>
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
                    <input type="hidden" name="action" value="save_evaluation_questions">
                    <?php wp_nonce_field('evaluation_questions'); ?>
                </form>
            </div>
            <?php
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('lu-admin');
        });

});

/**
 * Column content for columns of registration
 */
add_action( 'manage_registration_posts_custom_column' , function( $column, $post_id ) {
	$statuses = get_post_stati( array(), 'objects' );
	switch( $column ){
		case 'status':
			echo $statuses[ get_post_status( $post_id) ]->label;
			break;
		case 'program':
			$program = get_post( get_post_meta( $post_id, 'program', true ) );
			$department = get_term( get_post_meta( $post_id, '_department', true ), 'department' );
			if( null != $department ) echo $program->post_title . ', <br>' . $department->name;
			break;
		case 'modified':
			echo get_post_field('post_modified', $post_id);
			break;
	}
	
	return $column;
}, 10, 2 );

/**
 * Add status column to registration post type.
 */
add_filter( 'manage_registration_posts_columns' , function( $columns ) {
	unset( $columns['date'] );
	return array_merge( $columns, 
		array(
			'status'  => __( 'Current Status', 'leading-university' ),
			'author'  => __( 'Last Modified By', 'leading-university' ),
			'program' => __( 'Program, Department', 'leading-university' ),
			'modified'    => __( 'Last Modified', 'leading-university' ) ) );
} );

/**
 * @param $data
 */
function lu_process_evaluation_question( $data, $i = 0, $type = 'aspect', $parent = 0 ) {
	global $wpdb;

	if ( $data['id'] == 0 && $data['removed'] != 'yes' ) {
		$wpdb->insert( "{$wpdb->prefix}evaluation_questions",
			array(
				'name'   => $data['name'],
				'weight' => $data['weight'],
				'type'   => $type,
                'index'  => $i,
				'parent' => $parent,
			) );

		return $wpdb->insert_id;
	} else if ( $data['id'] != 0 && $data['removed'] != 'yes' ) {
		$wpdb->update( "{$wpdb->prefix}evaluation_questions",
			array(
				'name'   => $data['name'],
				'weight' => $data['weight'],
				'type'   => $type,
                'index'  => $i,
				'parent' => $parent,
			),
			array( 'ID' => $data['id'] ) );

		return $data['id'];
	} else if ( $data['id'] != 0 && $data['removed'] == 'yes' ) {
		$wpdb->delete( "{$wpdb->prefix}evaluation_questions",
			array( 'ID' => $data['id'] ) );

		return 0;
	}
}

/**
 * Handle submission of Evaluation Questions settings page
 */
add_action( 'admin_post_save_evaluation_questions', function() {
    if(!wp_verify_nonce($_POST['_wpnonce'], 'evaluation_questions') || !current_user_can('manage_evaluation_questions')) {
        if(wp_redirect($_POST['_wp_http_referer'])) exit;
    }

    $aspects = array();
    for($i = 0; $i < count($_POST['aspect_index']); $i++) {
        $aspects[$i]['name'] = $_POST['aspect_name'][$i];
        $aspects[$i]['weight'] = $_POST['aspect_weight'][$i];
        $aspects[$i]['removed'] = $_POST['aspect_removed'][$i];
        $aspects[$i]['id'] = $_POST['aspect_id'][$i];
    }

    for($i = 0; $i < count($_POST['question_name']); $i++) {
	    $aspects[$_POST['question_aspect_index'][$i]]['questions'][$_POST['question_index'][$i]]['name'] = $_POST['question_name'][$i];
	    $aspects[$_POST['question_aspect_index'][$i]]['questions'][$_POST['question_index'][$i]]['weight'] = 1;
	    $aspects[$_POST['question_aspect_index'][$i]]['questions'][$_POST['question_index'][$i]]['removed'] = $_POST['question_removed'][$i];
	    if($aspects[$_POST['question_aspect_index'][$i]]['removed'] == 'yes')
	        $aspects[$_POST['question_aspect_index'][$i]]['questions'][$_POST['question_index'][$i]]['removed'] = 'yes';

	    $aspects[$_POST['question_aspect_index'][$i]]['questions'][$_POST['question_index'][$i]]['id'] = $_POST['question_id'][$i];
    }

	foreach($aspects as $i => $aspect) {
		$aspect_id = lu_process_evaluation_question( $aspect, $i );

		foreach($aspect['questions'] as $j => $question) {
		    lu_process_evaluation_question($question, $j, 'question', $aspect_id);
        }
	}

	wp_redirect(add_query_arg( 'message', '1', $_POST['_wp_http_referer'] ));
    exit;
});

/**
 * Save evaluation for a submission.
 */
add_action( 'admin_post_lute_save_evaluation', function() {
    global $wpdb;

    if(!wp_verify_nonce($_POST['_wpnonce'], 'save_evaluation')) wp_die('Invalid submission.');

    $aspects = lute_aspects();
    $answers = $_POST['question'];
    // TODO: Verify if the course is available to the student.
    $course_id = $_POST['course_id'];

    // TODO: Redirect with message instead of killing.
    if(lute_course_evaluated($course_id)) wp_die("Evaluatoin already submitted.");

    $teacher_id = $_POST['course_teacher'];
    $section = $_POST['section'];

    $insert = array();

    $total_score = 0;
    $total_weight = 0;
    foreach($aspects as $aspect){
        $aspect_total_score = 0;
        $aspect_total_weight = 0;

        foreach($aspect->questions as $question){
            if(!isset($answers[$aspect->ID]) || !isset($answers[$aspect->ID][$question->ID])){
                wp_die("Invalid submission.");
            }

            $aspect_total_score += $answers[$aspect->ID][$question->ID] * $question->weight;
            $aspect_total_weight += $question->weight;

            $insert[] = array(
                'question_id' => $question->ID,
                'score' => $answers[$aspect->ID][$question->ID],
                'user_id' => get_current_user_id(),
                'course_id' => $course_id,
                'teacher_id' => $teacher_id,
                'semester_id' => lu_get_user_program_meta('_evaluation_semester'),
                'section' => $section
            );
        }

        $aspect_score = (double) $aspect_total_score / $aspect_total_weight;
        $insert[] = array(
            'question_id' => $aspect->ID,
            'score' => $aspect_score,
            'user_id' => get_current_user_id(),
            'course_id' => $course_id,
            'teacher_id' => $teacher_id,
            'semester_id' => lu_get_user_program_meta('_evaluation_semester'),
            'section' => $section
        );

        $total_score += $aspect_score * $aspect->weight;
        $total_weight += $aspect->weight;
    }

    $score = (double) $total_score / $total_weight;
    $insert[] = array(
        'question_id' => 0,
        'score' => $score,
        'user_id' => get_current_user_id(),
        'course_id' => $course_id,
        'teacher_id' => $teacher_id,
        'semester_id' => lu_get_user_program_meta('_evaluation_semester'),
        'section' => $section
    );


    foreach($insert as $row){
        $wpdb->insert($wpdb->prefix.'evaluation_score', $row, array('%d', '%f', '%d', '%d', '%d', '%d', '%s'));
    }

    wp_redirect(add_query_arg('course', lute_next_pending_evaluation_course(), $_POST['_wp_http_referer']));
});

/**
 * Remove autosave feature from the custom post types.
 */
add_action( 'admin_enqueue_scripts', function( $pagehook ) {
	global $post_type, $current_screen;

	if( in_array( $post_type, array( 'registration', 'program', 'syllabus' ) ) )
		wp_dequeue_script( 'autosave' );         
});

// Essential class extensions for campus map nested shortcodes to map and provide row support
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_Lu_Campus_Map extends WPBakeryShortCodesContainer {
	}
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
	class WPBakeryShortCode_Lu_Campus extends WPBakeryShortCode {
	}
}
