<?php
/**
 * The markup for the evaluation form.
 */

if(!lu_is_student()) :
echo "You must be a student to access the evaluation form.";
else :
?>
<form action="<?php echo admin_url("admin-post.php"); ?>" id="evaluation-form" method="post">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-pills">
            <?php
            $registered_courses = lu_get_registered_courses(
                lu_get_student_registration_id(
                    false,
                    get_term(lu_get_user_program_meta('_evaluation_semester'), 'semester')->name
                )
            );
            $cur_registered_course = 0;
            foreach($registered_courses as $registered_course):
                $course = lu_get_course($registered_course->course_id);
                if(isset($_GET['course']) && $registered_course->course_id == $_GET['course']) $cur_registered_course = $registered_course;
                ?>
            <li role="presentation" <?php if(isset($_GET['course']) && $_GET['course'] == $course->ID) echo 'class="active"';?>>
                <a href="<?php echo add_query_arg('course', $course->ID); ?>">
                    <?php echo $course->code; ?>
                    <?php if(lute_course_evaluated($course->ID)): ?>
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <?php endif; ?>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>

        <?php if(isset($_GET['course']) && $_GET['course'] != 0 && $_GET['course'] != '' && $cur_registered_course != 0): ?>

            <?php
            $cur_course = lu_get_course($_GET['course']);
            $cur_score = lute_get_evaluation_score($cur_course->ID);
            $evaluated = lute_course_evaluated($cur_course->ID);

            ?>

            <h2>Course: <?php echo $cur_course->title; ?></h2>
            <?php
            $teacher_id = 0;
            if($evaluated) {
                global $wpdb;
                $teacher_id = $wpdb->get_var( $wpdb->prepare("SELECT `teacher_id`
                  FROM `{$wpdb->prefix}evaluation_score`
                  WHERE `question_id`=0
                    AND `user_id`=%d
                    AND `course_id`=%d", get_current_user_id(), $cur_course->ID
                ));
            }
            ?>

            <div class="row evaluation-options">
                <div class="col-md-6">
                    <label for="course-teacher">Course Teacher</label>
                    <select class="selectpicker custom-dropdown-btn section-el" id="course-teacher"
                            name="course_teacher" required
                            <?php echo $evaluated ? 'disabled="disabled"' : ''; ?>>
                        <option value="">Select...</option>
                        <?php
                        $departments = get_terms( 'department', array('hide_empty' => false) );
                        foreach($departments as $department):
                            $teachers = lu_get_department_teachers($department->term_id);
                            if(!empty($teachers)):
                            ?>
                            <optgroup label="<?php echo $department->name;?>">
                                <?php foreach( $teachers as $teacher): ?>
                                    <option value="<?php echo $teacher->ID;?>"
                                        <?php echo $teacher_id == $teacher->ID ? 'selected="selected"' : ''; ?>>
                                        <?php echo $teacher->display_name;?>
                                    </option>
                                <?php endforeach; ?>
                            </optgroup>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php
                        $teachers = lu_get_department_teachers(0);
                        if(!empty($teachers)):
                        ?>
                        <optgroup label="<?php _e('Other', 'leading-university');?>">
                            <?php foreach( $teachers as $teacher): ?>
                                <option value="<?php echo $teacher->ID;?>"
                                    <?php echo $teacher_id == $teacher->ID ? 'selected="selected"' : ''; ?>>
                                    <?php echo $teacher->display_name;?>
                                </option>
                            <?php endforeach; ?>
                        </optgroup>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="semester">Semester</label>
                    <?php lu_semester_select_dropdown(
                        lu_get_user_program_meta('_evaluation_semester'),
                        array(
                            'class' => 'selectpicker custom-dropdown-btn section-el',
                            'id' => 'semester',
                            'disabled' => 'disabled'
                        )
                    ); ?>
                </div>
            </div>

            <p>
            <?php echo $content; ?>
            </p>
            
            <div class="tef-aspect-section">
                <?php foreach(lute_aspects() as $i => $aspect): ?>
                <h3>
                    <?php echo chr(65+$i).": ".$aspect->name; ?>
                </h3>
                <table class="table tef-table">
                    <thead class="hidden-xs">
                    <tr>
                        <th rowspan="2">Aspects related to teaching performance</th>
                        <th colspan="5">Scale</th>
                    </tr>
                    <tr>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($aspect->questions as $j => $question): ?>
                    <tr class="answer-row">
                        <td class="tef-question"><?php echo ($j+1).". $question->name";?></td>
                        <td class="tef-scale">
                            <input type="radio" name="question[<?php echo $aspect->ID;?>][<?php echo $question->ID;?>]"
                                   id="<?php echo "question-$i-$j-1";?>" value="1" class="form-control answer"
                                   <?php if($evaluated) echo 'disabled="disabled"'; ?>
                                   <?php if($evaluated && $cur_score[$question->ID] == 1) echo 'checked="checked"';?>>
                            <label for="<?php echo "question-$i-$j-1";?>">1</label>
                        </td>
                        <td class="tef-scale">
                            <input type="radio" name="question[<?php echo $aspect->ID;?>][<?php echo $question->ID;?>]"
                                   id="<?php echo "question-$i-$j-2";?>" value="2" class="form-control answer"
                                   <?php if($evaluated) echo 'disabled="disabled"'; ?>
                                   <?php if($evaluated && $cur_score[$question->ID] == 2) echo 'checked="checked"';?>>
                            <label for="<?php echo "question-$i-$j-2";?>">2</label>
                        </td>
                        <td class="tef-scale">
                            <input type="radio" name="question[<?php echo $aspect->ID;?>][<?php echo $question->ID;?>]"
                                   id="<?php echo "question-$i-$j-3";?>" value="3" class="form-control answer"
                                   <?php if($evaluated) echo 'disabled="disabled"'; ?>
                                   <?php if($evaluated && $cur_score[$question->ID] == 3) echo 'checked="checked"';?>>
                            <label for="<?php echo "question-$i-$j-3";?>">3</label>
                        </td>
                        <td class="tef-scale">
                            <input type="radio" name="question[<?php echo $aspect->ID;?>][<?php echo $question->ID;?>]"
                                   id="<?php echo "question-$i-$j-4";?>" value="4" class="form-control answer"
                                   <?php if($evaluated) echo 'disabled="disabled"'; ?>
                                   <?php if($evaluated && $cur_score[$question->ID] == 4) echo 'checked="checked"';?>>
                            <label for="<?php echo "question-$i-$j-4";?>">4</label>
                        </td>
                        <td class="tef-scale">
                            <input type="radio" name="question[<?php echo $aspect->ID;?>][<?php echo $question->ID;?>]"
                                   id="<?php echo "question-$i-$j-5";?>" value="5" class="form-control answer"
                                   <?php if($evaluated) echo 'disabled="disabled"'; ?>
                                   <?php if($evaluated && $cur_score[$question->ID] == 5) echo 'checked="checked"';?>>
                            <label for="<?php echo "question-$i-$j-5";?>">5</label>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endforeach; ?>
            </div>


            <button type="submit" class="btn btn-primary waves-effect waves-light"
                <?php if($evaluated) echo 'disabled="disabled"';?>>Save</button>
            <input type="hidden" name="course_id" value="<?php echo (int) $_GET['course'];?>">
            <input type="hidden" name="section"
                   value="<?php echo !is_null($cur_registered_course) ? $cur_registered_course->section : '';?>">
            <input type="hidden" name="action" value="lute_save_evaluation">
            <?php wp_nonce_field('save_evaluation'); ?>
        <?php else: ?>
            <p>Something went wrong!</p>
        <?php endif; ?>
    </div>
</form>
<?php endif; ?>

