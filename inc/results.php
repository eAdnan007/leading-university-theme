<?php
add_action( 'wp_ajax_nopriv_get-result', 'lu_op_result' );
add_action( 'wp_ajax_get-result', 'lu_op_result' );

function lu_op_result(){
	header('Content-Type: application/json');

	if(
		!isset($_POST['student_id']) || 
		!preg_match("/[0-9A-Za-z]{10,20}/", $_POST['student_id'])
	) {
		lu_error('invalid_request', 'Form was not submitted properly.');
	}

	$student_user = lu_get_student_user( $_POST['student_id'] );
	
	if( !current_user_can( 'view_results' ) && !$student_user )
		lu_error( 
			'not_registered', __( 'This student is not registered to student portal. Please register first to view your results.' ) );
	
	$raw_data = lu_fetch_result( $_POST['student_id'] );
	if( is_wp_error( $raw_data ) ){
		foreach( $raw_data->errors as $code => $message ){
			lu_error( $code, $message[0] );
		}
	}
	
	if( 
		isset( $_POST['birth_date'] ) &&
		get_user_meta( get_current_user_id(), 'student_id', true ) != $_POST['student_id'] && 
		!lu_verify_birthdate( $_POST['birth_date'], $raw_data->student ) &&
		'' != trim( $_POST['birth_date'] ) && 
		'undefined' != $_POST['birth_date'] && 
		!current_user_can( 'view_results' ) )
		lu_error( 'birthdate_mismatch', __( 'Input verification failed, contact admission office.', 'leading-university' ) );

	$response = array(
		'success' => true,
		'student' => array(
			'name'       => $raw_data->student->StudentName,
			'id'         => $raw_data->student->StudentID,
			'department' => $raw_data->student->Department,
			'semester'   => $raw_data->student->Semister . '-' . $raw_data->student->AddYear,
			'degree'     => $raw_data->student->Degree ) );

	$results = array();

	$waived_credit = $weighted_point = $total_credit = 0;

	foreach( $raw_data->result as $result){

		// Remove faild and incomplete results
		// if(0.0 == $result->GPA) continue;

		if( 'Courses Waived' == $result->Semister ){
			$waived_credit += $result->CCredit;
			continue;
		}

		$temp = array();
		$temp['course_code']  = $result->CCode;
		$temp['course_title'] = $result->CTitle;
		$temp['grade']        = $result->LG;
		$temp['gpa']          = $result->GPA;
		$temp['credit']       = $result->CCredit;

		$results[$result->CYear][$result->Semister][] = $temp;

		$weighted_point += $temp['gpa'] * $temp['credit'];
		$total_credit += $temp['credit'];
	}

	$response['student']['cgpa'] = $weighted_point != 0 ? round($weighted_point / $total_credit, 2) : 0;
	$response['student']['credit'] = $total_credit;
	$response['student']['grade'] = $weighted_point != 0 ? lu_get_grade($response['student']['cgpa']) : "I";
	$response['student']['waived_credit'] = $waived_credit;

	$response = apply_filters('lu_result', $response, $_POST['student_id'], $_POST['birth_date']);


	/**
	 * Only include the gradesheet if user is logged in.
	 */
	if(
		lu_verify_birthdate( $_POST['birth_date'], $raw_data->student ) ||
		current_user_can( 'view_results' ) ||
		get_user_meta( get_current_user_id(), 'student_id', true ) == $response['student']['id']
		) :

		$response['results'] = array();
		/**
		 * Sort the semesters
		 */
		foreach($results as $year => $semesters){
			$seasons = array(
				'spring' => 'Spring',
				'summer' => 'Summer',
				'fall'   => 'Fall' );

			foreach( $seasons as $key => $s_name ){
				if(isset($semesters[$s_name])){
					$courses = $semesters[$s_name];
					$credit = $gpa = 0;
					foreach( $courses as $course ){
						$credit += $course['credit'];
						$gpa += ($course['gpa']*$course['credit']);
					}
					$t = array();
					$t['courses'] = $courses;
					$t['name'] = "$s_name - $year";
					$t['gpa'] = $credit != 0 ? round($gpa/$credit, 2) : 0;
					$t['credit'] = $credit;
					$t['grade'] = $credit != 0 ? lu_get_grade(round($gpa/$credit, 2)) : 'F';

					$response['results'][$year][] = $t;
				}
			}
		}
	endif;


	/**
	 * Include the row data with response to debug if debugging. If it is a student restrict results for unevaluated
     * courses.
	 */
	if( current_user_can( 'debug_result' ) ){
	    $response['row_data'] = $raw_data;
    }
    elseif(!current_user_can( 'view_results' )) {
	    $seasons = array(
            'Spring',
            'Summer',
            'Fall'
        );

        $evaluation_semester = get_term(lu_get_user_program_meta('_evaluation_semester', $student_user->ID ),
            'semester');

        $evaluation_semester = explode('-', $evaluation_semester->name);

        if(isset($response['results'][$evaluation_semester[1]])) {
	        $restrict = &$response['results'][$evaluation_semester[1]][array_search($evaluation_semester[0], $seasons)];

	        foreach($restrict['courses'] as &$course){
		        $course_id = lu_find_course_by_code($course['course_code'])->ID;

		        if(!lute_course_evaluated($course_id)) {
			        $course['gpa'] = 0.0;
			        $course['grade'] = 'EI';
			        $course['evaluation_link'] = lute_get_evaluation_link($course_id);

			        $restrict['gpa'] = 0;
			        $restrict['grade'] = 'EI';
		        }

	        }
        }
    }

	$response = apply_filters('lu_result_response', $response, $_POST['student_id'], $_POST['birth_date'], $raw_data);

	echo json_encode( $response );
	exit;
}

/**
 * Fetch result from remote server.
 */
function lu_fetch_result( $student_id ){
	global $lu_config;
	global $result_data;
	
	if( !isset( $result_data ) )
		$result_data = array();
	
	if( isset( $result_data[$student_id] ) )
		return $result_data[$student_id];
	
	$request = array(
		'action'     => 'get_result',
		'student_id' => trim( $student_id ),
		'public_key' => $lu_config['result_public_key'],
		'timestamp'  => time() );

	$request['token'] = lu_get_token( $request );

	$data = array( 'request' => json_encode($request) );

	$response = wp_remote_post( $lu_config['result_request_url'], array(
		'method'      => 'POST',
		'timeout'     => $lu_config['result_request_timeout'],
		'redirection' => 5,
		'httpversion' => '1.0',
		'blocking'    => true,
		'body'        => $data ) );
		
	
			
	if( is_wp_error( $response ) || 200 != $response['response']['code'] ) {
		$result_data[$student_id] = new WP_Error('service_unavailable', __( 'Service temporarily unavailable. Please try again later.', 'leading-university' ) );
		return $result_data[$student_id];
	}
	
	$raw_data = json_decode( $response['body'] );
	if( !is_object($raw_data ) ) return new WP_Error('service_unavailable', __( 'Service temporarily unavailable. Please try again later.', 'leading-university' ) );
	if( !isset($raw_data->message) && false == $raw_data->success ) $raw_data->message = __( 'Error occurred while requesting for result.', 'leading-university' );

	if( $raw_data->success == false ) return new WP_Error($raw_data->error, $raw_data->message);
	
	$result_data[$student_id] = $raw_data;

	return $raw_data;
}

function lu_get_student_previously_registered_courses($student_id) {
	global $wpdb;

	$user = lu_get_student_user($student_id);

	$query = $wpdb->prepare("SELECT *
		FROM {$wpdb->prefix}registered_courses
		WHERE student_id=%d
			AND status='registered'", $user->ID);


	$courses = $wpdb->get_results( $query );

	$courses = array_combine(array_map(function ($c) {
		return $c->course_id;
	}, $courses),
	$courses);


	return $courses;
}

/**
 * Get cgpa by course code
 * 
 * Loops through all courses to find if there were retakes. Returns the highest
 * cgpa from them.
 * 
 * @param string $student_id Student ID
 * @param string $course_code Course code to find result of
 * 
 * @return float The gpa achived. Returns -1 if course was not taken.
 */
function lu_get_course_gpa( $student_id, $course_code ){
	$raw_data = lu_fetch_result( $student_id );
	if( is_wp_error( $raw_data ) ){
		return 0;
	}
	$results = $raw_data->result;
	
	$gpa = -1;
	foreach( $results as $result ){
		
		if( $course_code == $result->CCode && $result->GPA > $gpa ){
			$gpa = $result->GPA;
		}
	}
	
	return $gpa;
}

/**
 * Stops the script execution and sends a failure message to relay.
 */
function lu_error( $error, $error_message = null ){
	$result = array(
		'success' => false,
		'error'   => $error );

	if( null != $error_message ) $result['message'] = $error_message;
	else $result['message'] = __( 'Unable to process your request at this moment.', 'leading-university' );

	echo json_encode( $result );

	die();
}

/**
 * Get token from request array. Always replaces request with get_result
 */
function lu_get_token( $request ){
	global $lu_config;
	ksort( $request );
	
	$new_token = md5( serialize( $request ) . $lu_config['result_private_key'] );

	/*
	 * The newly created token should be same as the token received with the request
	 */
	return $new_token;
}

/**
 * Get grade from gpa
 */
function lu_get_grade( $gpa ){
	if( $gpa == 4 ) $olg = 'A+';
	elseif( $gpa >= 3.75 ) $olg = 'A';
	elseif( $gpa >= 3.50 ) $olg = 'A-';
	elseif( $gpa >= 3.25 ) $olg = 'B+';
	elseif( $gpa >= 3.00 ) $olg = 'B';
	elseif( $gpa >= 2.75 ) $olg = 'B-';
	elseif( $gpa >= 2.50 ) $olg = 'C+';
	elseif( $gpa >= 2.25 ) $olg = 'C';
	elseif( $gpa >= 2.00 ) $olg = 'D';
	else $olg = 'F';

	return $olg;
}

