<?php

/**
 * Remove ul and li from choice fields.
 */
add_filter( 'gform_field_choice_markup_pre_render', function( $choice_markup, $choice, $field, $value ) {
	if ( $field->get_input_type() == 'radio' ) {
		return str_replace( array( '<ul', '</ul>', '<li', '</li>'), array( '<div', '</div>', '<div', '</div>'), $choice_markup );
	}
	
	return $choice_markup;
}, 10, 4 );

add_filter( 'gform_field_content', function ( $field_content, $field ) {
	if ( $field->type == 'select' ) {
		return str_replace( 'gfield_select', 'gfield_select browser-default', $field_content );
	}
	
	if( $field->type == 'textarea' ) {
		return preg_replace(
			array( 
				"/(<label[^>]*>.*<\/label>)<div[^>]*>\s*(<textarea[^>]*class=')([^']*)('[^<]*<\/textarea>)\s*<\/div>/",
				"/ (rows|cols)='[0-9]*'/"
				), 
			array(
				"<div class=\"input-field\">$1$2 materialize-textarea$4</div>",
				""
				), 
			$field_content );
	}

	if( $field->type == 'text' || $field->type == 'email' || $field->type == 'phone' || $field->type == 'date' ) {
		return preg_replace(
			"/\A(<label[^>]*>.*<\/label>)<div[^>]*>\s*(<input[^>]*>(\s*<img[^>]*>)?)\s*<\/div>/", 
			"<div class=\"input-field\">$1$2</div>", 
			$field_content );
	}
	
	if( $field->type == 'name' ) {
		return preg_replace(
			"/(<span id='input[^']*container' class=')([^']*)('[^>]*>)/", 
			"$1$2 input-field$3", 
			$field_content );
	}
	
	return $field_content;
}, 10, 2 );

add_filter( 'gform_next_button', 'lu_gf_button_filter', 10, 2 );
add_filter( 'gform_previous_button', 'lu_gf_button_filter', 10, 2 );
add_filter( 'gform_submit_button', 'lu_gf_button_filter', 10, 2 );
function lu_gf_button_filter( $button_markup, $form ) {
	
	return preg_replace(
		"/<input([^>]*)class='([^']*)' value='([^']*)'([^\/>]*)\/?>/", 
		"<button $1$4 class='$2 btn btn-custom waves-effect waves-light'>$3</button>", 
		$button_markup );
}