<?php
/**
 * Change visual composer classes to support directly provided libraries.
 */
add_filter( 'vc_shortcodes_css_class', function ( $class_string, $tag ) {
	return str_replace( array( 'vc_' ), array( '' ), $class_string );
}, 10, 2 );

/**
 * Displays the latest notice
 */
add_shortcode( 'lu_latest_notice', function ( $atts ) {
	extract(
		shortcode_atts(
			array(
				'notice_count' => 5,
				'tags'         => array(),
				'categories'   => array(),
				'departments'  => array()
			),
			$atts ) );

	if ( ! is_array( $tags ) ) {
		$tags = explode( ',', $tags );
		$tags = array_map( function ( $tag ) {
			$tag = trim( $tag );
			if ( ! is_numeric( $tag ) ) {
				$tag = get_term_by( 'slug', $tag, 'post_tag' );

				return $tag->term_id;
			}

			return (int) $tag;
		}, $tags );
	}

	if ( ! is_array( $categories ) ) {
		$categories = explode( ',', $categories );
		$categories = array_map( function ( $category ) {
			$category = trim( $category );
			if ( ! is_numeric( $category ) ) {
				$category = get_term_by( 'slug', $category, 'category' );

				return $category->term_id;
			}

			return (int) $category;
		}, $categories );
	}

	if ( ! is_array( $departments ) ) {
		$departments = explode( ',', $departments );
		$departments = array_map( function ( $department ) {
			$department = trim( $department );
			if ( ! is_numeric( $department ) ) {
				$department = get_term_by( 'slug', $department, 'department' );

				return $department->term_id;
			}

			return (int) $department;
		}, $departments );
	}

	$q_args = array(
		'post_type'      => 'notice',
		'post_status'    => 'publish',
		'posts_per_page' => $notice_count
	);

	// If there are at least two type of taxonomy that contains data, query relation should be AND
	if ( ( ! empty( $tags ) && ! empty( $categories ) ) ||
		 ( ! empty( $categories ) && ! empty( $departments ) ||
		   ( ! empty( $departments ) && ! empty( $tags ) ) ) ) {
		$q_args['tax_query']['relation'] = 'AND';
	}

	if ( ! empty( $tags ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'post_tags',
			'terms'    => $tags
		);
	}

	if ( ! empty( $categories ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'category',
			'terms'    => $categories
		);
	}

	if ( ! empty( $departments ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'department',
			'terms'    => $departments
		);
	}


	$notice_query = new WP_Query( $q_args );

	ob_start();
	if ( $notice_query->have_posts() ) :
		?>
		<ul class="list-group notice-shortcode">
			<?php while ( $notice_query->have_posts() ) : $notice_query->the_post(); ?>
				<li class="list-group-item">
				<span class="<?php echo 'badge' . ( 'on' == get_post_meta( get_the_ID(), '_important',
						true ) ? ' badge-color' : '' ) . ' pull-left'; ?>">
					<?php echo get_the_date( 'j' ); ?> <br> <?php echo get_the_date( 'M' ); ?>
				</span>
					<h1><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
					<p>
						<?php the_excerpt(); ?>
					</p>
				</li>
			<?php endwhile; ?>
			<li class="list-group-item">
				<p class="text-right clearfix">
					<a href="<?php echo get_post_type_archive_link( 'notice' ); ?>"
					   class="btn btn-custom waves-animate waves-ligh archive-btn"><?php _e( 'Archive',
							'leading-university' ); ?></a>
				</p>
			</li>
		</ul>
	<?php
	endif;
	wp_reset_postdata();

	return ob_get_clean();
} );

/**
 * Displays the latest news
 */
add_shortcode( 'lu_latest_news', function ( $atts ) {
	extract(
		shortcode_atts(
			array(
				'news_count'  => 5,
				'tags'        => array(),
				'categories'  => array(),
				'departments' => array()
			),
			$atts ) );

	if ( ! is_array( $tags ) ) {
		$tags = explode( ',', $tags );
		$tags = array_map( function ( $tag ) {
			$tag = trim( $tag );
			if ( ! is_numeric( $tag ) ) {
				$tag = get_term_by( 'slug', $tag, 'post_tag' );

				return $tag->term_id;
			}

			return (int) $tag;
		}, $tags );
	}

	if ( ! is_array( $categories ) ) {
		$categories = explode( ',', $categories );
		$categories = array_map( function ( $category ) {
			$category = trim( $category );
			if ( ! is_numeric( $category ) ) {
				$category = get_term_by( 'slug', $category, 'category' );

				return $category->term_id;
			}

			return (int) $category;
		}, $categories );
	}

	if ( ! is_array( $departments ) ) {
		$departments = explode( ',', $departments );
		$departments = array_map( function ( $department ) {
			$department = trim( $department );
			if ( ! is_numeric( $department ) ) {
				$department = get_term_by( 'slug', $department, 'department' );

				return $department->term_id;
			}

			return (int) $department;
		}, $departments );
	}

	$q_args = array(
		'post_type'      => 'news',
		'post_status'    => 'publish',
		'posts_per_page' => $news_count
	);

	if ( ( ! empty( $tags ) && ! empty( $categories ) ) ||
		 ( ! empty( $categories ) && ! empty( $departments ) ||
		   ( ! empty( $departments ) && ! empty( $tags ) ) ) ) {
		$q_args['tax_query']['relation'] = 'AND';
	}

	if ( ! empty( $tags ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'post_tags',
			'terms'    => $tags
		);
	}

	if ( ! empty( $categories ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'category',
			'terms'    => $categories
		);
	}

	if ( ! empty( $departments ) ) {
		$q_args['tax_query'][] = array(
			'taxonomy' => 'department',
			'terms'    => $departments
		);
	}


	$news_query = new WP_Query( $q_args );

	ob_start();
	if ( $news_query->have_posts() ) :
		?>
		<div class="latest-news container-fluid box">

			<?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
				<article class="media row">
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="media-left col-md-3 col-sm-4 col-xs-12">
							<?php the_post_thumbnail( 'thumbnail',
								array( 'class' => 'media-object img-responsive' ) ); ?>
						</div>
					<?php endif; ?>

					<div class="news-body <?php echo has_post_thumbnail() ? 'col-sm-8 col-md-9' : 'col-sm-12'; ?>">
						<?php echo sprintf( '<h1 class="media-heading"><a href="%s">%s</a></h1>', get_permalink(),
							get_the_title() ); ?>
						<p><?php the_excerpt(); ?></p>
						<a href="<?php the_permalink(); ?>"><?php _e( 'Read More...', 'leading-university' ); ?></a>
					</div>
				</article>
			<?php endwhile; ?>
			<div class="container-fluid">
				<button class="btn btn-custom archive-btn waves-effect waves-light">
					<a href="<?php echo get_post_type_archive_link( 'news' ); ?>"><?php _e( 'News Archive',
							'leading-university' ); ?></a>
				</button>
			</div>
		</div>
	<?php
	endif;
	wp_reset_postdata();

	return ob_get_clean();
} );

/**
 * Display tile for campus facility
 */
add_shortcode( 'lu_campus_facility', function ( $atts, $content ) {
	extract( shortcode_atts(
		array(
			'icon'  => 'star',
			'title' => ''
		),
		$atts
	) );

	ob_start();

	?>
	<div class="box text-center">
		<i class="fa fa-<?php echo $icon; ?> fa-4x small-margin"></i>
		<h3><?php echo $title; ?></h3>
		<p><?php echo $content; ?></p>
	</div>
	<?php

	return ob_get_clean();
} );

/**
 * Displays the latest news
 */
add_shortcode( 'lu_upcoming_events', function ( $atts ) {
	$events    = tribe_get_events( array( 'eventDisplay' => 'list' ) );
	$event_ids = array_map( function ( $e ) {
		return $e->ID;
	}, $events );
	$events    = array_merge( $events, tribe_get_events( array(
		'order'          => 'DESC',
		'post__not_in'   => $event_ids,
		'posts_per_page' => max( 0, 3 - sizeof( $events ) )
	) ) );

	extract( shortcode_atts( array(
		'cols_md' => 3,
		'cols_sm' => 2
	), $atts ) );
	ob_start();
	?>
	<?php if ( ! empty( $events ) ): ?>
		<div class="slick row" data-cols-md="<?php echo trim( $cols_md ); ?>"
			 data-cols-sm="<?php echo trim( $cols_sm ); ?>">
			<?php foreach ( $events as $event ): ?>
				<aside class="<?php echo strtotime( $event->EventEndDate ) < time() ? 'past-event' : ''; ?> col-sm-<?php echo (int) ( 12 / ( (int) $cols_sm ) ); ?> col-md-<?php echo (int) ( 12 / ( (int) $cols_md ) ); ?>">
					<div class="event box">
						<div class="event-thumb">
							<?php echo get_the_post_thumbnail( $event->ID, 'event-card-thumb',
								array( 'class' => 'img-responsive' ) ); ?>
						</div>
						<div class="caption">
					<span class="events-date">
						<?php echo tribe_get_start_date( $event->ID, false, 'jS F Y' ); ?>
					</span>
							<?php echo sprintf( '<h1 class="event-title"><a href="%s">%s</a></h1>',
								get_permalink( $event->ID ), get_the_title( $event->ID ) ); ?>
							<?php setup_postdata( $event ); ?>
							<p><?php echo get_the_excerpt(); ?></p>
							<?php wp_reset_postdata(); ?>
						</div>
					</div>
				</aside>
			<?php endforeach; ?>
		</div>
	<?php else : ?>
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<?php _e( 'No upcoming events scheduled so far.', 'leading-university' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php

	return ob_get_clean();
} );

/**
 * Display the class schedule
 */
add_shortcode( 'lu_class_schedule', function () {
	ob_start();
	?>
	<div class="class_schedule">
		<div class="row filter box-margin">
			<div class="col-md-12">
				<label><?php _e( 'Program', 'leading-university' ); ?></label>
				<?php
				$programs = get_posts( array(
					'post_type'      => 'program',
					'post_status'    => 'publish',
					'posts_per_page' => - 1
				) );

				$programs = array_combine(
					array_map( function ( $d ) {
						return $d->ID;
					}, $programs ),
					array_map( function ( $d ) {
						return $d->post_title;
					}, $programs )
				);
				lu_create_select_field( $programs, 0, array(
					'name'  => '',
					'id'    => '',
					'class' => 'selectpicker custom-dropdown-btn program-el'
				),
					true );
				?>
			</div>
			<div class="col-md-6">
				<label><?php _e( 'Section', 'leading-university' ); ?></label>
				<select class="selectpicker custom-dropdown-btn section-el">
				</select>
			</div>
			<div class="col-md-6">
				<label><?php _e( 'Room', 'leading-university' ); ?></label>
				<select class="selectpicker custom-dropdown-btn room-el">
				</select>
			</div>
			<div class="col-md-6">
				<label><?php _e( 'Course', 'leading-university' ); ?></label>
				<select class="selectpicker custom-dropdown-btn course-el">
				</select>
			</div>
			<div class="col-md-6">
				<label><?php _e( 'Teacher', 'leading-university' ); ?></label>
				<?php
				$teachers = get_users( array( 'role' => 'teacher' ) );

				$teachers = array_combine(
					array_map( function ( $d ) {
						return $d->ID;
					}, $teachers ),
					array_map( function ( $d ) {
						return $d->display_name;
					}, $teachers )
				);

				lu_create_select_field( $teachers, 0, array(
					'name'  => '',
					'id'    => '',
					'class' => 'selectpicker custom-dropdown-btn teacher-el'
				),
					true );
				?>
			</div>
		</div> <!-- End Filter -->

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 routine">
				<div class="calendar">
				</div>
			</div>
		</div>

		<?php if ( is_user_logged_in() ): ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-info">
						<?php
						$cal_link = site_url( wp_get_current_user()->user_login . '.ics' );
						printf(
							__( 'Your personal ical feed url: <b><a href="%s">%s</a></b>', 'leading-university' ),
							$cal_link, $cal_link );
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php
	return ob_get_clean();
} );

/**
 * Display the student registration form.
 */
add_shortcode( 'lu_student_registration_form', function ( $args, $content ) {
	if ( ! get_option( 'users_can_register' ) ) {
		return __( 'New user registration is currently disabled.', 'leading-university' ) .
			   ' <a href="' . wp_login_url() . '">' . __( 'Login', 'leading-university' ) . '</a>';
	}
	ob_start();
	?>
	<div ng-app="LURegister">
		<div ng-controller="StudentInfoCtrl">
			<form
					ng-show="!registered"
					ng-submit="register()"
					id="registration-form">
				<div class="row">
					<div class="input-field" ng-class="{'col-sm-5':!enable, 'col-sm-6':enable}">
						<input type="text" name="student_id" ng-model="studentID"><br/>
						<label><?php _e( 'Student ID', 'leading-university' ); ?></label>
					</div>
					<div class="input-field" ng-class="{'col-sm-4':!enable, 'col-sm-6':enable}">
						<input type="text" name="dob" ng-model="DOB"><br/>
						<label><?php _e( 'Date of Birth <small>(yyyy-mm-dd)</small>', 'leading-university' ); ?></label>
					</div>
					<div class="col-sm-3">
						<button type="submit" id="fetch_s_info" ng-if="!enable"
								class="btn btn-custom waves-effect waves-light"><?php _e( 'Continue',
								'leading-university' ); ?></button>
					</div>
				</div>
				<div class="row" ng-show="enable">
					<div class="col-sm-12 input-field">
						<input type="text" name="student_name" disabled="disabled" ng-model="name"><br/>
						<label ng-class="{active: enable}"><?php _e( 'Name', 'leading-university' ); ?></label>
					</div>
				</div>
				<div class="row" ng-show="enable">
					<div class="col-sm-12 input-field">
						<input type="email" name="email" ng-model="email"><br/>
						<label><?php _e( 'Email', 'leading-university' ); ?></label>
						<small><?php _e( 'You cannot activate your account without it being correct.',
								'leading-university' ); ?></small>
					</div>
				</div>
				<div class="row" ng-if="enable">
					<div class="col-sm-12">
						<button type="submit" id="register"
								class="btn btn-custom waves-effect waves-light"><?php _e( 'Register',
								'leading-university' ); ?></button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<br>
						<div class="alert alert-danger" ng-if="hasError">{{errorData.message}}</div>
					</div>
				</div>
			</form>
			<div ng-if="registered" class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<?php
						_e(
							'You have successfully registered as a student. A link to set your password has been sent to your email address. Please check spam if you did not receive the email.',
							'leading-university' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<a href="<?php echo wp_login_url(); ?>"
	   title="<?php _e( 'Login', 'leading-university' ); ?>"><?php _e( 'Login', 'leading-university' ); ?></a>
	<?php
	return ob_get_clean();
} );

/**
 * Display the login form.
 */
add_shortcode( 'lu_login_form', function ( $args ) {

	if ( is_user_logged_in() ) {
		return sprintf( __( 'You are already logged in. <a href="%s">Log out?</a>', 'leading-university' ),
			wp_logout_url( get_permalink() ) );
	}

	$defaults = array(
		'redirect'       => ( ! isset( $_GET['redirect'] ) || $_GET['redirect'] == '' ) ? get_bloginfo( 'url' ) : urldecode( $_GET['redirect'] ),
		// Default redirect is back to the current page
		'form_id'        => 'loginform',
		'label_username' => __( 'Username' ),
		'label_password' => __( 'Password' ),
		'label_remember' => __( 'Remember Me' ),
		'label_log_in'   => __( 'Log In' ),
		'id_username'    => 'user_login',
		'id_password'    => 'user_pass',
		'id_remember'    => 'rememberme',
		'id_submit'      => 'wp-submit',
		'remember'       => true,
		'value_username' => '',
		'value_remember' => false,
		// Set this to true to default the "Remember me" checkbox to checked
	);

	/**
	 * Filter the default login form output arguments.
	 *
	 * @param  array  $defaults  An array of default login form arguments.
	 *
	 * @see wp_login_form()
	 *
	 * @since 3.0.0
	 *
	 */
	$args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );

	/**
	 * Filter content to display at the top of the login form.
	 *
	 * The filter evaluates just following the opening form tag element.
	 *
	 * @param  string  $content  Content to display. Default empty.
	 * @param  array  $args  Array of login form arguments.
	 *
	 * @since 3.0.0
	 *
	 */
	$login_form_top = apply_filters( 'login_form_top', '', $args );

	/**
	 * Filter content to display in the middle of the login form.
	 *
	 * The filter evaluates just following the location where the 'login-password'
	 * field is displayed.
	 *
	 * @param  string  $content  Content to display. Default empty.
	 * @param  array  $args  Array of login form arguments.
	 *
	 * @since 3.0.0
	 *
	 */
	$login_form_middle = apply_filters( 'login_form_middle', '', $args );

	/**
	 * Filter content to display at the bottom of the login form.
	 *
	 * The filter evaluates just preceding the closing form tag element.
	 *
	 * @param  string  $content  Content to display. Default empty.
	 * @param  array  $args  Array of login form arguments.
	 *
	 * @since 3.0.0
	 *
	 */
	$login_form_bottom = apply_filters( 'login_form_bottom', '', $args );

	$form = '
		<div ng-app="LULogin" ng-controller="LoginCTRL">
		<form name="' . $args['form_id'] . '" id="' . $args['form_id'] . ' method="post" ng-submit="login()">
		' . $login_form_top . '
			<p class="login-username input-field">
				<input type="text" ng-model="username" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" /><br />
				<label for="' . esc_attr( $args['id_username'] ) . '">' . __( 'Student ID or',
			'leading-university' ) . ' ' . esc_html( $args['label_username'] ) . '</label>
			</p>
			<p class="login-password input-field">
				<input type="password" ng-model="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" /><br />
				<label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '</label>
			</p>
			<br>
			<div class="alert alert-danger" ng-if="hasError" ng-bind-html="errorData"></div>
			' . $login_form_middle . '
			' . ( $args['remember'] ? '<p class="login-remember"><input name="rememberme" ng-model="rememberme" type="checkbox" id="' . esc_attr( $args['id_remember'] ) . '" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /> <label for="' . esc_attr( $args['id_remember'] ) . '">' . esc_html( $args['label_remember'] ) . '</label></p>' : '' ) . '
			<p class="login-submit">
				<button type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="btn btn-custom waves-effect waves-light">' . esc_attr( $args['label_log_in'] ) . '</button>
				<input type="hidden" name="redirect_to" ng-model="redirect_to" ng-init="redirect_to=\'' . esc_url( $args['redirect'] ) . '\'" />
			</p>
		' . $login_form_bottom . '
	</form>
	</div>
	<br>
	<a href="' . wp_lostpassword_url( get_permalink() ) . '" title=".' . __( 'Lost Password' ) . '">' . __( 'Lost Password' ) . '</a> | 
	<a href="' . wp_registration_url() . '" title=".' . __( 'Register' ) . '">' . __( 'Register' ) . '</a>
	';

	return $form;
} );

/**
 * Display change password form.
 */
add_shortcode( 'lu_change_password_form', function ( $args ) {

	if ( ! is_user_logged_in() ) {
		return sprintf( __( 'You are currently not <a href="%s">logged in</a>.', 'leading-university' ),
			wp_login_url( get_permalink() ) );
	}

	$redirect = ( ! isset( $_GET['redirect'] ) || $_GET['redirect'] == '' ) ? get_bloginfo( 'url' ) : urldecode( $_GET['redirect'] ); // Default redirect is back to the current page

	$form = '
		<div ng-app="LUChangePassword" ng-controller="ChangePasswordCTRL" ng-init="nonce=\'' . wp_create_nonce( 'lu_change_password' ) . '\'">
		<div ng-if="done" class="alert alert-success">' . sprintf( __( 'Your password has been changed. You will need to <a href="%s">log in</a> again.' ),
			wp_login_url() ) . '</div>
		<form ng-submit="change_password()" ng-show="!done">
			<p class="current_password input-field">
				<input type="password" ng-model="current_password" id="current-password" /><br />
				<label for="current-password">' . __( 'Current Password', 'leading-university' ) . '</label>
			</p>
			<p class="new_password input-field">
				<input type="password" ng-model="new_password" id="new-password" /><br />
				<label for="new-password">' . __( 'New Password', 'leading-university' ) . '</label>
			</p>
			<p class="confirm_password input-field">
				<input type="password" ng-model="confirm_password" id="confirm-password" /><br />
				<label for="confirm-password">' . __( 'Confirm New Password', 'leading-university' ) . '</label>
			</p>
			<br>
			<div class="alert alert-danger" ng-if="hasError" ng-bind-html="errorData"></div>

			<p>
				<button type="submit" class="btn btn-custom waves-effect waves-light">' . __( 'Change Password',
			'leading-university' ) . '</button>
				<input type="hidden" name="redirect_to" ng-model="redirect_to" ng-init="redirect_to=\'' . $redirect . '\'" />
			</p>
			
	</form>
	</div>
	';

	return $form;
} );

/**
 * Display forgot password form.
 */
add_shortcode( 'lu_forgot_password_form', function ( $attributes, $content = null ) {
	// Parse shortcode attributes

	if ( is_user_logged_in() ) {
		return __( 'You are already signed in.', 'leading-university' );
	}

	if ( isset( $_GET['checkemail'] ) && $_GET['checkemail'] == 'confirm' ) {
		return '<div class="alert alert-success">' . __( 'Check your email for a link to reset your password.',
				'leading-university' ) . '</div>';
	}

	ob_start();

	$errors = array(
		'empty_username' => __( 'You need to enter your email address or username to continue.', 'leading-university' ),
		'invalid_email'  => __( 'This email address is not registered with any of the accounts.',
			'leading-university' ),
		'invalidcombo'   => __( 'There is no user found with the specified username.', 'leading-university' ),
		'expiredkey'     => __( 'The link has been expired. Please try resetting again.', 'leading-university' ),
		'invalidkey'     => __( 'Your password reset link appears to be invalid. Please request a new link',
			'leading-university' ),
	);
	?>
	<form id="lostpasswordform" method="post" action="<?php echo wp_lostpassword_url(); ?>">
		<div class="form-row row">
			<div class="col-sm-9 col-lg-10 input-field">
				<input type="text" name="user_login" id="user_login"/> <br/>
				<label for="user_login"><?php _e( 'Email', 'personalize-login' ); ?>
			</div>

			<div class="col-sm-3 col-lg-2 text-right">
				<button type="submit"
						class="btn btn-custom waves-effect waves-light"><?php _e( 'Reset',
						'leading-university' ); ?></button>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php
				if ( isset( $_REQUEST['errors'] ) ) {
					$error_codes = explode( ',', $_REQUEST['errors'] );

					foreach ( $error_codes as $error_code ) {
						$error_messages[] = $errors[ $error_code ];
					}
					?>
					<div class="alert alert-danger">
						<?php foreach ( $error_messages as $error ) : ?>
							<p>
								<?php echo $error; ?>
							</p>
						<?php endforeach; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</form>
	<a href="<?php echo wp_login_url(); ?>" title="<?php _e( 'Login' ); ?>"><?php _e( 'Login' ); ?></a> |
	<a href="<?php echo wp_registration_url(); ?>" title="<?php _e( 'Register' ); ?>"><?php _e( 'Register' ); ?></a>
	<?php

	return ob_get_clean();
} );

/**
 * Display the reset password form.
 */
add_shortcode( 'lu_reset_password_form', function () {

	$errors = array(
		'password_reset_mismatch' => __( "The two passwords you entered don't match.", 'leading-university' ),
		'password_reset_empty'    => __( "Sorry, we don't accept empty passwords.", 'leading-university' ),
	);

	if ( is_user_logged_in() ) {
		return __( 'You are already signed in.', 'leading-university' );
	} else {
		if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
			$login = $_REQUEST['login'];
			$key   = $_REQUEST['key'];

			// Error messages
			$error_messages = array();
			if ( isset( $_REQUEST['error'] ) ) {
				$error_codes = explode( ',', $_REQUEST['error'] );

				foreach ( $error_codes as $code ) {
					$error_messages[] = $errors[ $code ];
				}
			}
			ob_start();
			?>
			<form name="resetpassform" id="resetpassform"
				  action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
				<input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $login ); ?>"
					   autocomplete="off"/>
				<input type="hidden" name="rp_key" value="<?php echo esc_attr( $key ); ?>"/>

				<?php if ( count( $error_messages ) > 0 ) : ?>
					<div class="alert alert-danger">
						<?php foreach ( $error_messages as $error ) : ?>
							<p>
								<?php echo $error; ?>
							</p>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<div class="input-field">
					<input type="password" name="pass1" id="pass1" class="input" size="20" value=""
						   autocomplete="off"/><br/>
					<label for="pass1"><?php _e( 'New password', 'leading-university' ) ?></label>
				</div>
				<div class="input-field">
					<input type="password" name="pass2" id="pass2" class="input" size="20" value=""
						   autocomplete="off"/><br/>
					<label for="pass2"><?php _e( 'Repeat new password', 'leading-university' ) ?></label>
				</div>

				<div class="alert alert-info"><?php echo wp_get_password_hint(); ?></div>

				<div class="text-center">
					<button type="submit" name="submit" id="resetpass-button"
							class="btn btn-custom waves-effect waves-light"><?php _e( 'Reset Password',
							'leading-university' ); ?></button>
				</div>
			</form>

			<?php
			return ob_get_clean();

		} else {
			return __( 'Invalid password reset link.', 'leading-university' );
		}
	}
} );

/**
 * Semester registration form.
 */
add_shortcode( 'lu_semester_registration_form', function ( $args, $content ) {
	ob_start();

	lu_semester_registration_form();

	return ob_get_clean();
} );

/**
 * Output the front end registration form.
 */
function lu_semester_registration_form() {
	$user_id        = get_current_user_id();
	$program_id     = get_user_meta( $user_id, 'program', true );
	$program        = get_post( $program_id );
	$current_reg_id = lu_get_student_registration_id( get_user_meta( $user_id, 'student_id', true ) );
	if ( $current_reg_id ) {
		$current_reg = get_post( $current_reg_id );
	} else {
		$current_reg = false;
	}

	if ( ! is_user_logged_in() ):
		echo '<div class="box">' . sprintf( __( 'You must be <a href="%s">logged in</a> to use this form.',
				'leading-university' ), wp_login_url( get_permalink() ) ) . '</div>';
	elseif ( ! lu_is_student() ):
		echo '<div class="box">' . __( 'You must be a student to access this page.', 'leading-university' ) . '</div>';
	elseif ( 'on' != get_post_meta( $program_id, '_registration_open',
			true ) && ( ! $current_reg || ! in_array( $current_reg->post_status, array(
				'review',
				'approved',
				'registered'
			) ) ) ):
		echo '<div class="box">' . sprintf( __( 'Registration for %s is currently closed.', 'leading-university' ),
				$program->post_title ) . '</div>';
	else : ?>

		<form ng-app="editRegistrationApp" ng-controller="RegistrationCTRL" action="<?php echo get_page_link(); ?>"
			  method="post">
			<!--Semester Selection-->
			<div class="row hidden-print" ng-if="!freez_registration">
				<div class="col-sm-9 filter-title">
					<h3><?php _e( 'Semester (&amp; Section)', 'leading-university' ); ?></h3>
				</div>
				<div class="col-sm-3 filter-title">
					<select class="selectpicker custom-dropdown-btn spare" ng-model="main_section"
							ng-change="registerSectionCourses(main_section)">
						<option ng-repeat="section in sections()">{{section}}</option>
					</select>
				</div>
			</div>
			<!--End of Semester Selection-->

			<!--New Course-->
			<div class="row new-course hidden-print" ng-if="!freez_registration">
				<div class="col-sm-12">
					<h3><?php _e( 'Add Course', 'leading-university' ); ?></h3>
				</div>
				<div class="col-sm-8 col-md-7 col-lg-8">
					<select
							class="selectpicker custom-dropdown-btn spare"
							ng-options="(course.code + ' : ' + course.title + (course.gpa>=0?' (GP ' + course.gpa + ')':' (New)')) disable when ( course.registered || !course.retakable ) for course in courses track by course.ID"
							ng-model="newCourse">
					</select>
				</div>
				<div class="col-sm-2 col-md-2 col-lg-2">
					<select
							class="selectpicker custom-dropdown-btn spare"
							ng-options="sec.name as sec.name for sec in newCourse['sections']"
							ng-model="newSection">
					</select>
				</div>
				<div class="col-sm-2 col-md-3 col-lg-2 text-right">
					<button type="button" ng-click="addCourse(newCourse, newSection)"
							class="btn btn-custom-alt add waves-effect waves-light"><?php _e( 'Add',
							'leading-university' ); ?></button>
				</div>
			</div>
			<!--End of New Course-->

			<!--Regular Courses-->
			<div class="row" ng-if="hasRegularCourses()">
				<div class="col-sm-12">
					<h3><?php _e( 'Regular Courses', 'leading-university' ); ?></h3>
					<div class="table-responsive">
						<table class="table table-bordered registered-courses-table">
							<thead>
							<th><?php _e( 'Course Title', 'leading-university' ); ?></th>
							<th><?php _e( 'Course Code', 'leading-university' ); ?></th>
							<th><?php _e( 'Credit', 'leading-university' ); ?></th>
							<th><?php _e( 'Section', 'leading-university' ); ?></th>
							</thead>
							<tbody>
							<tr ng-repeat="course in courses | toArray | filter: { registered: true, type: 'regular' }">
								<td class="title-remove">
									<div class="container-fluid">
										<div class="row">
											<div class="course-title col-xs-10">
												{{course.title}}
											</div>

											<div class="course-remove col-xs-2 hidden-print"
												 ng-if="!freez_registration">
												<button type="button" class="close" aria-label="Close"
														aria-hidden="true" ng-click="removeCourse(course.ID)">
													<span class="label label-danger">&times;</span>
												</button>
											</div>
										</div>
									</div>
								</td>

								<td class="sub-code">
									{{course.code}}
								</td>
								<td class="sub-credit">
									{{course.credit}}
								</td>
								<td class="sub-section">
									<select
											class="selectpicker custom-dropdown-btn spare"
											ng-disabled="freez_registration"
											ng-options="sec.name as sec.name for sec in course.sections"
											ng-model="course.reged_section">
									</select>
									<input type="hidden" name="course[regular][{{$index}}][ID]" value="{{course.ID}}">
									<input type="hidden" name="course[regular][{{$index}}][section]"
										   value="{{course.reged_section}}">
								</td>
							</tr>
							</tbody>
							<tfoot>
							<tr>
								<td class="invisible"></td>
								<td class="sub-code">
									<?php _e( 'Total Credit', 'leading-university' ); ?>
								</td>
								<td class="sub-code total-credit total-credit" colspan="2">
									{{creditCount('regular')}}
								</td>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!--End of Regular Courses-->

			<!--Retake Courses-->
			<div class="row" ng-if="hasRetakeCourses()">
				<div class="col-sm-12">
					<h3><?php _e( 'Retake Courses', 'leading-university' ); ?></h3>
					<div class="table-responsive">
						<table class="table table-bordered registered-courses-table">
							<thead>
							<th><?php _e( 'Course Title', 'leading-university' ); ?></th>
							<th><?php _e( 'Course Code', 'leading-university' ); ?></th>
							<th><?php _e( 'Credit', 'leading-university' ); ?></th>
							<th><?php _e( 'Section', 'leading-university' ); ?></th>
							</thead>
							<tbody>
							<tr ng-repeat="course in courses | toArray | filter: { registered: true, type: 'retake' }">
								<td class="title-remove">
									<div class="container-fluid">
										<div class="row">
											<div class="course-title col-xs-10">
												{{course.title}}
											</div>

											<div class="course-remove col-xs-2 hidden-print"
												 ng-if="!freez_registration">
												<button type="button" class="close" aria-label="Close"
														aria-hidden="true" ng-click="removeCourse(course.ID)">
													<span class="label label-danger">&times;</span>
												</button>
											</div>
										</div>
									</div>
								</td>

								<td class="sub-code">
									{{course.code}}
								</td>
								<td class="sub-credit">
									{{course.credit}}
								</td>
								<td class="sub-section">
									<select
											class="selectpicker custom-dropdown-btn spare"
											ng-disabled="freez_registration"
											ng-options="sec.name as sec.name for sec in course.sections"
											ng-model="course.reged_section">
									</select>
									<input type="hidden" name="course[retake][{{$index}}][ID]" value="{{course.ID}}">
									<input type="hidden" name="course[retake][{{$index}}][section]"
										   value="{{course.reged_section}}">
								</td>
							</tr>
							</tbody>
							<tfoot>
							<tr>
								<td class="invisible"></td>
								<td class="sub-code">
									<?php _e( 'Total Credit', 'leading-university' ); ?>
								</td>
								<td class="sub-code total-credit total-credit" colspan="2">
									{{creditCount('retake')}}
								</td>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!--End of Retake Courses-->

			<!--Registration Details-->
			<div class="row">
				<div class="col-sm-12">
					<h3><?php _e( 'Registration Summary', 'leading-university' ); ?></h3>
					<div class="container-fluid">
						<div class="row">
							<div class="registration-snippet col-md-4 hidden-print">
								<span class="title"><?php _e( 'Regular Credits', 'leading-university' ); ?></span> <br>
								<span class="figure">{{creditCount( 'regular' )}}</span>
							</div>
							<div class="registration-snippet col-md-4 hidden-print">
								<span class="title"><?php _e( 'Retake Credits', 'leading-university' ); ?></span> <br>
								<span class="figure">{{creditCount( 'retake' )}}</span>
							</div>
							<div class="registration-snippet col-md-4">
								<span class="title"><?php _e( 'Total Credits', 'leading-university' ); ?></span> <br>
								<span class="figure">{{creditCount()}}</span>
							</div>
							<div class="registration-snippet col-md-4 hidden-print">
								<span class="title"><?php _e( 'Tution Fees', 'leading-university' ); ?></span> <br>
								<span class="figure">{{tutionFees() | currency : '৳ ' : 2}}</span>
							</div>
							<div class="registration-snippet col-md-4 hidden-print">
								<span class="title"><?php _e( 'Other Fees', 'leading-university' ); ?></span> <br>
								<span class="figure">{{other_fees | currency : '৳ ' : 2}}</span>
							</div>
							<div class="registration-snippet col-md-4">
								<span class="title"><?php _e( 'Total Fees', 'leading-university' ); ?></span> <br>
								<span class="figure">{{totalFees() | currency : '৳ ' : 2}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--End of Registration Details-->

			<div class="row hidden-print">
				<?php
				$user_id    = get_current_user_id();
				$student_id = get_user_meta( $user_id, 'student_id', true );
				$reg_id     = lu_get_student_registration_id( $student_id );

				?>
				<div class="col-sm-12" ng-if="false != error">
					<div class="alert alert-danger">{{error}}</div>
				</div>
				<div class="col-sm-12">
					<div class="alert alert-info">
						<?php _e( 'Waiver of {{waiver}}% applied to the tution fees of regular courses.',
							'leading-university' ); ?>
						<span ng-if="registration_status">
						<?php _e( 'Current registration status: <b>{{registration_status}}</b>.',
							'leading-university' ); ?>
					</span>
					</div>
				</div>
				<div class="col-sm-12" ng-if="creditExceed()">
					<div class="alert alert-danger"><?php _e( 'You can only take at most {{credit_limit}} credits per semester.',
							'leading-university' ); ?></div>
				</div>
				<div class="col-sm-12 text-center">
					<button type="submit" name="status" value="submitted"
							class="btn btn-custom waves-effect waves-light"
							ng-disabled="freez_registration||creditExceed()">
						<?php
						$reg_id == 0 ? _e( 'Submit', 'leading-university' ) : _e( 'Update', 'leading-university' );
						?>
					</button>
				</div>
				<?php wp_nonce_field( 'save_registration', 'nonce' ); ?>
			</div>
		</form>
	<?php
	endif;
}

/**
 * Shortcode should run for each campus in the map
 */
add_shortcode( 'lu_campus', function ( $args, $content ) {
	extract(
		shortcode_atts(
			array(
				'latitude'  => 0,
				'longitude' => 0,
				'title'     => '',
				'class'     => 'campus col-sm-3 text-center',
				'image_url' => '',
				'image_id'  => 0
			),
			$args
		)
	);

	ob_start();
	?>
	<div class="<?php echo $class; ?>" data-lat="<?php echo $latitude; ?>" data-lng="<?php echo $longitude; ?>">
		<div class="thubnail-cus">
			<?php if ( $image_id ): ?>
				<?php echo wp_get_attachment_image( $image_id, 'campus-map-thumb', false,
					array( 'class' => 'hidden-xs img-responsive' ) ); ?>
			<?php else: ?>
				<img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?> Image" class="hidden-xs img-responsive">
			<?php endif; ?>
			<div class="mapcampustitle"><?php echo $title; ?></div>
		</div>
		<address>
			<?php echo $content; ?>
		</address>
	</div>
	<?php

	return ob_get_clean();
} );

/**
 * Create shortcode for campus map
 */
add_shortcode( 'lu_campus_map', function ( $args, $content ) {
	ob_start();

	echo '<div id="campus-map">';
	echo do_shortcode( $content );
	?>
	<!-- Google Map  -->
	<div class="googlemap-location col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<div id="map"></div>
		</div>
	</div>
	<?php
	echo '</div>';

	return ob_get_clean();
} );


/**
 * Return a list of teachers based on department.
 *
 * @param  int  $department  id of the department
 *
 * @return array Array of users.
 */
function lu_get_department_teachers( $department = 0 ) {
	$args = array(
		'role'     => 'teacher',
		'meta_key' => 'order',
		'orderby'  => 'meta_value',
		'order'    => 'ASC'
	);

	if ( 0 != $department ) {
		$args['meta_query'] = array(
			'key'   => 'department',
			'value' => $department
		);
	} else {
		$args['meta_query'] = array(
			'key'   => 'department',
			'value' => ''
		);
	}

	$query = new WP_User_Query( $args );

	$users = $query->get_results();

	return $users;
}

/**
 * Shortcode for displaying list of faculty members of a department.
 */
add_shortcode( 'lu_faculty_list', function ( $atts ) {
	extract( shortcode_atts(
		array(
			'department' => 0
		),
		$atts
	) );

	$users = lu_get_department_teachers( $department );

	ob_start();
	?>
	<div class="row">
		<?php foreach ( $users as $teacher ) : ?>
			<div class="col-sm-6 col-xs-12 teacher-info">
				<div class="row">
					<div class="teacher-img col-xs-4">
						<?php
						if ( function_exists( 'get_wp_user_avatar' ) ) {
							echo get_wp_user_avatar( $teacher->ID, 200, null, $teacher->data->display_name );
						} else {
							echo get_avatar( $teacher->ID, 200, null, $teacher->data->display_name );
						}
						?>
					</div>
					<div class="col-xs-8">
						<h2>
							<a href="<?php echo get_author_posts_url( $teacher->ID ); ?>"><?php echo $teacher->data->display_name; ?></a>
						</h2>
						<p><?php
							echo get_user_meta( $teacher->ID, 'designation', true );

							echo '<br>';

							$additional_role = trim( get_user_meta( $teacher->ID, 'additional_role', true ) );
							if ( '' != $additional_role ) {
								echo "$additional_role <br>";
							}
							?>
						</p>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php
	return str_replace( array( "\r", "\n" ), '', ob_get_clean() );
} );

/**
 * Result page content.
 */
add_shortcode( 'lu_results', function () {
	ob_start();
	?>
	<div ng-app="LUResults">
		<form
				id="result-form"
				method="POST"
				class="hidden-print"
				ng-controller="ResultFormCtrl"
				ng-submit="updateResult(studentID, DOB)"
				ng-init="updateResult(current_student_id)">

			<div class="row">
				<?php if ( current_user_can( 'view_results' ) ): ?>
					<div class="input-field col-sm-8">
						<input type="text" id="student_id" ng-model="studentID" name="student_id"><br/>
						<label for="student_id"><?php _e( 'Student ID', 'leading-university' ); ?></label>
					</div>
					<div class="col-sm-4">
						<button class="btn btn-custom waves-effect waves-light pull-right" type="submit" name="action">
							<?php _e( 'Fetch Results', 'leading-university' ); ?>
						</button>
					</div>
				<?php else : ?>
					<div class="input-field"
						 ng-class="{'col-sm-4': studentID.trim() != current_student_id, 'col-sm-8': studentID.trim() == current_student_id}">
						<input type="text" id="student_id" ng-model="studentID" name="student_id"><br/>
						<label for="student_id"><?php _e( 'Student ID', 'leading-university' ); ?></label>
					</div>
					<div class="input-field col-sm-4" ng-show="studentID.trim() != current_student_id">
						<input type="text" id="dob" ng-model="DOB" name="dob"><br/>
						<label for="dob"><?php _e( 'Date of Birth (Y-m-d)', 'leading-university' ); ?></label>
					</div>
					<div class="col-sm-4">
						<button class="btn waves-effect waves-light pull-right" type="submit" name="action">
							<?php _e( 'Fetch Results', 'leading-university' ); ?>
						</button>
					</div>

				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-danger" ng-if="hasError">{{errorData.message}}</div>
				</div>
			</div>
		</form>
		<div id="student-info" ng-controller="StudentInfoCtrl">
			<div class="row" ng-if="enable">
				<div class="col-sm-6">
					<strong><?php _e( 'Student ID :', 'leading-university' ); ?></strong>
					{{std.id}}
				</div>
				<div class="col-sm-6">
					<strong><?php _e( 'Semester :', 'leading-university' ); ?></strong>
					{{std.semester}}
				</div>
				<div class="col-sm-3">
					<strong><?php _e( 'CGPA :', 'leading-university' ); ?></strong>
					{{std.cgpa}}
				</div>
				<div class="col-sm-3">
					<strong><?php _e( 'Grade :', 'leading-university' ); ?></strong>
					{{std.grade}}
				</div>
				<div class="col-sm-6">
					<strong><?php _e( 'Credit Completed :', 'leading-university' ); ?></strong>
					{{std.credit}}
				</div>
				<div class="col-sm-12">
					<strong><?php _e( 'Name :', 'leading-university' ); ?></strong>
					{{std.name}}
				</div>
				<div class="col-sm-12">
					<strong><?php _e( 'Program :', 'leading-university' ); ?></strong>
					{{std.degree}}
				</div>
				<div class="col-sm-12">
					<strong><?php _e( 'Department :', 'leading-university' ); ?></strong>
					{{std.department}}
				</div>
				<div class="col-sm-12" ng-if="std.waived_credit">
					<strong><?php _e( 'Waived Credits :', 'leading-university' ); ?></strong>
					{{std.waived_credit}}
				</div>
			</div>
		</div>
		<div id="results" class="container-fluid" ng-controller="ResultsCtrl">
			<div class="row" ng-if="enable">
				<div ng-repeat="(year, semesters) in r" class="row">
					<div ng-repeat="semester in semesters" class="col-sm-12 semester">
						<h2>{{semester.name}}</h2>
						<table class="table">
							<thead>
							<tr>
								<th><?php _e( 'Course Code', 'leading-university' ); ?></th>
								<th class="hidden-xs show-print"><?php _e( 'Course Title',
										'leading-university' ); ?></th>
								<th><?php _e( 'Credit', 'leading-university' ); ?></th>
								<th><?php _e( 'GP', 'leading-university' ); ?></th>
								<th><?php _e( 'Grade', 'leading-university' ); ?></th>
							</tr>
							</thead>
							<tfoot>
							<tr>
								<th></th>
								<th class="hidden-xs show-print"></th>
								<th>{{semester.credit}}</th>
								<th>{{semester.gpa}}</th>
								<th>{{semester.grade}}</th>
							</tr>
							</tfoot>
							<tbody>
							<tr ng-repeat="course in semester.courses">
								<td>{{course.course_code}}</td>
								<td ng-if="course.grade!='EI'" class="hidden-xs show-print course-title">
									{{course.course_title}}
								</td>
								<td ng-if="course.grade!='EI'">{{course.credit}}</td>
								<td ng-if="course.grade!='EI'">{{course.gpa}}</td>
								<td ng-if="course.grade!='EI'">{{course.grade}}</td>
								<td ng-if="course.grade=='EI'" colspan="4">
									<?php _e( '<a href="{{course.evaluation_link}}" target="_blank">Complete teacher\'s evaluation</a> to view result.' ); ?>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php

	return ob_get_clean();
} );

/**
 * The evaluation form for student's end.
 */
add_shortcode( 'lute_evaluation_form', function ( $atts, $content ) {
	ob_start();

	include "evaluation-form.php";

	return ob_get_clean();
} );
