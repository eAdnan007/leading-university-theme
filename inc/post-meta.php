<?php
/**	
 * Save lu specific metadata when post is being saved.
 */
function lu_save_post( $post_id ){
	global $dept_based_cap_post_types;
	remove_action( 'save_post', __FUNCTION__ );
	
	$post = get_post( $post_id );
	switch( $post->post_type ){
		case 'syllabus':
			lu_save_syllabus( $post );
			break;
		case 'program':
			lu_save_program( $post );
			break;
		case 'notice':
			lu_save_notice( $post );
			break;
		case 'registration':
			lu_save_registration( $post );
	}
	
	$post_type = get_post_type_object( $post->post_type );
	if( in_array( $post->post_type, $dept_based_cap_post_types ) && !current_user_can( $post_type->cap->delete_others_posts ) ){
		$user_dept = trim( get_user_meta( get_current_user_id(), 'department', true ) );
		$user_dept = get_term( $user_dept, 'department' );
		
		if( null != $user_dept ){
			wp_set_post_terms( $post_id, $user_dept->name, 'department' );
			
			if( 'notice' ==  $post->post_type && 'off' != get_post_meta( $post_id, '_hide_in_widget', true ) ){
				update_post_meta( $post_id, '_hide_in_widget', 'on' );
			}
		}
		else {
			$post->post_status = 'draft';
			wp_update_post( $post );
		}
	}
	
	
	add_action( 'save_post', __FUNCTION__ );
}
add_action( 'save_post', 'lu_save_post' );

/**
 * Delete courses associated with a syllabus when the syllabus has been deleted.
 */
add_action( 'before_delete_post', function( $post_id ){
	global $wpdb;
	
	$post = get_post( $post_id );
	
	if( 'syllabus' == $post->post_type ){
		$wpdb->delete(
			$wpdb->prefix.'courses',
			array(
				'syllabus' => $post_id ) );
	}
});

/**
 * Save metadata for notice.
 */
function lu_save_notice( $post ){
	if( !isset( $_POST['lu_nonce'] ) || !wp_verify_nonce( $_POST['lu_nonce'], 'save_notice' ) ) return;
	
	$atts = array( 'important', 'hide_in_widget' );
	foreach( $atts as $meta_key ){
		if( isset( $_POST['notice_meta'][ $meta_key ] ) )
			$meta_value = $_POST['notice_meta'][ $meta_key ];
		else
			$meta_value = 'off';
			
		update_post_meta( $post->ID, "_$meta_key", $meta_value );
	}
}

/**
 * Handles registration status changing to block any unauthorised changes.
 */
add_action( 'transition_post_status', function( $new_status, $old_status, $post ){
	$approve_stats = array( 'draft', 'submitted', 'review' );
	$student_id    = get_user_meta( get_current_user_id(), 'student_id', true );
	$program       = get_user_meta( get_current_user_id(), 'program', true );
	
	if( 
		'' != $student_id && 
		'' != $program ) return;
	
	if( 
		(
			(
				// If the user doesn't have previlage to approve but trying change status related to it
				in_array( $new_status, $approve_stats ) ||
				in_array( $old_status, $approve_stats )
			) && 
			!current_user_can( 'approve_registration' )
		)
		||
		(	// If the user doesn't have previlage to register but trying to register an unregistered or unregister a registered
			( 'registered' == $new_status || 'registered' == $old_status ) && 
			!current_user_can( 'confirm_registration' )
		)
	){
		$post->post_status = $old_status;
		wp_update_post( $post );
	}
	
	if( !in_array( $post->post_status, array( 'submitted', 'review', 'approved', 'registered' ) ) ) return;
	
	$statuses       = get_post_stati( array(), 'objects' );
	$status         = $statuses[ $post->post_status ]->label;
	$student_id     = get_post_meta( $post->ID, 'student_id', true );
	$semester       = get_term( get_post_meta( $post->ID, 'semester', true ), 'semester' )->name;
	$user           = lu_get_student_user( $student_id );
	$student_name   = $user->data->display_name;
	$student_email  = $user->data->user_email;
	
	global $lu_config;
	
	$message = $lu_config['registration_status_message'];
	$message = str_replace(
		array(
			'{status}',
			'{semester}',
			'{student_name}',
			'{student_id}' ),
		array(
			$status,
			$semester,
			$student_name,
			$student_id ),
		$message );
	
	add_filter( 'wp_mail_content_type', function( $content_type ) {
		return 'text/html';
	});
	
	wp_mail( $student_email, '[' . get_bloginfo( 'title' ) . '] ' . __( 'Registration Status Change', 'leading-university' ), wpautop( $message ) );

	add_filter( 'wp_mail_content_type', function( $content_type ) {
		return 'text/plain';
	});
	
}, 10, 3);

/**
 * Save metadata for registration
 */
function lu_save_registration( $post ){
	if( !isset( $_POST['lu_nonce'] ) || !wp_verify_nonce( $_POST['lu_nonce'], 'save_registration' ) ) return;
	
	/*
	 * Check if it is possible to get previous results.
	 */
	if( is_wp_error( lu_fetch_result( $_POST['registration_meta']['student_id'] ) ) )
		wp_die( __( 'Unable to communicate with exam server.', 'leading-university' ) );
	
	global $wpdb;
	
	$existing_registrations = get_posts( array(
		'posts_per_page' => 1,
		'post_type' => 'registration',
		'post__not_in' => array( $post->ID ),
		'post_status' => array( 'draft', 'submitted', 'review', 'approved', 'registered' ),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'student_id',
				'value'   => $_POST['registration_meta']['student_id'],
				'compare' => '='
			),
			array(
				'key'     => 'semester',
				'value'   => $_POST['registration_meta']['semester'],
				'compare' => '='
			) ) ) );
	
	if( !empty( $existing_registrations ) ){
		wp_trash_post( $post->ID );
		
		wp_die(
			__( 'Student already registered for the same semester.', 'leading-university' ) .
			' <a href="'.get_edit_post_link( $existing_registrations[0] ) .'">' .
			__( 'Edit original registration.', 'leading-university' ) .
			'</a>'
			);
	}
	
	
	/*
	 * Set the new post status
	 */
	$post->post_status = $_POST['status'];
	 
	/*
	 * Set the post title
	 */
	if( '' == $_POST['registration_meta']['semester'] ){
		$semester = lu_get_student_current_semester( $_POST['registration_meta']['student_id'] );
		$_POST['registration_meta']['semester'] = $semester->term_id;
	}
	else {
		$semester = get_term( $_POST['registration_meta']['semester'], 'semester' );
	}
	

	if( null == $semester ) wp_die( __( 'Invalid semester', 'leading-university' ) );
	$post->post_title = "{$_POST['registration_meta']['student_id']} for $semester->name";
	
	
	$atts = array( 'student_id', 'semester' );
	foreach( $atts as $meta_key ){
		$meta_value = $_POST['registration_meta'][ $meta_key ];
			
		update_post_meta( $post->ID, $meta_key, $meta_value );
	}
	
	$student_user  = lu_get_student_user( $_POST['registration_meta']['student_id'] );
	$program_id    = get_user_meta( $student_user->ID, 'program', true );
	$department_id = get_post_meta( $program_id, '_department', true );
	update_post_meta( $post->ID, 'program', $program_id );
	update_post_meta( $post->ID, '_department', $department_id );
	
	if( !current_user_can( 'edit_post', $post->ID ) ){
		wp_delete_post( $post->ID, true );
		wp_die( __( 'You cannot register students of other departments.', 'leading-university' ) );
	}
	
	$courses = $_POST['course'];
	
	$course_ids = array();
	foreach( $courses as $course ){
		$course_ids[] = intval($course['ID']);
	}
	
	if( !empty( $course_ids ) )
		$course_details = $wpdb->get_results("
			SELECT *
			FROM {$wpdb->prefix}courses
			WHERE
				ID IN (" . implode(',', $course_ids) . ");");
	
	$course_codes = array_combine(
		array_map( function($c){
			return $c->ID;
		}, $course_details ),
		array_map( function($c){
			return $c->code;
		}, $course_details ) );
		
	$timestamp = date('Y-m-d H:i:s');
	
	foreach( $courses as $course ){
		$code          = $course_codes[$course['ID']];
		$reg_id        = $post->ID;
		$course_id     = $course['ID'];
		$section       = $course['section'];
		$type          = lu_get_course_gpa( $_POST['registration_meta']['student_id'], $code ) == -1 ? 'regular' : 'retake';
			
		if( in_array( '', array( $code, $section ) ) ) continue;
		
		$reg_course_id = $wpdb->get_var(  $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}registered_courses WHERE registration_id=%d AND course_id=%d", $reg_id, $course_id ) );
		
		if( null != $reg_course_id )
			$wpdb->replace(
				$wpdb->prefix.'registered_courses',
				array(
					'ID'              => $reg_course_id,
					'registration_id' => $reg_id,
					'course_id'       => $course_id,
					'semester_id'     => $_POST['registration_meta'][ 'semester' ],
					'program_id'      => $program_id,
					'student_id'      => $student_user->ID,
					'status'          => $post->post_status,
					'section'         => $section,
					'type'            => $type,
					'updated_at'      => $timestamp
				)
			);
		else
			$wpdb->insert(
				$wpdb->prefix.'registered_courses',
				array(
					'registration_id' => $reg_id,
					'course_id'       => $course_id,
					'semester_id'     => $_POST['registration_meta'][ 'semester' ],
					'program_id'      => $program_id,
					'student_id'      => $student_user->ID,
					'status'          => $post->post_status,
					'section'         => $section,
					'type'            => $type,
					'updated_at'      => $timestamp
				),
				array(
					'%d',
					'%d',
					'%d',
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s' ) );
	}
	
	$wpdb->query( "DELETE FROM {$wpdb->prefix}registered_courses WHERE registration_id=$reg_id AND updated_at!='$timestamp'" );
	
	 /**
	  * Save the edited post.
	  */
	wp_update_post( $post );
	
}

/**
 * Save front-end registration.
 * 
 * These submissions are made by students for their own selves throught the form
 * provided in the front end.
 */
function lu_save_front_registration(){
	remove_action( 'save_post', 'lu_save_post' );
	global $wpdb, $lu_config;
	if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'save_registration' ) ) return;

	
	$user_id = get_current_user_id();
	$student_id = get_user_meta( $user_id, 'student_id', true );
	$program = get_user_meta( $user_id, 'program', true );
	
	/*
	 * Check if it is possible to get previous results.
	 */
	if( is_wp_error( lu_fetch_result( $student_id ) ) )
		wp_die( __( 'Unable to communicate with exam server.', 'leading-university' ) );
		
	$reg_id = lu_get_student_registration_id( $student_id );
	if( 0 != $reg_id )
		$registration = get_post( $reg_id );
	else
		$registration = null;
	
	if( isset( $_POST['course'] ) ) $courses = $_POST['course'];
	else $courses = array();
	
	if( !isset( $courses['regular'] ) ) $courses['regular'] = array();
	if( !isset( $courses['retake'] ) ) $courses['retake'] = array();
	$courses = array_merge( $courses['regular'], $courses['retake'] );
	
	if( empty( $courses ) )
		wp_die( __( 'You did not select any course.' ) );
		
	$semester = lu_get_student_current_semester( $student_id );
	if( null == $semester || !$semester ) wp_die( __( 'Invalid semester', 'leading-university' ) );
	
	if( null == $registration || $registration->post_status == 'trash' ){
		
		$registration = array( );
		$registration['post_title'] = "$student_id for $semester->name";
		$registration['post_status'] = 'submitted';
		$registration['post_type'] = 'registration';
		
		$post_id = wp_insert_post( $registration );
	}
	else {
		if( !in_array( $registration->post_status, array( 'draft', 'submitted' ) ) ) return;
		$post_id = $registration->ID;
		$registration->post_status = 'submitted';
		wp_update_post( $registration );
	}
	
	
	
	$student_user  = lu_get_student_user( $student_id );
	$program_id    = get_user_meta( $student_user->ID, 'program', true );
	$department_id = get_post_meta( $program_id, '_department', true );
	update_post_meta( $post_id, 'student_id', $student_id );
	update_post_meta( $post_id, 'semester', $semester->term_id );
	update_post_meta( $post_id, 'program', $program_id );
	update_post_meta( $post_id, '_department', $department_id );

	
	$available_courses = lu_get_student_available_courses( $user_id );
	
	$course_ids = array();
	$credit_count = 0;
	foreach( $courses as $course ){
		if( !isset( $available_courses[ $course['ID'] ] ) ) continue; // If this course is not one of their available course, probably a hack.
		if( $available_courses[ $course['ID'] ]->gpa > $lu_config['retake_gpa_limit'] ) continue; // Do not let them register if previous result is more than allowed for retake.
		if( $credit_count + $available_courses[ $course['ID'] ]->credit > get_post_meta( $program, '_credit_limit', true ) ) break; // Don't let them take more courses than allowed.
		
		$credit_count += $available_courses[ $course['ID'] ]->credit;
		$course_ids[] = intval($course['ID']);
	}
	
	if( !empty( $course_ids ) )
		$course_details = $wpdb->get_results("
			SELECT *
			FROM {$wpdb->prefix}courses
			WHERE
				ID IN (" . implode(',', $course_ids) . ");");
	
	$course_codes = array_combine(
		array_map( function($c){
			return $c->ID;
		}, $course_details ),
		array_map( function($c){
			return $c->code;
		}, $course_details ) );
		
	$timestamp = date('Y-m-d H:i:s');
	
	foreach( $courses as $course ){
		if( !isset( $course_codes[$course['ID']] ) ) continue;
		
		$code          = $course_codes[$course['ID']];
		$reg_id        = $post_id;
		$course_id     = $course['ID'];
		$section       = $course['section'];
		$type          = lu_get_course_gpa( $student_id, $code ) == -1 ? 'regular' : 'retake';
			
		if( in_array( '', array( $code, $section ) ) ) continue;
		
		$reg_course_id = $wpdb->get_var(  $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}registered_courses WHERE registration_id=%d AND course_id=%d", $reg_id, $course_id ) );
		



		if( null != $reg_course_id )
			$wpdb->replace(
				$wpdb->prefix.'registered_courses',
				array(
					'ID'              => $reg_course_id,
					'registration_id' => $reg_id,
					'course_id'       => $course_id,
					'semester_id'     => $semester->term_id,
					'program_id'      => $program_id,
					'student_id'      => $student_user->ID,
					'status'          => $registration->post_status,
					'section'         => $section,
					'type'            => $type,
					'updated_at'      => $timestamp
				)
			);
		else
			$wpdb->insert(
				$wpdb->prefix.'registered_courses',
				array(
					'registration_id' => $reg_id,
					'course_id'       => $course_id,
					'semester_id'     => $semester->term_id,
					'program_id'      => $program_id,
					'student_id'      => $student_user->ID,
					'status'          => get_post_status( $reg_id ),
					'section'         => $section,
					'type'            => $type,
					'updated_at'      => $timestamp
				),
				array(
					'%d',
					'%d',
					'%d',
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s' ) );
	}
	
	$wpdb->query( "DELETE FROM {$wpdb->prefix}registered_courses WHERE registration_id=$reg_id AND updated_at!='$timestamp'" );
	
	add_action( 'save_post', 'lu_save_post' );
}

/**
 * Save metadata for program.
 */
function lu_save_program( $post ){
	global $wpdb;
	
	if( !isset( $_POST['lu_nonce'] ) || !wp_verify_nonce( $_POST['lu_nonce'], 'save_program' ) ) return;
	
	
	// Save the settings metabox
	$settings = $_POST['program_settings'];
	if( !isset( $settings['registration_open'] ) ) $settings['registration_open'] = '';
	
	$fees = $settings['fees'];
	$settings['fees'] = array();
	
	foreach( $fees as $fee ){
		$fee['starting_id'] = trim( $fee['starting_id'] );
		$fee['ending_id']   = trim( $fee['ending_id'] );
		$fee['tution_fee']  = '' == trim( $fee['tution_fee'] ) ? 0 : trim( $fee['tution_fee'] );
		$fee['other_fees']  = '' == trim( $fee['other_fees'] ) ? 0 : trim( $fee['other_fees'] );
		$fee['syllabus']    = '' == trim( $fee['syllabus'] ) ? 0 : trim( $fee['syllabus'] );
		
		if( !in_array( '', array( $fee['starting_id'], $fee['ending_id'] ) ) )
			$settings['fees'][] = $fee;
	}
	
	foreach( $settings as $name => $pref ){
		update_post_meta( $post->ID, "_$name", $pref );
	}
	
	
	// Create the current semester term if it does not already exist.
	$semester_name = $settings['semester_season'] . '-' . $settings['semester_year'];
	$semester_term = get_term_by( 'name', $semester_name, 'semester' );
	if( !$semester_term && '' != $settings['semester_season'] && '' != $settings['semester_year'] ){
		wp_insert_term( $semester_name, 'semester' );
	}
	
	
	$offering = array();
	foreach( $_POST['offering'] as $a ){
		$a['sections'] = json_decode( stripcslashes( $a['sections'] ) );
		$a['courses'] = json_decode( stripcslashes( $a['courses'] ) );
		$a['semester'] = trim( $a['semester'] ); 
		
		if( '' != $a['semester'] && !empty( $a['courses'] ) )
			$offering[] = $a;
	}

	$timestamp = date('Y-m-d H:i:s');
	
	$existing_entries = array();
	foreach( $_POST['schedule'] as $s ){
		if( 0 != $s['ID'] ) $existing_entries[] = (int) $s['ID'];
	}
	
	if( !empty( $existing_entries ) )
		$existing_entries = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}program_schedule WHERE ID IN (".implode( ', ', $existing_entries ).") AND program = %d;", $post->ID ) );
	
	$update_entries = $new_entries = array();
	foreach( $_POST['schedule'] as $s ){
		$s['course'] = json_decode( stripcslashes( $s['course'] ) );
		
		if( !in_array(
			$s['day'], 
			array( 
				'saturday', 
				'sunday', 
				'monday', 
				'tuesday', 
				'wednesday', 
				'thursday', 
				'friday', 
				'one_off' ) ) )
			continue;
		
		// Check if any of the detail is missing
		if( in_array( '', array( 
			$s['section'], 
			$s['course'], 
			$s['start_time'], 
			$s['duration'] ) ) ||
			( $s['day'] == 'one_off' && '' == $s['date'] ) )
			continue;
		
		$time = strtotime( $s['start_time'] ) - get_option( 'gmt_offset' )*60*60;
		$start_time = date( 'H', $time ) * 60 * 60 + date( 'i', $time ) * 60;
		$ns = array(
			'semester_section' => $s['section'],
			'event_name'       => isset( $s['course']->isCustom ) && $s['course']->isCustom ? $s['course']->title : $s['course']->code . ' : ' . $s['course']->title,
			'course'           => isset( $s['course']->isCustom ) && $s['course']->isCustom ? 0 : $s['course']->ID,
			'start_time'       => $start_time,
			'duration'         => intval( $s['duration'] ) * 60,
			'faculty'          => intval( $s['teacher'] ),
			'room'             => intval( $s['room'] ),
			'disable'          => ( isset( $s['disable'] ) && 'on' == $s['disable'] ) ? 1 : 0,
			'day'              => $s['day'],
			'date'             => null,
			'program'          => $post->ID,
			'updated_at'       => $timestamp
		);
		
		if( 'one_off' == $s['day'] ) 
			$ns['date'] = date( 'Y-m-d', strtotime( $s['date'] ) );
		
		if( 0 != $s['ID'] && in_array( $s['ID'], $existing_entries ) ){
			$ns['ID'] = $s['ID'];
			$update_entries[] = $ns;
		}
		else {
			$new_entries[] = $ns;
		}
	}
	
	/**
	 * Build the update query
	 */
	if( !empty( $update_entries ) ){
		$update_cases = array(
			'semester_section' => array(),
			'event_name' => array(),
			'course' => array(),
			'start_time' => array(),
			'duration' => array(),
			'faculty' => array(),
			'room' => array(),
			'disable' => array(),
			'day' => array(),
			'program' => array(),
			'updated_at' => array(),
			'date' => array()
			);
		
		foreach( $update_entries as $e ){
			$update_cases['semester_section'][] = $wpdb->prepare( "WHEN %d THEN %s", $e['ID'], $e['semester_section'] );
			$update_cases['event_name'][] = $wpdb->prepare( "WHEN %d THEN %s", $e['ID'], $e['event_name'] );
			$update_cases['course'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['course'] );
			$update_cases['start_time'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['start_time'] );
			$update_cases['duration'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['duration'] );
			$update_cases['faculty'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['faculty'] );
			$update_cases['room'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['room'] );
			$update_cases['disable'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['disable'] );
			$update_cases['day'][] = $wpdb->prepare( "WHEN %d THEN %s", $e['ID'], $e['day'] );
			$update_cases['program'][] = $wpdb->prepare( "WHEN %d THEN %d", $e['ID'], $e['program'] );
			$update_cases['updated_at'][] = $wpdb->prepare( "WHEN %d THEN %s", $e['ID'], $e['updated_at'] );
			
			if( 'one_off' == $e['day'] )
				$update_cases['date'][$e['ID']] = $wpdb->prepare( "WHEN %d THEN %s", $e['ID'], $e['date'] );
		}
		
		// Do the update
		$wpdb->query( "
			UPDATE {$wpdb->prefix}program_schedule
			SET semester_section = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['semester_section']) . "
			END,
			event_name = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['event_name']) . "
			END,
			course = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['course']) . "
			END,
			start_time = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['start_time']) . "
			END,
			duration = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['duration']) . "
			END,
			faculty = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['faculty']) . "
			END,
			room = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['room']) . "
			END,
			disable = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['disable']) . "
			END,
			day = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['day']) . "
			END,
			program = CASE ID
				" . implode( "\n\t\t\t\t", $update_cases['program']) . "
			END,
			updated_at = '$timestamp'" . ( !empty( $update_cases['date'] ) ? ",\n\t\t\tdate = CASE ID\n\t\t\t\t" . implode( "\n\t\t\t\t", $update_cases['date'] ) ."\n\t\t\tEND" : '' ) . "
			WHERE id IN (".implode( ', ', $existing_entries ).")" );
	}
	
	/**
	 * Build the insert query
	 */
	if( !empty( $new_entries ) ){
		$touples = array();
		
		// Create the touples
		foreach( $new_entries as $e ){
			$touples[] = $wpdb->prepare( 
				"(%s, %s, %d, %d, %d, %d, %d, %d, %s, %s, %d, %s)", 
				$e['semester_section'], 
				$e['event_name'],
				$e['course'],
				$e['start_time'],
				$e['duration'],
				$e['faculty'],
				$e['room'],
				$e['disable'],
				$e['day'],
				$e['date'],
				$e['program'],
				$e['updated_at'] );
		}
		
		$wpdb->query( "
			INSERT INTO {$wpdb->prefix}program_schedule
			(`semester_section`, `event_name`, `course`, `start_time`, `duration`, `faculty`, `room`, `disable`, `day`, `date`, `program`, `updated_at`) 
			VALUES 
				" . implode( ', ', $touples ) . ";
			" );
	}
	
	
	/**
	 * Delete those that are not received from this submission as they are intended to be deleted.
	 */
	$wpdb->query( 
		$wpdb->prepare( 
			"
			DELETE FROM {$wpdb->prefix}program_schedule
			WHERE program = %d
			AND updated_at < %s
			",
			$post->ID,
			$timestamp 
		)
	);

	
	update_post_meta( $post->ID, 'offering', $offering );
	lu_save_entity_department( $post->ID, 'edit_others_programs' );
}

/**
 * Save department of an entity after checking necessery permissions
 * 
 * @param int $entity_id Post ID of the entity the meta is being associated to.
 * @param string $req_cap Required capability to make this change (edit_others_posts of the post type).
 */
function lu_save_entity_department( $entity_id, $req_cap ){
	
	if( isset( $_POST['entity_department'] ) && 0 != $_POST['entity_department'] && current_user_can( $req_cap ) )
		$dept_id = trim( $_POST['entity_department'] );
	else
		$dept_id = get_user_meta( get_current_user_id(), 'department', true );
	
	update_post_meta( $entity_id, '_department', $dept_id );
}

/**
 * Save program of an entity after checking necessery permissions
 * 
 * @param int $entity_id Post ID of the entity the meta is being associated to.
 * @param string $req_cap Required capability to make this change (edit_others_posts of the post type).
 */
function lu_save_entity_program( $entity_id, $req_cap ){
	
	if( 
		isset( $_POST['entity_program'] ) && 
		0 != $_POST['entity_program'] && 
		( 
			current_user_can( $req_cap ) || 
			get_post_meta( $entity_id, '_department', true ) == get_user_meta( get_current_user_id(), 'department', true )
		) 
	)
	update_post_meta( $entity_id, '_program', trim( $_POST['entity_program'] ) );
}

/**	
 * Save metadata of syllabus.
 */
function lu_save_syllabus( $post ){
	if( !isset( $_POST['lu_nonce'] ) || !wp_verify_nonce( $_POST['lu_nonce'], 'save_syllabus' ) ) return;
	global $wpdb;
	
	lu_save_entity_program( $post->ID, 'edit_others_syllabuses' );
	lu_save_entity_department( $post->ID, 'edit_others_syllabuses' );
	
	$timestamp = date('Y-m-d H:i:s');
	
	foreach( $_POST['courses'] as $course ){
		$code          = trim( $course['code'] );
		$title         = trim( $course['title'] );
		$credit        = trim( $course['credit'] );
		$prerequisites = trim( stripcslashes( $course['prerequisites'] ) );
		$syllabus      = $post->ID;
		
		if( in_array( '', array( $code, $title, $credit ) ) ) continue;
		
		$course_id = $wpdb->get_var(  $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}courses WHERE code=%s AND syllabus=%d", $code, $syllabus ) );
		
		if( null != $course_id )
			$wpdb->replace(
				$wpdb->prefix.'courses',
				array(
					'ID'            => $course_id,
					'code'          => $code,
					'title'         => $title,
					'credit'        => $credit,
					'prerequisites' => $prerequisites,
					'syllabus'      => $syllabus,
					'updated_at'    => $timestamp 
				)
			);
		else
			$wpdb->insert(
				$wpdb->prefix.'courses',
				array(
					'code'          => $code,
					'title'         => $title,
					'credit'        => $credit,
					'prerequisites' => $prerequisites,
					'syllabus'      => $syllabus,
					'updated_at'    => $timestamp ),
				array(
					'%s',
					'%s',
					'%f',
					'%s',
					'%d',
					'%s' ) );
	}
	
	// Remove the courses that were not sent with the submission (deleted with js)
	$wpdb->query( "DELETE FROM {$wpdb->prefix}courses WHERE syllabus=$syllabus AND updated_at!='$timestamp'" );
}
