<?php
/**
 * Get events feed by query through URL.
 */
function lu_get_event_feed(){
	
	$program = isset( $_REQUEST['program'] ) && '' != $_REQUEST['program'] && 0 != $_REQUEST['program'] ? $_REQUEST['program'] : false;
	$section = isset( $_REQUEST['section'] ) && '' != $_REQUEST['section'] && 0 != $_REQUEST['section'] ? $_REQUEST['section'] : false;
	$room = isset( $_REQUEST['room'] ) && '' != $_REQUEST['room'] && 0 != $_REQUEST['room'] ? $_REQUEST['room'] : false;
	$course = isset( $_REQUEST['course'] ) && '' != $_REQUEST['course'] && 0 != $_REQUEST['course'] ? $_REQUEST['course'] : false;
	$teacher = isset( $_REQUEST['teacher'] ) && '' != $_REQUEST['teacher'] && 0 != $_REQUEST['teacher'] ? $_REQUEST['teacher'] : false;
	
	$events = lu_get_events( $program, $section, $room, $course, $teacher );
	
	echo json_encode( $events );
	exit;
	
}
add_action( 'wp_ajax_get-event-feed', 'lu_get_event_feed' );
add_action( 'wp_ajax_nopriv_get-event-feed', 'lu_get_event_feed' );

/**
 * Output an ical feed for a particular user, requested through ajax.
 */
function lu_get_ical_feed(){
	$events = lu_get_events( 0, 0, 0, 0, 0, $_REQUEST['user'] );
	
	$stream = 
	"BEGIN:VCALENDAR\r\n".
	"VERSION:2.0\r\n".
	"NAME:[LU] Schedule for $_REQUEST[user]\r\n".
	"X-WR-CALNAME:[LU] Schedule for $_REQUEST[user]\r\n".
	"PRODID:-//LeadingUniversity/eAdnan007//NONSGML v1.0//EN\r\n";
	
	foreach( $events as $e ){
		$stream .= 
		"BEGIN:VEVENT\r\n".
		"UID:" . md5($e->id) . "@lus.ac.bd\r\n".
		"DTSTAMP:".date('Ymd\THis\Z', $e->updated_at )."\r\n".
		"DTSTART:".date('Ymd\THis\Z', strtotime( $e->start ) )."\r\n".
		"DTEND:".date('Ymd\THis\Z', strtotime( $e->end ) )."\r\n".
		($e->type == 'recurring' ? "RRULE:FREQ=WEEKLY\r\n": '' ).
		"SUMMARY:$e->title\r\n".
		"LOCATION:$e->location\r\n".
		"END:VEVENT\r\n";
	}
	
	$stream .= "END:VCALENDAR\r\n";

	$filename = $_REQUEST['user'] . '-calendar.ics';

	// Set the headers
	header('Content-type: text/calendar; charset=utf-8');
	header('Content-Disposition: attachment; filename=' . $filename);
	
	echo $stream;
	exit;
}
add_action( 'wp_ajax_get-ical-feed', 'lu_get_ical_feed' );
add_action( 'wp_ajax_nopriv_get-ical-feed', 'lu_get_ical_feed' );

/**
 * Provides the registration information feed.
 */
add_action( 'wp_ajax_get-student-registration-feed', function(){
	$user = lu_get_student_user( $_REQUEST['student_id'] );
	$user_id = $user->ID;
	$program_id = get_user_meta( $user_id, 'program', true );
	
	$semester_id = $_REQUEST['semester'];
	if( '' == $semester_id )
		$semester_id = get_post_meta( $_REQUEST['registration_id'], 'semester', true );


	$current_semester = lu_get_student_current_semester( $_REQUEST['student_id'] );
	if( '' == $semester_id ) {
		$semester                        = $current_semester;
		$semester_id                     = $semester->term_id;
	}
	else {
		$semester = get_term( $semester_id, 'semester' );
	}
	
	$existing = get_posts( array(
		'post_type'       => 'registration',
		'post_status'     => array( 'draft', 'submitted', 'review', 'approved', 'registered' ),
		'posts_per_page'  => 1,
		'meta_query'      => array(
			'relation'    => 'AND',
			array(
				'key'     => 'student_id',
				'value'   => $_REQUEST['student_id'],
				'compare' => '=' ),
			array(
				'key'     => 'semester',
				'value'   => $semester_id,
				'compare' => '=' ) ),
		'post__not_in'    => array( $_REQUEST['registration_id'] ) ) );
	
	if( !empty( $existing ) ){
		$existing = html_entity_decode( 
			sprintf( 
				__( "Registration already exists for semester $semester->name. <a href='%s'>Edit &rarr;</a>", 'leading-university' ), 
				get_edit_post_link( $existing[0]->ID ) ) );
	}
	else {
		$existing = false;
	}
	
	$row_data = lu_fetch_result( $_POST['student_id'] );
	if( is_wp_error( $row_data ) ){
		lu_error( $row_data->get_error_code(), $row_data->get_error_message() );
	}

	$completed_credits = 0;
	foreach( $row_data->result as $result){
	
		$completed_credits += $result->CCredit;
	}
	
	echo json_encode( array(
		'success'           => true,
		'student_name'      => $user->data->display_name,
		'completed_credits' => $completed_credits,
		'current_semester'  => $current_semester->term_id,
		'courses'           => lu_student_available_courses( $_REQUEST['student_id'], $_REQUEST['registration_id'] ),
		'batch_meta'        => lu_get_user_batch_meta( $user_id ),
		'waiver'            => lu_get_student_waiver( $_REQUEST['student_id'] ),
		'credit_limit'      => (int) get_post_meta( $program_id, '_credit_limit', true ),
		'offering'          => lu_get_program_offerings( $program_id ),
		'existing'          => $existing,
		'error'             => is_wp_error( lu_fetch_result( $_REQUEST['student_id'] ) ) ? __( 'Unable to communicate with exam server.', 'leading-university' ) : false ) );
	exit;
});

/**
 * Authenticates a user.
 */
add_action( 'wp_ajax_nopriv_login', function(){
	$user = wp_authenticate( $_POST['username'], $_POST['password'] );
	
	if( is_wp_error( $user ) ){
		$error_str = '';
		foreach( $user->errors as $error ) $error_str .= "$error[0] <br>";
		echo json_encode( array( 'success' => false, 'errors' => $error_str ) );
	}
	else {
		wp_set_auth_cookie( $user->ID, 'true' == $_POST['rememberme'] );
		update_user_meta( $user->ID, 'lu_account_status', 'active' );
		echo json_encode( array( 'success' => true ) );
	}
	
	exit;
});

/**
 * Initiate student registration process
 * 
 * Checks wheather the id and date of birth verifies and sends confirmation so 
 * the front end form can expend to allow more information.
 */
function lu_init_student_registration(){
	header('Content-Type: application/json');
	global $lu_config;
	
	$_POST['student_id'] = trim( $_POST['student_id'] );
	$_POST['birth_date'] = trim( $_POST['birth_date'] );

	if(
		!isset($_POST['student_id']) || 
		!preg_match("/[0-9A-Za-z]{10,20}/", $_POST['student_id']) ||
		!isset($_POST['birth_date']) ||
		$_POST['birth_date'] == '' ||
		$_POST['birth_date'] == 'undefined'
	) {
		lu_error('invalid_request', 'Form was not submitted properly.');
	}
	
	$existing_user = lu_get_student_user( $_POST['student_id'] );
	if( $existing_user && 'active' == get_user_meta( $existing_user->ID, 'lu_account_status', true ) )
		lu_error( 
			'already_registered', 
			__( 'Student already registered. Please use the forgot password link if you forgot your password.', 'leading-university' ) );
	
	$row_data = lu_fetch_result( $_POST['student_id'] );
	if( is_wp_error( $row_data ) ){
		foreach( $row_data->errors as $code => $message ){
			lu_error( $code, $message[0] );
		}
	}

	if( !lu_verify_birthdate( $_POST['birth_date'], $row_data->student ) )
		lu_error( 'birthdate_mismatch', __( 'Input verification failed, contact admission office.', 'leading-university' ) );
	
	$student_program =  get_page_by_title( $row_data->student->Degree, 'OBJECT', 'program' );
	if( null == $student_program )
		lu_error( 'invalid_program', __( 'Invalid program assigned to your profile. Please contact admission before trying again.', 'leading-university' ) );


	$response = array(
		'success' => true,
		'name' => $row_data->student->StudentName );
	
	echo json_encode( $response );
	exit;
}
add_action( 'wp_ajax_nopriv_init-student-registration', 'lu_init_student_registration' );
add_action( 'wp_ajax_init-student-registration', 'lu_init_student_registration' );

/**
 * Register a student.
 */
function lu_register_student(){
	header('Content-Type: application/json');
	global $lu_config;
	
	$_POST['student_id'] = trim( $_POST['student_id'] );
	$_POST['birth_date'] = trim( $_POST['birth_date'] );
	$_POST['email'] = trim( $_POST['email'] );

	if(
		!isset($_POST['student_id']) || 
		!preg_match("/[0-9A-Za-z]{10,20}/", $_POST['student_id']) ||
		!isset($_POST['birth_date']) ||
		$_POST['birth_date'] == '' ||
		$_POST['birth_date'] == 'undefined'
	) {
		lu_error('invalid_request', 'Form was not submitted properly.');
	}
	
	$row_data = lu_fetch_result( $_POST['student_id'] );
	if( is_wp_error( $row_data ) ){
		foreach( $row_data->errors as $code => $message ){
			lu_error( $code, $message[0] );
		}
	}

	if( !lu_verify_birthdate( $_POST['birth_date'], $row_data->student ) )
		lu_error( 'birthdate_mismatch', __( 'Input verification failed, contact admission office.', 'leading-university' ) );

	
	$existing_user = lu_get_student_user( $_POST['student_id'] );
	
	if( $existing_user && 'active' == get_user_meta( $existing_user->ID, 'lu_account_status', true ) )
	lu_error( 
		'already_registered', 
		__( 'Student already registered. Please use the forgot password link if you forgot your password.', 'leading-university' ) );
		
	if( $existing_user && 'active' != get_user_meta( $existing_user->ID, 'lu_account_status', true ) ){
		wp_delete_user( $existing_user->ID );
	}
	
	$student_program =  get_page_by_title( $row_data->student->Degree, 'OBJECT', 'program' );
	if( null == $student_program )
		lu_error( 'invalid_program', __( 'Invalid program assigned to your profile. Please contact admission before trying again.', 'leading-university' ) );

	$name = $row_data->student->StudentName;
	
	$user_id = register_new_user( $_POST['student_id'], $_POST['email'] );
	
	if( is_wp_error( $user_id ) )
		lu_error( 'unable_to_register', __( 'Unable to create account. Already used this email for another ID?', 'leading-university' ) );
		
	add_user_meta( $user_id, 'student_id', $_POST['student_id'] );
	add_user_meta( $user_id, 'program', $student_program->ID );
	add_user_meta( $user_id, 'lu_account_status', 'inactive' );
	
	$user = get_userdata( $user_id );
	$name_parts = lu_name_split( $name );
	$updates = array(
		'ID'           => $user->ID,
		'display_name' => $name,
		'first_name'   => $name_parts[0],
		'last_name'    => $name_parts[1],
		'nickname'     => $name );
	wp_update_user( $updates );
	
	echo json_encode( array(
		'success' => true ) );
	
	exit;
}
add_action( 'wp_ajax_nopriv_register-student', 'lu_register_student' );
add_action( 'wp_ajax_register-student', 'lu_register_student' );

/**
 * Provides the meta of a program required for filtering class routine in front-end
 * through ajax
 */
function lu_get_program_meta(){
	$p = $_REQUEST['program_id'];
	$offerings = lu_get_program_offerings( $p );
	$courses  = lu_get_program_courses( $p );
	$rooms    = lu_get_program_rooms( $p );
	
	$sections = array();
	foreach( $offerings as $of ){
		if( isset( $of['sections'] ) && !empty( $of['sections'] ) ){
			foreach( $of['sections'] as $sec ){
				$sections[] = "$of[semester] ($sec)";
			}
		}
		else $sections[] = $of['semester'];
	}
	array_unshift( $sections, (object) array( 'id' => 0, 'text' => __( 'Select...', 'leading-university' ) ) );
	
	$courses = array_map( 
		function( $c ){
			return (object) array( 'id' => (int) $c->ID, 'text' => "[$c->code] $c->title" );
		}, $courses );
	array_unshift( $courses, (object) array( 'id' => 0, 'text' => __( 'Select...', 'leading-university' ) ) );
	
	$rooms = array_map( 
		function( $c ){
			return (object) array( 'id' => (int) $c->term_id, 'text' => $c->name );
		}, $rooms);
	array_unshift( $rooms, (object) array( 'id' => 0, 'text' => __( 'Select...', 'leading-university' ) ) );
		
	echo json_encode( 
		array(
			'sections' => $sections,
			'courses'  => $courses,
			'rooms'    => $rooms
			)
		);
	
	exit;
}
add_action( 'wp_ajax_get-program-meta', 'lu_get_program_meta' );
add_action( 'wp_ajax_nopriv_get-program-meta', 'lu_get_program_meta' );

add_action( 'wp_ajax_get-program-courses', function(){
	$p = $_REQUEST['program_id'];
	
	echo json_encode( 
		array(
			'courses'  => lu_get_program_courses( $p )
			)
		);
	
	exit;
	
});

/**
 * Change the password for password change form submission.
 */
add_action( 'wp_ajax_change-password', function(){
	if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'lu_change_password' ) ){
		echo json_encode( array( 'success' => 'false', 'errors' => __( 'Invalid request.', 'leading-university' ) ) );
		exit;
	}
	
	$errors = array();
	$user = wp_get_current_user();
	
	extract( $_POST );
	if( '' == $current_password ) $errors[] = __( 'You must provide the current password to change it.', 'leading-university' );
	elseif( !( $user && wp_check_password( $current_password, $user->data->user_pass, $user->ID) ) ) $errors[] = __( 'The current password you provided is wrong. Unable to verify.', 'leading-university' );
	if( '' == $new_password ) $errors[] = __( 'Empty passwords are not allowed.', 'leading-university' );
	if( $confirm_password != $new_password ) $errors[] = __( 'New password did not match. Please try again.', 'leading-university' );
	
	if( !empty( $errors ) ){
		echo json_encode( array( 'success' => false, 'errors' => implode( '<br>', $errors ) ) );
		exit;
	}
	
	wp_set_password( $new_password, $user->ID );
	echo json_encode( array( 'success' => true ) );
	exit;
});

/**
 * Genarates a csv stream with all registered courses and pushes it as a file to
 * download.
 */
add_action( 'wp_ajax_export-registration', function(){
	if( !wp_verify_nonce( $_POST['lu_nonce'], 'export_registration' ) || !current_user_can( 'export_registration' ) ) exit;

	global $wpdb;

	$course_ids = json_decode( stripslashes( $_POST['courses'] ) );
	$course_ids = array_map( function( $id ){
		return (int) $id;
	}, $course_ids );

	$course_ids = implode( ', ', $course_ids );

	$semester = explode( '-', get_term( $_POST['semester_id'], 'semester' )->name );
	$department = get_term( get_post_meta( $_POST['program_id'], '_department', true ), 'department' )->name;	
	$department = html_entity_decode( $department );

	$prepend = array( 
		'year'     => $semester[1],
		'semester' => $semester[0],
		'department' => $department );

	$query = $wpdb->prepare( "SELECT
		um.meta_value as student_id,
		c.code as course_code,
		c.title as course_title,
		c.credit as credit,
		u.display_name as student_name,
		rc.section as section
	FROM {$wpdb->prefix}registered_courses rc
	LEFT JOIN {$wpdb->prefix}courses c
		ON
		c.ID = rc.course_id

	LEFT JOIN ( $wpdb->users u, $wpdb->usermeta um )
		ON
		u.ID = rc.student_id
		AND
		um.meta_key = 'student_id'
		AND
		um.user_id = u.ID

	WHERE
		rc.status = %s
		AND
		rc.semester_id = %d
		AND
		rc.program_id = %d"
		.( $course_ids != '' ? " AND rc.course_id IN ($course_ids)" : '' ),
	$_POST['status'], $_POST['semester_id'], $_POST['program_id'] );

	$rows = $wpdb->get_results( $query, ARRAY_A );


	$out = fopen('php://output', 'w');
	header('Content-type: text/csv');
	header('Content-disposition: filename="export.csv"');
	

	$has_header = false;
	foreach( $rows as $row ){
		$row = $prepend + $row;

		if( !$has_header ){
			$has_header = true;
			fputcsv( $out, array_keys( $row ) );
		}

		fputcsv( $out, $row );
	}

	fclose($out);
	
	exit;
	
});

add_action( 'wp_ajax_evaluation_report_filter_options', function(){
    if( !wp_verify_nonce($_REQUEST['_wpnonce'], 'evaluation_report')){
        echo json_encode(array('success' => false));
        exit;
    }

    global $wpdb;

    $teacher_id = $_REQUEST['teacher_id'];
    $semester_id = $_REQUEST['semester_id'];
    $course_id = $_REQUEST['course_id'];
    $section = $_REQUEST['section'];


    $course_list = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT course_id, section FROM {$wpdb->prefix}evaluation_score WHERE teacher_id=%d AND semester_id=%d GROUP BY course_id, section;",
            $teacher_id, $semester_id));

    $courses = array();

    foreach($course_list as $row){
        if( !isset( $courses[$row->course_id] ) ){
            $courses[$row->course_id] = lu_get_course($row->course_id);
            $courses[$row->course_id]->sections = array();
        }
        $courses[$row->course_id]->sections[] = $row->section;
    }

    $conditions = array();
    if( $teacher_id ) $conditions['teacher_id'] = array( $teacher_id, '%d' );
    if( $semester_id ) $conditions['semester_id'] = array( $semester_id, '%d' );
    if( $course_id ) $conditions['course_id'] = array( $course_id, '%d' );
    if( $section ) $conditions['section'] = array( $section, '%s' );

    $condition_strings = array("1");
    foreach($conditions as $col => $val){
        $condition_strings[] = $wpdb->prepare("$col={$val[1]}", $val[0]);
    }

    $filter = implode(' AND ', $condition_strings );

    $score_result = $wpdb->get_results("
SELECT question_id, AVG(score) as avg_score, COUNT(score) as ct 
FROM {$wpdb->prefix}evaluation_score WHERE $filter
GROUP BY question_id;");

    $scores = array();
    $count = 0;
    foreach($score_result as $score) {
        $scores[$score->question_id] = $score->avg_score;
        $count = $score->ct;
    }



    echo json_encode(array(
        'success' => true,
        'courses' => array_values($courses),
        'scores' => $scores,
        'count' => $count
    ));
    exit;
});
