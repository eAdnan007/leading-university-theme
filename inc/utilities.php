<?php
/**
 * Check if a post belongs to a users department
 */
function lu_post_under_users_dept( $user_id, $post_id ){
 	if( get_user_meta( $user_id, 'department', true ) == get_post_meta( $post_id, '_department', true ) ){
		return true;
	}
	else {
		$departments = wp_get_post_terms( $post_id, 'department' );

		$departments = array_map( function( $dept ){
			return $dept->term_id;
		}, $departments );
		
		return in_array( get_user_meta( $user_id, 'department', true ), $departments ) && sizeof( $departments ) == 1;
	}
}

/**
 * Create a select field.
 * 
 * @param mixed[] $options Associative or non associative array of available options.
 * @param string $value Default/initial value for the select field.
 * @param mixed[] $atts Array of attributes of the select element.
 * @param bool $assoc Wheather the options are associative array or not.
 * @param bool $echo Echo the element if true. Return otherwise.
 */
function lu_create_select_field( $options, $value = '', $atts = array(), $assoc = false, $echo = true ){
	$att_strs = array();
	foreach( $atts as $key => $val ){
		$att_strs[] = "$key=\"$val\"";
	}
	$atts = implode( ' ', $att_strs );
	$el = "<select $atts>";
	$el .= "<option value=''>".__( 'Select...', 'leading-university' )."</option>";
	foreach( $options as $key => $option ){
		if( $assoc ){
			$el .= "<option value=\"$key\" ".( $value == $key ? 'selected="selected"':'' ).">$option</option>";
		}
		else {
			$el .= "<option value=\"$option\" ".( $value == $option ? 'selected="selected"':'' ).">$option</option>";
		}
	}
	$el .= "</select>";
	
	if( $echo ) echo $el;
	else return $el;
}

/**
 * Prints a dropdown to select to department
 */
function lu_department_select_dropdown( $name, $cur_dept = '0', $id = ''){
	$departments = get_terms( 'department', array( 'hide_empty' => false ) );

	$departments = array_combine( 
		array_map( function( $d ){
			return $d->term_id;
		}, $departments ),
		array_map( function( $d ){
			return $d->name;
		}, $departments)
	);
	lu_create_select_field( $departments, $cur_dept, array(
		'name' => $name,
		'id'   => $id ),
		true );
}

/**
 * Prints a dropdown to select semester
 * @param string $selected The default selected value.
 * @param array $atts Array of attributes.
 */
function lu_semester_select_dropdown( $selected = '', $atts = array()){
	$semesters = get_terms( 'semester', array( 'hide_empty' => false, 'orderby' => 'term_id', 'order' => 'DESC' ) );

	$semesters = array_combine( 
		array_map( function( $s ){
			return $s->term_id;
		}, $semesters ),
		array_map( function( $s ){
			return $s->name;
		}, $semesters)
	);
	lu_create_select_field( $semesters, $selected, $atts, true );
}

/**
 * Provides a name for the admin screen based on it's action and post type.
 */
function lu_screen_name(){
	$screen = get_current_screen();
	
	if( '' == $screen->action && isset( $_REQUEST['action'] ) ) $screen->action = $_REQUEST['action'];

	if( $screen->action == '' && $screen->post_type == '' && $_GET['page'] != '')
	    return str_replace( '-', '_', $_GET['page'] );
	
	return "{$screen->action}_{$screen->post_type}";
}

/**
 * Returns array of program's course offerings
 */
function lu_get_program_offerings( $program_id = null ){
	
	if( null == $program_id ){
		global $post;
		$program_id = $post->ID;
	}
	
	$of = get_post_meta( $program_id, 'offering', true );
	if( !is_array( $of ) ) return array();
	else return $of;
}

/**
 * Returns array of all courses from all syllabuses of a program
 */
function lu_get_program_courses( $program_id = null ){
	
	if( null == $program_id ){
		global $post;
		$program_id = $post->ID;
	}
	
	$syllabuses = lu_get_program_syllabuses( $program_id );
	
	$all_courses = array();
	foreach( $syllabuses as $key => $syllabus_name ){
		$courses = lu_get_syllabus_courses( $key );
		$courses = array_map( function( $c ) use ( $syllabus_name ){
			$c->syllabus_name = $syllabus_name;
			return $c;
		}, $courses);
		
		$all_courses[] = $courses;
	}
	
	if( !empty( $all_courses ) )
		return call_user_func_array( 'array_merge', $all_courses );
	else
		return array();
}

/**
 * Returns array of all courses from all syllabuses of a program
 */
function lu_get_program_rooms( $program_id = null ){
	
	if( null == $program_id ){
		global $post;
		$program_id = $post->ID;
	}
	
	return wp_get_post_terms( $program_id, 'room' );
}

/**
 * Returns array of syllabuses associated with a program.
 */
function lu_get_program_syllabuses( $program_id = null ){
	
	if( null == $program_id ){
		global $post;
		$program_id = $post->ID;
	}
	
	$syllabuses = get_posts( 
		array(
			'post_type'      => 'syllabus',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'meta_query'     => array(
				array(
					'key'   => '_program',
					'value' => $program_id
					)
				)
			)
		);
		
	$syllabuses = array_combine(
		array_map( function( $sb ){
			return $sb->ID;
		}, $syllabuses ),
		array_map(function( $sb ){
			return $sb->post_title;
		}, $syllabuses )
	);
	
	return $syllabuses;
}

/**
 * Get the full schedule of a program
 */
function lu_get_program_schedule( $program_id = null ){
	if( null == $program_id ){
		global $post;
		$program_id = $post->ID;
	}
	
	global $wpdb;
	
	$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}program_schedule WHERE program=$program_id");
	
	$result = array_map( function($e) use ($wpdb){
		$e->section = $e->semester_section;
		$e->start_time = date( DATE_ISO8601, $e->start_time );
		$e->duration /= 60;
		$e->teacher = $e->faculty;
		
		if( $e->course != 0 ){
			$e->course = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}courses WHERE ID=$e->course" );
			$e->course->prerequisites = json_decode( $e->course->prerequisites );
			$e->course->syllabus_name = get_post( $e->course->syllabus )->post_title;
		}
		else {
			$e->course = (object) array(
				'title'    => $e->event_name,
				'isCustom' => true );
		}
		
		unset( $e->semester_section );
		unset( $e->event_name );
		unset( $e->updated_at );
		unset( $e->faculty );
		if( '' == $e->date ) unset( $e->date );
		
		return $e;
	}, $result );
	
	return $result;
}

/**
 * Provides a list of courses with details for a particular syllabus
 */
function lu_get_syllabus_courses( $syllabus_id = null ){
	global $wpdb;
	
	if( null == $syllabus_id ){
		global $post;
		$syllabus_id = $post->ID;
	}
	
	$result = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}courses WHERE syllabus=$syllabus_id" );
	
	foreach( $result as $key => $row ){
		$pr = json_decode( $result[$key]->prerequisites );
		$result[$key]->prerequisits = $pr;
		$result[$key]->sections = array();
		$result[$key]->offered = false;
	}
	
	return $result;
}

/**
 * Return all courses from a particular syllabus.
 * 
 * Provides the list of courses from a syllabus that can be registered for in 
 * ongoing semester. It also includes relavent information such as sections
 * offering it in case offered. Should not be available to public.
 */
function lu_get_syllabus_all_courses( $syllabus_id ){
	$program_id = get_post_meta( $syllabus_id, '_program', true );
	
	$sc = lu_get_syllabus_courses( $syllabus_id );
	$sc = array_combine( array_map( function($c){ return $c->ID; }, $sc ), $sc );
	
	$offerings = lu_get_program_offerings( $program_id );
	
	foreach( $offerings as $offering ){
		
		$sections = array();
		
		foreach( $offering['courses'] as $course ){
			if( isset( $sc[$course] ) ){
				if( empty( $sections ) ){
					if( !is_array( $offering['sections'] ) || count( $offering['sections'] ) < 2 ){
						$sections[] = $offering['semester'];
					}
					else {
						foreach( $offering['sections'] as $secAlpha ){
							$sections[] = "$offering[semester] ($secAlpha)";
						}
					}
				}
				
				$sc[$course]->sections = array_merge( $sections, $sc[$course]->sections );
				$sc[$course]->offered = true;
			}
		}
	}
	
	return $sc;
}

/**
 * Return courses from a particular syllabus that has been offered.
 * 
 * Provides the list of courses from a syllabus that can be registered for in 
 * ongoing semester. It also includes relavent information such as sections
 * offering it.
 */
function lu_get_offered_syllabus_courses( $syllabus_id ){
	$program_id = get_post_meta( $syllabus_id, '_program', true );
	
	$sc = lu_get_syllabus_courses( $syllabus_id );
	$sc = array_combine( array_map( function($c){ return $c->ID; }, $sc ), $sc );
	
	$offerings = lu_get_program_offerings( $program_id );
	
	$courses = array();
	foreach( $offerings as $offering ){
		
		$sections = array();
		
		foreach( $offering['courses'] as $course ){
			if( isset( $sc[$course] ) ){
				if( empty( $sections ) ){
					if( !is_array( $offering['sections'] ) || empty( $offering['sections'] ) ){
						$sections[] = $offering['semester'];
					}
					else {
						foreach( $offering['sections'] as $secAlpha ){
							$sections[] = "$offering[semester] ($secAlpha)";
						}
					}
				}
				
				if( !isset( $courses[ $course ] ) )
					$courses[$course] = (object) array(
						'ID' => $sc[$course]->ID,
						'code' => $sc[$course]->code,
						'title' => $sc[$course]->title,
						'credit' => $sc[$course]->credit,
						'sections' => $sections,
						'prerequisits' => $sc[$course]->prerequisits,
						'offered' => true
					);
				else
					$courses[ $course ]->sections = array_merge( $sections, $courses[$course]->sections );
			}
		}
	}
	
	return $courses;
}

/**
 * Get the current semester term of a student.
 */
function lu_get_student_current_semester( $student_id ){
	$user = lu_get_student_user( $student_id );
	if( !$user ) return false;
	
	$program = get_user_meta( $user->ID, 'program', true );
	
	$semester_name = get_post_meta( $program, '_semester_season', true ) . '-' . get_post_meta( $program, '_semester_year', true );
	$semester = get_term_by( 'name', $semester_name, 'semester' );
	
	return $semester;
}

/**
 * Get the courses the student can register in this semester.
 */
function lu_get_student_available_courses( $user_id, $semester = 0 ){
	global $lu_config;
	
	$student_meta = lu_get_user_batch_meta( $user_id );
	$syllabus_id = $student_meta['syllabus'];
	$student_id = get_user_meta( $user_id, 'student_id', true );
	if( $semester == 0 ) {
		$semester = lu_get_student_current_semester( $student_id )->term_id;
	}

	if( current_user_can( 'edit_registrations' ) )
		$courses = lu_get_syllabus_all_courses( $syllabus_id );
	else
		$courses = lu_get_offered_syllabus_courses( $syllabus_id );

	$registered_courses = lu_get_student_previously_registered_courses($student_id);

	array_walk( $courses, function( &$v, $k ) use ( $student_id, $lu_config, $courses, $semester, $registered_courses ){
		$v->gpa = isset($registered_courses[$v->ID]) ? lu_get_course_gpa( $student_id, $v->code ) : -1;
		$v->type = -1 == $v->gpa ? 'regular' : 'retake';
		$v->retakable = ( floatval( $v->gpa ) <= floatval( $lu_config['retake_gpa_limit'] ) );
		
		if( is_array( $v->prerequisits ) ){
			array_walk( $v->prerequisits, function( &$course, $key ) use ( $student_id ){
				$course = (object) array( 'code' => $course, 'gpa' => lu_get_course_gpa( $student_id, $course ) );
			});
		}
		
		array_walk( $v->sections, function( &$section, $key ) use ( $v, $semester ){
			global $wpdb;
			$count = $wpdb->get_var( 
				$wpdb->prepare( 
					"SELECT COUNT({$wpdb->prefix}registered_courses.section) 
					FROM 
						{$wpdb->prefix}registered_courses,
						{$wpdb->posts},
						{$wpdb->postmeta}
					WHERE 
						{$wpdb->prefix}registered_courses.course_id=%d 
						AND 
						{$wpdb->prefix}registered_courses.section=%s
						AND
						{$wpdb->prefix}registered_courses.registration_id={$wpdb->postmeta}.post_id
						AND
						{$wpdb->postmeta}.meta_key='semester'
						AND
						{$wpdb->postmeta}.meta_value=%d
						AND
						{$wpdb->prefix}registered_courses.registration_id={$wpdb->posts}.ID
						AND
						{$wpdb->posts}.post_status NOT IN ('draft', 'trash', 'submitted')",
					$v->ID, 
					$section,
					$semester ) );

			$section = (object) array( 'name' => $section, 'count' => $count );
		});
	});
	
	return $courses;
}


/**
 * Tells wheather a user is student or not.
 */
function lu_is_student( $id = false ){
	if( !$id ){
		$id = get_current_user_id();
	}
	elseif( is_object( $id ) ) {
		$id = $id->ID;
	}
	
	return trim( get_user_meta( $id, 'student_id', true ) ) != '';
}

/**
 * Get meta of the user's program.
 *
 * @param $meta_key Meta key to receive
 * @param null $user_id Id of the user to check against.
 */
function lu_get_user_program_meta($meta_key, $user_id = null ) {
    if( is_null($user_id) ) $user_id = get_current_user_id();
    $program_id = get_user_meta($user_id, 'program', true);

    return get_post_meta($program_id, $meta_key, true);
}

/**
 * Get current registration ID of a student.
 * 
 * Provides the registration post's ID of the student for current semester.
 * 
 * @param string $student_id Student ID.
 * @param string $semester Semester name.
 */
function lu_get_student_registration_id( $student_id = false, $semester = false ){
	
	if( false == $student_id ) {
		$student_id = get_user_meta( get_current_user_id(), 'student_id', true );
		if( '' == $student_id ) return 0;
	}
	
	if( !$semester ){
		$semester = lu_get_student_current_semester( $student_id );
	}
	else {
		$semester = get_term_by( 'name', $semester, 'semester' );
	}
	
	if( $semester )
		$semester = $semester->term_id;
	else return 0;
	
	
	$query_args = array(
		'post_type'   => 'registration',
		'post_status' => array( 'draft', 'submitted', 'review', 'approved', 'registered' ),
		'meta_query'  => array(
			array(
				'key'     => 'student_id',
				'value'   => $student_id,
				'compare' => '='
			),
			array(
				'key'     => 'semester',
				'value'   => $semester,
				'compare' => '='
			) ) );
			
	$posts = get_posts( $query_args );
	
	if( $posts )
		return $posts[0]->ID;
	
	return 0;
}

/**
 * Get fees, syllabus and other batch specific details for a particular student.
 */
function lu_get_user_batch_meta( $user_id ){
	$student_id = get_user_meta( $user_id, 'student_id', true );
	$program_id = get_user_meta( $user_id, 'program', true );

	
	$ranges = get_post_meta( $program_id, '_fees', true );

	
	$selected_range = false;
	foreach( $ranges as $range ){
		if(
			( $range['starting_id'] <= $student_id && $range['ending_id'] >= $student_id ) ||
			( $range['ending_id'] <= $student_id && $range['starting_id'] >= $student_id )
		){
			$selected_range = $range;
		}
	}
	
	return $selected_range;
}

/**
 * Get name of menu by location.
 */
function lu_get_menu_by_location( $location ) {
	if( empty($location) ) return false;

	$locations = get_nav_menu_locations();
	if( ! isset( $locations[$location] ) ) return false;
	
	$menu_obj = get_term( $locations[$location], 'nav_menu' );
	
	return $menu_obj;
}

/**
 * Finds the user id of a student.
 */
function lu_get_student_user( $student_id ){
	$users = get_users( array( 'meta_key' => 'student_id', 'meta_value' => $student_id ) );
	
	if( empty( $users ) ) return false;
	
	return $users[0];
}

/**
 * Provide list of courses and registered courses for a student id
 */
function lu_student_available_courses( $student_id = false, $registration_id = false ){

	if( !$student_id && isset( $_REQUEST['student_id'] ) ){
		$student_id = $_REQUEST['student_id'];
	}
	elseif( !$student_id && !isset( $_REQUEST['student_id'] ) ){
		return array();
	}
	
	if( !$registration_id && isset($_REQUEST['registration_id'] ) ){
		$registration_id = $_REQUEST['registration_id'];
	}
	
	$user = lu_get_student_user( $student_id );
	$user_id = $user->ID;
	
	
	$courses = lu_get_student_available_courses( $user_id, get_post_meta( $registration_id, 'semester', true ) );
	
	if( $registration_id ){
		$registered_courses = lu_get_registered_courses( $registration_id );
				
				
		foreach( $registered_courses as $course ){
			$courses[$course->course_id]->registered = true;
			$courses[$course->course_id]->reged_section = $course->section;
			$courses[$course->course_id]->type = $course->type;
		}
	}

	return $courses;
}

/**
 * Get student registered courses.
 */
function lu_get_registered_courses( $registration_id ){
	global $wpdb;
	
	$registered_courses_table = "{$wpdb->prefix}registered_courses";
	$posts_table = "{$wpdb->prefix}posts";

	return $wpdb->get_results( 
			$wpdb->prepare( 
				"SELECT $registered_courses_table.*
					FROM 
						$registered_courses_table,
						$posts_table
					WHERE
						$posts_table.ID=$registered_courses_table.registration_id
						AND
						$posts_table.post_type='registration'
						AND
						$registered_courses_table.registration_id=%d",
				$registration_id ) );
}

/**
 * Get course from a registered course row.
 *
 * @param int $course_id Id of the course.
 * @return object Course object.
 */
function lu_get_course($course_id ) {
    global $wpdb;
    return $wpdb->get_row(
        $wpdb->prepare("SELECT * FROM {$wpdb->prefix}courses WHERE ID=%d;", $course_id)
    );
}

/**
 * Verify a student's birthdate by id or student object returned from database.
 */
function lu_verify_birthdate( $birthdate_string, $student_data ){
	$bdate_input = strtotime($birthdate_string);
	if( is_string( $student_data ) ){
		$student_data = lu_fetch_result( $student_data );
		if( is_wp_error( $student_data ) ){
			return false;
		}
		
		$student_data = $student_data->student;
	}
	if( is_object( $student_data ) ){
		$bdate_dbase = strtotime($student_data->Date_of_Birth);
	}
	
	$bdate_deflt = strtotime("1-1-1900"); // Many people does not have their birthdate stored
	
	if( $bdate_input == $bdate_dbase || $bdate_dbase == $bdate_deflt ) return true;
	return false;
}

/**
 * Provides the waiver percentile for a student and keeps it in the database in case of database outage.
 */
function lu_get_student_waiver( $student_id = null ){
	if( null == $student_id ){
		$student_id = get_user_meta( get_current_user_id(), 'student_id', true );
		$user = get_current_user_id();
	}
	else {
		$user = lu_get_student_user( $student_id )->ID;
	}
	
	$results = lu_fetch_result( $student_id );
	if( !is_wp_error( $results ) ){
		$waiver = (int) $results->student->Waiver;
		update_user_meta( $user, 'waiver', $waiver );
	}
	elseif( get_user_meta( $user, 'waiver', true ) != '' ){
		$waiver = (int) get_user_meta( $user, 'waiver', true );
	}
	else {
		$waiver = 0;
	}
	
	return $waiver;
}

/**
 * Devides a name in two parts to use as first name and last name.
 */
function lu_name_split( $name ){
	$offset = 0;
	$p = array();
	
	while($offset = strpos($name, " ", $offset+1)){
		$p[] = $offset;
	}
	
	$m = floor( sizeof( $p ) /2 );
	$fname = substr( $name, 0, $p[$m] );
	$lname = substr( $name, $p[$m]+1, strlen($name) );
	
	return array( $fname, $lname );
}

/**
 * Get list of events for class schedule.
 */
function lu_get_events( $program = false, $section = false, $room = false, $course = false, $teacher = false, $username = false ){
	
	global $wpdb;
	
	if( !$username ){
		$user = wp_get_current_user();
	}
	else {
		$user = get_user_by( 'login', $username );
	}
	
	$conditions = array();
	if( $program ) $conditions[] = $wpdb->prepare( "program = %d", $program );
	if( $section ) $conditions[] = $wpdb->prepare( "semester_section = %s", $section );
	if( $room )    $conditions[] = $wpdb->prepare( "room = %d", $room );
	if( $course )  $conditions[] = $wpdb->prepare( "course = %d", $course );
	if( $teacher ) $conditions[] = $wpdb->prepare( "faculty = %d", $teacher );
	
	if( !$program && !$teacher && $user ){
		if( lu_is_student( $user->ID ) ){
			$registered_courses = lu_get_registered_courses( 
				lu_get_student_registration_id( 
					get_user_meta( $user->ID, 'student_id', true ) ) );
			
			if( empty( $registered_courses ) ){
				$conditions[] = $wpdb->prepare( "program = %d", get_user_meta( $user->ID, 'program', true ) );
			}
			else {	
				$conditions = array_map( function( $rc ) use ( $wpdb ){
					return $wpdb->prepare( "( course = %d AND semester_section = %s )", $rc->course_id, $rc->section );
				}, $registered_courses );
				
				$qstr = implode( ' OR ', $conditions );
				
				$conditions = array( "( $qstr )" );
			}
		}
		else {
			$conditions[] = $wpdb->prepare( "faculty = %d", $user->ID );
		}
	}
	
	$conditions[] = "disable != true";
	
	$start_timestamp = isset( $_REQUEST['start'] ) ? strtotime( $_REQUEST['start'] ) : time();
	$end_timestamp   = isset( $_REQUEST['end'] ) ? strtotime( $_REQUEST['end'] ) : time() + 7 * 24 * 60 * 60;
	
	$weekdays = array();
	$ts = $start_timestamp;
	while( $ts <= $end_timestamp ){
		$weekdays[ strtolower(date('l', $ts )) ][] = $ts;
		$ts = mktime( 0, 0, 0, date('n', $ts), date('j', $ts)+1, date('Y', $ts) );
	}
	
	$recurring_cond   = $conditions;
	$recurring_cond[] = "day!='one_off'";
	
	$one_off_cond     = $conditions;
	$one_off_cond[]   = "day='one_off'";
	$one_off_cond[]   = $wpdb->prepare( "date>=%s", date( 'Y-m-d', $start_timestamp ) );
	$one_off_cond[]   = $wpdb->prepare( "date<=%s", date( 'Y-m-d', $end_timestamp ) );
	
	unset( $conditions );
	$one_off_cond     = implode( ' AND ', $one_off_cond );
	$recurring_cond   = implode( ' AND ', $recurring_cond );
	
	$recurring_q      = "SELECT * FROM {$wpdb->prefix}program_schedule WHERE $recurring_cond";
	$one_off_q        = "SELECT * FROM {$wpdb->prefix}program_schedule WHERE $one_off_cond";
	
	$recurring_events = $wpdb->get_results( $recurring_q );
	$one_off_events   = $wpdb->get_results( $one_off_q );
	
	$events = array();
	foreach( $recurring_events as $re ){
		$faculty_name = 0 != $re->faculty ? " by " . get_userdata($re->faculty)->display_name : '';
		$room_name    = 0 != $re->room ? " in " . get_term( $re->room, 'room' )->name : '';
		
		// If current weekday does not exists, initialize it as an array
		if( !isset( $weekdays[$re->day] ) ) $weekdays[$re->day] = array();
		
		foreach( $weekdays[$re->day] as $date ){
			$events[] = (object) array(
				'id'         => $re->ID,
				'title'      => $re->event_name . $faculty_name . $room_name,
				'start'      => date( DATE_ISO8601, $date + $re->start_time ),
				'end'        => date( DATE_ISO8601, $date + $re->start_time + $re->duration ),
				'type'       => 'recurring',
				'location'   => 0 != $re->room ? get_term( $re->room, 'room' )->name : '',
				'updated_at' => strtotime( $re->updated_at )
			);
		}
	}
	
	foreach( $one_off_events as $ooe ){
		$faculty_name = 0 != $ooe->faculty ? " by " . get_userdata($ooe->faculty)->display_name : '';
		$room_name    = 0 != $ooe->room ? " in " . get_term( $ooe->room, 'room' )->name : '';
		$date         = strtotime( $ooe->date );

		$events[] = (object) array(
			'id'            => $ooe->ID,
			'title'         => $ooe->event_name . $faculty_name . $room_name,
			'start'         => date( DATE_ISO8601, $date + $ooe->start_time ),
			'end'           => date( DATE_ISO8601, $date + $ooe->start_time + $ooe->duration ),
			'type'          => 'one_off',
			'location'      => 0 != $ooe->room ? get_term( $ooe->room, 'room' )->name : '',
			'updated_at'    => strtotime( $ooe->updated_at )
			);
	}
	
	return $events;
}

/**
 * Get a course by it's course code.
 *
 * @param $course_code
 * @param null $syllabus_id
 * @return object Database row defining the course.
 */
function lu_find_course_by_code( $course_code, $syllabus_id = null ){
    global $wpdb;

    if( null == $syllabus_id ){
        $batch_meta = lu_get_user_batch_meta(get_current_user_id());
        $syllabus_id = $batch_meta['syllabus'];
    }

    return $wpdb->get_row($wpdb->prepare("SELECT * FROM luw_courses
WHERE syllabus=%d AND code=%s;", $syllabus_id, $course_code));
}

/**
 * Get a list of evaluation questions grouped by aspects.
 *
 * @return object[] List of all aspects with the questions associated.
 */
function lute_aspects() {
	global $wpdb;

	$aspects = $wpdb->get_results("
        SELECT *
        FROM `{$wpdb->prefix}evaluation_questions`
        WHERE `type`='aspect'
        ORDER BY `index` ASC;
    ", OBJECT);

	$aspects = array_map(function($aspect) use ($wpdb) {
		$aspect->questions = $wpdb->get_results("
            SELECT *
            FROM `{$wpdb->prefix}evaluation_questions`
            WHERE 
              `type`='question'
              AND
              `parent`=$aspect->ID
              ORDER BY `index` ASC;
        ", OBJECT);

		return $aspect;
	}, $aspects);

	return $aspects;
}

/**
 * Get id of the next post to be evaluated by current user(student).
 *
 * @return int
 */
function lute_next_pending_evaluation_course() {
    $registered_courses = lu_get_registered_courses(
        lu_get_student_registration_id(
            false,
            get_term(lu_get_user_program_meta('_evaluation_semester'), 'semester')->name
        )
    );

    foreach($registered_courses as $course){
        if(!lute_course_evaluated($course->course_id)) return $course->course_id;
    }

    return 0;
}

/**
 * Check if course is already evaluated.
 *
 * @param $course_id The id of the course that is being checked.
 * @param $semester_id
 * @param null $user_id Id of the user to check against.
 * @return bool
 */
function lute_course_evaluated($course_id, $semester_id = null, $user_id = null) {
    global $wpdb;

    if(is_null($user_id)) $user_id = get_current_user_id();
    if(is_null($semester_id)) $semester_id = lu_get_user_program_meta('_evaluation_semester');

    $existing = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*)
          FROM `{$wpdb->prefix}evaluation_score`
          WHERE `question_id` = 0
            AND `user_id`=%d
            AND `course_id`=%d
            AND `semester_id`=%d", $user_id, $course_id, $semester_id
    ));

    return $existing > 0;
}

/**
 * Return evaluation score of a particular submission
 *
 * @param $course_id The id of the course that is being checked.
 * @param null $user_id Id of the user to check against.
 * @return array Rows relevent to the submission.
 */
function lute_get_evaluation_score($course_id, $user_id = null) {
    global $wpdb;

    if(is_null($user_id)) $user_id = get_current_user_id();

    $result = $wpdb->get_results( $wpdb->prepare("SELECT *
          FROM `{$wpdb->prefix}evaluation_score`
          WHERE `user_id`=%d
            AND `course_id`=%d", $user_id, $course_id
    ));

    $scores = array();

    foreach($result as $row){
        $scores[$row->question_id] = $row->score;
    }

    return $scores;
}

/**
 * Get link to the evaluation form for a particular course.
 *
 * @param $course_id
 * @return string url to evaluation form.
 */
function lute_get_evaluation_link($course_id) {
    $page = get_page_by_path('evaluation');
    $page_link = get_page_link($page->ID);

    $evaluation_url = add_query_arg('course', $course_id, $page_link);

    return $evaluation_url;
}
