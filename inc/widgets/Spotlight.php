<?php // Creating the widget 
class Spotlight_Widget extends WP_Widget {
	function __construct() {
		parent:: __construct(
			'spotlight_widget',
			__('Spotlight', 'leading-university'),
			array( 
				'classname'   => 'spotlight',
				'description' => __( 'Display the most important message. Use &lt;span&gt; tag for bigger font.', 'leading-university')
			)
		);
	}
	
	// Creating widget front-end
	public function widget( $args, $instance) {

		echo $args['before_widget'];

		echo '<p>' . $instance['content'] . '</p>';
		echo '<a href="' . $instance['link'] . '" class="pull-right">' . $instance['link_text'] . '</a>';

		echo $args['after_widget'];
	}
	
	// Widget Backend 
	public function form( $instance) {
		$title = __( 'Spotlight', 'leading-university');
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'content' ); ?>">
				<?php _e( 'Content', 'leading-university' ); ?>
			</label>
			<textarea name="<?php echo $this->get_field_name( 'content' ); ?>" id="<?php echo $this->get_field_id( 'content' ); ?>" class="widefat"
				><?php echo isset( $instance['content'] ) ? esc_attr( $instance['content'] ) : ''; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_text' ); ?>">
				<?php _e( 'Link Text', 'leading-university' ); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'link_text' ); ?>" name="<?php echo $this->get_field_name( 'link_text' ); ?>" type="text" value="<?php echo isset( $instance['link_text'] ) ? esc_attr( $instance['link_text'] ) : ''; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>">
				<?php _e( 'Link URL', 'leading-university' ); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo isset( $instance['link'] ) ? esc_attr( $instance['link'] ) : ''; ?>" />
		</p>
		<?php
	}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance) {
		$instance=array();
		$instance['content']=( ! empty( $new_instance['content'])) ? $new_instance['content'] : '';
		$instance['link_text']=( ! empty( $new_instance['link_text'])) ? strip_tags( $new_instance['link_text']): '';
		$instance['link']=( ! empty( $new_instance['link'])) ? strip_tags( $new_instance['link']): '';
		return $instance;
	}
}