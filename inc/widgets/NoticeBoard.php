<?php // Creating the widget 
class NoticeBoard_Widget extends WP_Widget {
	function __construct() {
		parent:: __construct(
			'notice_board_widget',
			__('Notice Board', 'leading-university'),
			array( 
				'classname'   => 'notice-board',
				'description' => __( 'Show list of notices.', 'leading-university')
			)
		);
	}
	
	// Creating widget front-end
	public function widget( $args, $instance) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $args['before_widget'];
		
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		// The Query
		$the_query = new WP_Query( array(
			'post_type'      => 'notice',
			'post_status'    => 'publish',
			'posts_per_page' => $instance['number_of_posts'],
			'meta_query'     => array(
				'relation'     => 'OR',
				array(
					'key'      => '_hide_in_widget',
					'value'    => 'on',
					'compare'  => '!=' ),
				array(
					'key'      => '_hide_in_widget',
					'compare'  => 'NOT EXISTS' ) ),
			'order'          => 'DESC',
			'orderby'        => 'date' ) );
		
		// The Loop
		if ( $the_query->have_posts() ) {
			echo '<ul class="list-group">';
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				echo '<li class="list-group-item">';
				echo '<span class="badge' . ('on' == get_post_meta( get_the_ID(), '_important', true ) ? ' badge-color' : '') . ' pull-left">' . get_the_date( 'j' ) . '<br>' . get_the_date( 'M' ) . '</span>'
						.'<p><a href="'.get_permalink().'">'.get_the_title().'</a></p>';
				echo '</li>';
			}
			echo '</ul>';
			echo '<p class="text-right"><a href="'.get_post_type_archive_link( 'notice' ).'">'.__( 'Archive', 'leading-university' ).'</a></p>';
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();

		echo $args['after_widget'];
	}
	
	// Widget Backend 
	public function form( $instance) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number_of_posts' ); ?>"><?php _e( 'Number of Posts:', 'leading-university' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'number_of_posts' ); ?>" type="text" value="<?php echo isset( $instance['number_of_posts'] ) ? esc_attr( $instance['number_of_posts'] ) : ''; ?>" />
		</p>
		<?php 
}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance) {
		$instance=array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['number_of_posts'] = ( ! empty( $new_instance['number_of_posts'] ) ) ? strip_tags( $new_instance['number_of_posts'] ) : '';
		return $instance;
	}
}