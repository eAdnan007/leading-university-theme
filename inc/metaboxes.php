<?php

/**
 * Hide metaboxes for non-privileged users.
 */
add_action( 'admin_menu' , function() {
	global $dept_based_cap_post_types;
	foreach( $dept_based_cap_post_types as $post_type ){
		$post_type = get_post_type_object( $post_type );
		if( !current_user_can( $post_type->cap->edit_others_posts ) ){
			remove_meta_box( 'tagsdiv-department', $post_type->name, 'side' );
			
			if( 'notice' == $post_type->name )
				remove_meta_box( 'notice-attributes', $post_type->name, 'side' );
		}
	}
	
	remove_meta_box('submitdiv', 'registration', 'side');
	remove_meta_box('slugdiv', 'registration', 'normal');
});

/**
 * Remove the department metabox from program to allow only one department per program
 */
add_action( 'admin_menu', function(){
	remove_meta_box( 'tagsdiv-department', 'program', 'normal' );
	remove_meta_box( 'tagsdiv-semester', 'program', 'normal' );
});

/**
 * Create metabox for notice
 */
add_action( 'add_meta_boxes_notice', function(){
	add_meta_box(
		'notice-attributes',
		__( 'Notice Attributes', 'leading-university' ),
		function( $post ){
			?>
			<p>
				<label>
					<input
						type="checkbox"
						name="notice_meta[hide_in_widget]"
						<?php echo get_post_meta( $post->ID, '_hide_in_widget', true ) == 'on' ? 'checked="checked"' : ''; ?>>
					<?php _e( 'Hide from widget', 'leading-university' ); ?>
				</label>
			</p>
			<p>
				<label>
					<input
						type="checkbox"
						name="notice_meta[important]"
						<?php echo get_post_meta( $post->ID, '_important', true ) == 'on' ? 'checked="checked"' : ''; ?>>
					<?php _e( 'Important Notice', 'leading-university' ); ?>
				</label>
			</p>
			<?php
			wp_nonce_field( 'save_notice', 'lu_nonce' );
		},
		'notice',
		'side',
		'core' );
});

/**
 * Create metabox for notice
 */
add_action( 'add_meta_boxes_registration', function(){
	add_meta_box(
		'courses',
		__( 'Courses', 'leading-university' ),
		function( $post ){
			?>
			<table>
				<thead>
					<tr>
						<th class="course-title"><?php _e( 'Course', 'leading-university' ); ?></th>
						<th class="credit"><?php _e( 'Credit', 'leading-university' ); ?></th>
						<th class="section"><?php _e( 'Section', 'leading-university' ); ?></th>
						<th class="type"><?php _e( 'Type', 'leading-university' ); ?></th>
						<th class="remove" ng-show="can_change"><?php _e( 'Remove', 'leading-university' ); ?></th>
					</tr>
				</thead>
				<tbody ng-repeat="course in courses | toArray | filter: { registered: true }">
					<tr>
						<td class="course-title">
							{{course.code}} : {{course.title}} {{(course.type=='retake'?' (GP ' + course.gpa + ')':'')}}
							<div ng-repeat="prc in course.prerequisits" class="notice notice-{{prerequisiteSeverity(prc)}}" ng-show="!freez_registration">
								<b>{{prc.code}}</b>:
								{{prc.gpa==-1||prc.gpa==0?(prc.gpa==-1?"<?php _e( 'Never enrolled', 'leading-university' ); ?>":"<?php _e( 'Failed', 'leading-university' ); ?>"):prc.gpa}}
							</div>
						</td>
						<td class="credit">{{course.credit}}</td>
						<td class="section">
							<span ng-hide="can_change">{{course.reged_section}}</span>
                            <select
                                    ng-show="can_change && course.offered && current_semester == semester"
                                    ng-options="sec.name as (sec.name) for sec in course['sections']"
                                    ng-model="course.reged_section">
                            </select>
                            <input
                                    type="text"
                                    ng-show="can_change && !(course.offered && current_semester == semester)"
                                    ng-model="course.reged_section"/>
						</td>
						<td class="type">{{course.type}}</td>
						<td class="remove" ng-show="can_change">
							<button type="button" ng-disabled="!can_change" ng-click="removeCourse(course.ID)" class="button button-secondary"><?php _e( 'Remove', 'leading-university' ); ?></button>
							<input type="hidden" name="course[{{$index}}][ID]" value="{{course.ID}}">
							<input type="hidden" name="course[{{$index}}][section]" value="{{course.reged_section}}">
						</td>
					</tr>
				</tbody>
				<tfoot ng-show="can_change">
					<tr>
						<td class="course-title">
							<select name="" id="" ng-disabled="!can_change" ng-options="((course.offered && current_semester == semester ? '* ' : '') + course.code + ' : ' + course.title + (course.gpa>=0?' (GP ' + course.gpa + ')':' (New)')) disable when (course.registered || !course.retakable) for course in courses track by course.ID" ng-model="newCourse"></select>
						</td>
						<td class="credit">{{newCourse.credit}}</td>
						<td class="section">
							<select
                                ng-show="newCourse && can_change && newCourse.offered && current_semester == semester"
								ng-options="sec.name as (sec.name) for sec in newCourse['sections']"
								ng-model="newSection">
							</select>
                            <input
                                type="text"
                                ng-show="can_change && !(newCourse.offered && current_semester == semester)"
                                ng-model="newSection"/>
						</td>
						<td class="type">{{ newCourse.gpa == -1 ? 'regular' : 'retake' }}</td>
						<td class="add"><button type="button" ng-click="addCourse(newCourse, newSection)" class="button button-secondary"><?php _e( 'Add', 'leading-university' ); ?></button></td>
					</tr>
				</tfoot>
			</table>
			<?php
		},
		'registration',
		'normal',
		'high' );
		
	add_meta_box(
		'registration-meta',
		__( 'Registration Meta', 'leading-university' ),
		function( $post ){
			?>
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><label for="semester"><?php _e( 'Semester', 'leading-university' ); ?></label></th>
						<td>
							<?php
							lu_semester_select_dropdown( get_post_meta( $post->ID, 'semester', true ), array(
								'ng-init'      => "semester='".get_post_meta( $post->ID, 'semester', true )."'",
								'name'         => 'registration_meta[semester]',
								'ng-disabled'  => "!can_change",
								'ng-model'     => 'semester' ) );
							?>
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="student-id"><?php _e( 'Student ID', 'leading-university' ); ?></label></th>
						<td>
							<span ng-if="!can_change">{{student_id}}</span>
							<input type="text" ng-show="can_change" name="registration_meta[student_id]" id="student-id" ng-model="student_id" ng-init="student_id='<?php echo get_post_meta( $post->ID, 'student_id', true ); ?>';updateStudent();" class="regular-text">
							<div class="dashicons dashicons-update" ng-click="updateStudent()" ng-class="{point:!updating_student, 'spin-it':updating_student}"></div>
							<div ng-if="existing" class="notice notice-warning" ng-bind-html="existing | html"></div>
						</td>
					</tr>
					<tr>
						<th scope="row"><label><?php _e( 'Name', 'leading-university' ); ?></label></th>
						<td>
							{{student_name}}
						</td>
					</tr>
					<tr>
						<th scope="row"><label><?php _e( 'Credits Completed', 'leading-university' ); ?></label></th>
						<td>
							{{completed_credits}}
						</td>
					</tr>
					<tr ng-show="current_semester == semester">
						<th scope="row"><label for="section"><?php _e( 'Section', 'leading-university' ); ?></label></th>
						<td>
							<select ng-model="main_section" ng-change="registerSectionCourses(main_section)">
								<option ng-repeat="section in sections()">{{section}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Total Credits Taken', 'leading-university' ); ?></th>
						<td>
							<span class="total_credit">{{creditCount('regular')}}(regular) + {{creditCount('retake')}}(retake) = <b>{{creditCount()}}</b></span>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Total Fees', 'leading-university' ); ?></th>
						<td>
							<span class="total_fees">( {{creditCount('regular')}} x {{tution_fee}} - {{waiver}}% ) + ( {{creditCount('retake')}} x {{tution_fee}} ) + {{other_fees}} = <b>{{totalFees()}}</b></span>
						</td>
					</tr>
				</tbody>
			</table>
			<?php
		},
		'registration',
		'normal',
		'high' );
		
	add_meta_box(
		'submitdiv',
		__( 'Registration Status', 'leading-university' ),
		function( $post ){
			?>
			<div class="submitbox" id="submitpost">

				<div id="minor-publishing">
					<div class="misc-pub-section">
						<div class="input-group">
							<label for="status"><?php _e( 'Status', 'leading-university' ); ?></label>
							<?php
							$statuses = array(
								'draft' => array(
									'name'     => __( 'Draft', 'leading-university' ),
									'disabled' => ( !current_user_can( 'approve_registration' ) && $post->post_status != 'draft' ),
									'selected' => $post->post_status == 'draft' ),
								'submitted' => array(
									'name'     => __( 'Submitted', 'leading-university' ),
									'disabled' => ( !current_user_can( 'approve_registration' ) && $post->post_status != 'submitted' ),
									'selected' => $post->post_status == 'submitted' ),
								'review' => array(
									'name'     => __( 'Under Review', 'leading-university' ),
									'disabled' => ( !current_user_can( 'approve_registration' ) && $post->post_status != 'review' ),
									'selected' => $post->post_status == 'review' ),
								'approved' => array(
									'name'     => __( 'Approved', 'leading-university' ),
									'disabled' => ( !current_user_can( 'approve_registration' ) && !current_user_can( 'confirm_registration' )  && $post->post_status != 'approved' ),
									'selected' => $post->post_status == 'approved' ),
								'registered' => array(
									'name'     => __( 'Registered', 'leading-university' ),
									'disabled' => ( !current_user_can( 'confirm_registration' ) && $post->post_status != 'registered' ),
									'selected' => $post->post_status == 'registered' ) );?>
							<select name="status" id="status">
								<?php
								foreach( $statuses as $k => $status ){
									echo '<option value="'.$k.'"'.($status['disabled']?' disabled="disabled"':'').($status['selected']?' selected="selected"':'').'>'.$status['name'].'</option>';
								}
								?>
							</select>
						</div>
						<div class="notice notice-error" ng-if="false!=error||creditExceed()">
							<p ng-if="false!=error">{{error}}</p>
							<p ng-if="creditExceed()">
								<?php _e( 'Only {{credit_limit}} credits are allowed to be taken per semester.', 'leading-university' );?>
							</p>
						</div>
					</div>
				</div>
				
				<div id="major-publishing-actions">
					<div id="delete-action">
						<a class="submitdelete deletion" href="<?php echo get_delete_post_link( $post->ID ); ?>"><?php _e( 'Move to Trash', 'leading-university' ); ?></a>
					</div>
				
					<div id="publishing-action">
						<span class="spinner"></span>
						<input type="submit" name="publish" id="publish" class="button button-primary button-large" value="<?php _e( 'Save', 'leading-university' ); ?>" ng-disabled="freez_registration||creditExceed()">
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<?php
			wp_nonce_field( 'save_registration', 'lu_nonce' );
		},
		'registration',
		'side',
		'high' );
});

/**
 * Create the metabox(es) required for Program post type.
 */
add_action( 'add_meta_boxes_program', function(){
	add_meta_box(
		'offering',
		__( 'Course Offerings', 'leading-university' ),
		function( $post ){
			?>
			<div ng-controller="offering">
				<table class="striped">
					<thead>
						<tr>
							<th class="offering-semester"><?php _e( 'Semester', 'leading-university' ); ?></th>
							<th class="offering-course"><?php _e( 'Course', 'leading-university' ); ?></th>
							<th class="offering-sections"><?php _e( 'Sections', 'leading-university' ); ?></th>
							<th class="add-semester">
								<input type="button" ng-click="addSemester($event)" class="button" value="<?php _e( 'Add Semester', 'leading-university' ); ?>" />
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th class="offering-semester"><?php _e( 'Semester', 'leading-university' ); ?></th>
							<th class="offering-course"><?php _e( 'Course', 'leading-university' ); ?></th>
							<th class="offering-sections"><?php _e( 'Sections', 'leading-university' ); ?></th>
							<th class="add-semester">
								<input type="button" ng-click="addSemester($event)" class="button" value="<?php _e( 'Add Semester', 'leading-university' ); ?>" />
							</th>
						</tr>
					</tfoot>
					<tbody>
						<tr ng-repeat="semester in offering">
							<td class="offering-semester"><input type="text" name="offering[{{$index}}][semester]" ng-model="semester.semester"></td>
							<td class="offering-course">
								<ui-select multiple ng-model="semester.courses" theme="select2" ng-disabled="disabled" style="width: 300px;" append-to-body="false">
									<ui-select-match placeholder="Select courses...">{{$item.code}} ({{$item.syllabus_name}})</ui-select-match>
									<ui-select-choices repeat="ccode.ID as ccode in courseList | filter: $select.search">
										<div>{{ccode.code}}</div>
										<small>
											<?php _e( 'Title:', 'leading-university' );?> {{ccode.title}}<br>
											<?php _e( 'Syllabus:', 'leading-university' );?> {{ccode.syllabus_name}}<br>
											<?php _e( 'Credit:', 'leading-university' );?> <span ng-bind-html="''+ccode.credit | highlight: $select.search"></span>
										</small>
									</ui-select-choices>
								</ui-select>
								<input type="hidden" name="offering[{{$index}}][courses]" value="{{semester.courses}}">
							</td>
							<td class="offering-sections">
								<ui-select multiple tagging tagging-label="" ng-model="semester.sections" theme="select2" tagging-tokens=",|ENTER" style="width: 150px;">
									<ui-select-match placeholder="<?php _e( 'Separate sections by comma...', 'leading-university' ); ?>">{{$item}}</ui-select-match>
									<ui-select-choices repeat="section in availableSections | filter:$select.search">
										{{section}}
									</ui-select-choices>
								</ui-select>
								<input type="hidden" name="offering[{{$index}}][sections]" value="{{semester.sections}}">
							</td>
							<td class="remove-semester">
								<button ng-click="removeSemester($index)" class="button">
									<?php _e( 'Remove', 'leading-university' ); ?>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			
			wp_nonce_field( 'save_program', 'lu_nonce' );
		},
		'program',
		'normal',
		'core' );
		
	add_meta_box(
		'program-settings',
		__( 'Program Settings', 'leading-university' ),
		function(){
			global $post;
			?>
			<div ng-controller="settings" class="lu-jquery-ui">
				<table class="form-table" id="program-meta">
					<tbody>
						<tr>
							<th scope="row">
								<label for="current-semester"><?php _e( 'Current Semester', 'leading-university' ); ?></label>
							</th>
							<td>
								<?php lu_create_select_field( 
								array( 'Spring', 'Summer', 'Fall' ), 
								get_post_meta( $post->ID, 
								'_semester_season', true ), 
								array( 
									'id' => 'semester-season', 
									'name' => 'program_settings[semester_season]',
									'style' => 'width:200px' ) ); ?>
								<input type="text" name="program_settings[semester_year]" id="semester_year" placeholder="<?php _e( 'Year', 'leading-university' ); ?>" value="<?php echo get_post_meta( $post->ID, '_semester_year', true ); ?>" />
							</td>
						</tr>
                        <tr>
                            <th scope="row">
                                <lable for="evaluation-semester"><?php _e( "Evaluation Semester", 'leading-university' ); ?></lable>
                            </th>
                            <td>
                                <?php lu_semester_select_dropdown(
                                        get_post_meta( $post->ID, '_evaluation_semester', true ),
                                        array(
                                            'id' => 'evaluation-semester',
                                            'name' => 'program_settings[evaluation_semester]'
                                        )
                                    ); ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <lable for="registration-open"><?php _e( "Registration Open", 'leading-university' ); ?></lable>
                            </th>
                            <td>
                                <input type="checkbox" name="program_settings[registration_open]" ng-checked="'on'==registration_open">
                            </td>
                        </tr>
						<tr>
							<th scope="row">
								<lable for="credit-limit"><?php _e( "Credit per Semester", 'leading-university' ); ?></lable>
							</th>
							<td>
								<input type="text" name="program_settings[credit_limit]" value="<?php echo get_post_meta( $post->ID, '_credit_limit', true );?>">
							</td>
						</tr>
					</tbody>
				</table>
				<table id="program-fees">
					<thead>
						<tr>
							<th class="starting-id">
								<?php _e( 'Starting ID', 'leading-university' ); ?>
							</th>
							<th class="ending-id">
								<?php _e( 'Ending ID', 'leading-university' ); ?>
							</th>
							<th class="tution-fee">
								<?php _e( 'Tution Fee(per credit)', 'leading-university' ); ?>
							</th>
							<th class="activity-fee">
								<?php _e( 'Other fees(per semester)', 'leading-university' ); ?>
							</th>
							<th class="syllabus">
								<?php _e( 'Syllabus', 'leading-university' ); ?>
							</th>
							<th class="add-row">
								<input type="button" ng-click="addRow($event)" class="button" value="<?php _e( 'Add Row', 'leading-university' ); ?>" />
							</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="row in fees">
							<td class="starting-id">
								<input type="text" name="program_settings[fees][{{$index}}][starting_id]" ng-model="row.starting_id">
							</td>
							<td class="ending-id">
								<input type="text" name="program_settings[fees][{{$index}}][ending_id]" ng-model="row.ending_id">
							</td>
							<td class="tution-fee">
								<input type="text" name="program_settings[fees][{{$index}}][tution_fee]" ng-model="row.tution_fee">
							</td>
							<td class="activity-fee">
								<input type="text" name="program_settings[fees][{{$index}}][other_fees]" ng-model="row.other_fees">
							</td>
							<td class="syllabus">
								<?php lu_create_select_field( 
									lu_get_program_syllabuses(), 
									'', 
									array( 
										'name' => 'program_settings[fees][{{$index}}][syllabus]', 
										'ng-model' => 'row.syllabus' ),
									true ); ?>
							</td>
							<td class="add-row">
								<button ng-click="removeRow($index)" class="button">
									<?php _e( 'Remove Row', 'leading-university' ); ?>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
		},
		'program',
		'advanced',
		'core' );
		
	add_meta_box(
		'schedule',
		__( 'Schedule', 'leading-university' ),
		function(){
			?>
			<div ng-controller="schedule" class="lu-jquery-ui">
				<?php lu_print_schedule_table( __( 'Saturday', 'leading-university' ), 'saturday' ); ?>
				<?php lu_print_schedule_table( __( 'Sunday', 'leading-university' ), 'sunday' ); ?>
				<?php lu_print_schedule_table( __( 'Monday', 'leading-university' ), 'monday' ); ?>
				<?php lu_print_schedule_table( __( 'Tuesday', 'leading-university' ), 'tuesday' ); ?>
				<?php lu_print_schedule_table( __( 'Wednesday', 'leading-university' ), 'wednesday' ); ?>
				<?php lu_print_schedule_table( __( 'Thursday', 'leading-university' ), 'thursday' ); ?>
				<?php lu_print_schedule_table( __( 'Friday', 'leading-university' ), 'friday' ); ?>
				<?php lu_print_schedule_table( __( 'One Time Event', 'leading-university' ), 'one_off', true ); ?>
			</div>
			<?php
		},
		'program',
		'normal',
		'core' );
	
	if( current_user_can( 'edit_others_programs' ) ):
	add_meta_box(
		'department',
		__( 'Department', 'leading-university' ),
		'lu_select_department_metabox_content',
		'program',
		'side',
		'core' );
	endif;
});

/**
 * Create the metabox(es) required for Syllabus post type
 */
add_action( 'add_meta_boxes_syllabus', function(){
	add_meta_box(
		'courses',
		__( 'Courses', 'leading-university' ),
		function( $post ){
			?>
			<div ng-controller="courses">
				<table class="striped">
					<thead>
						<tr>
							<th class="course-code"><?php _e( 'Course Code', 'leading-university' );?></th>
							<th><?php _e( 'Course Title', 'leading-university' );?></th>
							<th class="course-credit"><?php _e( 'Credit', 'leading-university' );?></th>
							<th class="course-prerequisite"><?php _e( 'Prerequisite(s)', 'leading-university' );?></th>
							<th class="add-course">
								<input type="button" ng-click="addCourse($event)" class="button" value="<?php _e( 'Add Course', 'leading-university' ); ?>" />
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th class="course-code"><?php _e( 'Course Code', 'leading-university' );?></th>
							<th><?php _e( 'Course Title', 'leading-university' );?></th>
							<th class="course-credit"><?php _e( 'Credit', 'leading-university' );?></th>
							<th class="course-prerequisite"><?php _e( 'Prerequisite(s)', 'leading-university' );?></th>
							<th class="add-course">
								<input type="button" ng-click="addCourse($event)" class="button" value="<?php _e( 'Add Course', 'leading-university' ); ?>" />
							</th>
						</tr>
					</tfoot>
					<tbody>
						<tr ng-repeat="course in courses">
							<td class="course-code"><input type="text" name="courses[{{$index}}][code]" ng-model="course.code"></td>
							<td><input type="text" name="courses[{{$index}}][title]" ng-model="course.title"></td>
							<td class="course-credit"><input type="text" name="courses[{{$index}}][credit]" ng-model="course.credit"></td>
							<td class="course-prerequisite">
								<ui-select multiple="true" ng-model="course.prerequisits" theme="select2" ng-disabled="disabled" style="width:100%;">
									<ui-select-match placeholder="<?php _e('Enter prerequisites...', 'leading-university' );?>">{{$item.code}}</ui-select-match>
									<ui-select-choices repeat="course.code as course in courses| filter:$select.search">
										{{course.code}}
									</ui-select-choices>
								</ui-select>
								<input type="hidden" name="courses[{{$index}}][prerequisites]" value="{{course.prerequisits}}">
							</td>
							<td class="remove-course">
								<button ng-click="removeCourse($index)" class="button">
									<?php _e( 'Remove', 'leading-university' ); ?>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			wp_nonce_field( 'save_syllabus', 'lu_nonce' );
		}, 
		'syllabus', 
		'normal',
		'core' );
		
		if( current_user_can( 'edit_others_syllabuses' ) ):
		add_meta_box(
			'department',
			__( 'Department', 'leading-university' ),
			'lu_select_department_metabox_content',
			'syllabus',
			'side',
			'core' );
		endif;
		
		add_meta_box(
			'program',
			__( 'Program', 'leading-university' ),
			'lu_select_program_metabox_content',
			'syllabus',
			'side',
			'core' );
});


/**
 * Get the schedule table by day.
 * 
 * @param string $day_name Name of the day to be displayed in heading or any text
 * @param string $day_slug Day name to be stored in db
 * @param bool $date Wheather to include the date field(custom day)
 */
function lu_print_schedule_table( $day_name, $day_slug, $date = false ){
	?>
	<h3><?php echo $day_name; ?></h3>
	<div>
		<table class="schedule-day striped">
			<thead>
				<tr>
					<?php if( $date ): ?>
					<th class="schedule-date"><?php _e( 'Date', 'leading-university' ); ?></th>
					<?php endif; ?>
					<th class="schedule-semester-section"><?php _e( 'Semester', 'leading-university' ); ?></th>
					<th class="schedule-course-event"><?php _e( 'Course/Event Name', 'leading-university' ); ?></th>
					<th class="schedule-start-time"><?php _e( 'Start Time', 'leading-university' ); ?></th>
					<th class="schedule-duration"><?php _e( 'Duration (m)', 'leading-university' ); ?></th>
					<th class="schedule-teacher"><?php _e( 'Teacher', 'leading-university' ); ?></th>
					<th class="schedule-room"><?php _e( 'Room', 'leading-university' ); ?></th>
					<th class="schedule-add-row"><input type="button" ng-click="addRow('<?php echo $day_slug; ?>', $event)" class="button" value="<?php _e( 'Add Row', 'leading-university' ); ?>" /></th>
					<th class="schedule-disable"><?php _e( 'Disable', 'leading-university' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="row in schedule.<?php echo $day_slug; ?>">
					<?php if( $date ): ?>
					<td class="schedule-date">
						<input type="text" name="schedule[{{row.day}}{{$index}}][date]"  ui-date="{ dateFormat: 'yy-mm-dd' }" ng-model="row.date">
					</td>
					<?php endif; ?>
					<td class="schedule-semester-section">
						<ui-select ng-model="row.section" theme="select2" ng-disabled="disabled" style="width: 70px;" append-to-body="true">
							<ui-select-match placeholder="<?php _e( 'Select section...', 'leading-university' );?>">{{$select.selected}}</ui-select-match>
							<ui-select-choices repeat="section in sections() | filter: $select.search">
								{{section}}
							</ui-select-choices>
						</ui-select>
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][section]" value="{{row.section}}">
					</td>
					<td class="schedule-course-event">
						<ui-select tagging="tagTransform" tagging-label="false" ng-model="row.course" theme="select2" ng-disabled="disabled" style="width: 150px;" append-to-body="true">
							<ui-select-match placeholder="<?php _e( 'Select course...', 'leading-university' );?>">{{$select.selected.isCustom ? $select.selected.title: $select.selected.code}}</ui-select-match>
							<ui-select-choices repeat="course in courseList | propsFilter: {title: $select.search, code: $select.search}">
								<div ng-bind-html="course.code | highlight: $select.search"></div>
								<small>
									<?php _e( 'Title:', 'leading-university' );?> {{course.title}}<br>
									<?php _e( 'Syllabus:', 'leading-university' );?> {{course.syllabus_name}}<br>
									<?php _e( 'Credit:', 'leading-university' );?> <span ng-bind-html="''+course.credit | highlight: $select.search"></span>
								</small>
							</ui-select-choices>
						</ui-select>
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][course]" value="{{row.course}}">
					</td>
					<td class="schedule-start-time">
						<input type="text" name="schedule[{{row.day}}{{$index}}][start_time]" ui-timepicker="timePickerOptions" ng-model="row.start_time">
					</td>
					<td class="schedule-duration">
						<input type="text" name="schedule[{{row.day}}{{$index}}][duration]" ng-model="row.duration">
					</td>
					<td class="schedule-teacher">
						<ui-select ng-model="row.teacher" theme="select2" ng-disabled="disabled" style="min-width: 150px; width:100%" append-to-body="true">
							<ui-select-match placeholder="<?php _e( 'Select teacher...', 'leading-university' );?>">{{$select.selected.name}}</ui-select-match>
							<ui-select-choices repeat="teacher.ID as teacher in teachers | filter: $select.search">
								{{teacher.name}} <br>
								<small>{{teacher.designation}}, {{teacher.department.name}}</small>
							</ui-select-choices>
						</ui-select>
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][teacher]" value="{{row.teacher}}">
					</td>
					<td class="schedule-room">
						<ui-select ng-model="row.room" theme="select2" ng-disabled="disabled" style="width: 100px;" append-to-body="true">
							<ui-select-match placeholder="<?php _e( 'Select room...', 'leading-university' );?>">{{$select.selected.name}}</ui-select-match>
							<ui-select-choices repeat="room.term_id as room in rooms | filter: $select.search">
								{{room.name}}
							</ui-select-choices>
						</ui-select>
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][room]" value="{{row.room}}">
					</td>
					<td class="schedule-remove-row">
						<button ng-click="removeRow(row.day, $index)" class="button">
							<?php _e( 'Remove', 'leading-university' ); ?>
						</button>
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][day]" value="{{row.day}}" />
						<input type="hidden" name="schedule[{{row.day}}{{$index}}][ID]" value="{{row.ID}}" />
					</td>
					<td class="schedule-disable">
						<input type="checkbox" name="schedule[{{row.day}}{{$index}}][disable]" ng-checked="1==row.disable">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
}

/**
 * Metabox content to select department
 */
function lu_select_department_metabox_content( $post ){
	$cur_dept = get_post_meta( $post->ID, '_department', true );
	?>
	<label for="entity-department"><?php _e( 'Department this entity belongs to:', 'leading-university' );?></label>
	<?php
	lu_department_select_dropdown( 'entity_department', $cur_dept, 'entity-department' );
}

/**
 * Metabox content to select program
 */
function lu_select_program_metabox_content( $post ){
	$cur_program = get_post_meta( $post->ID, '_program', true );
	?>
	<label for="entity-program"><?php _e( 'Program this entity belongs to:', 'leading-university' );?></label>
	<select name="entity_program" id="entity-program">
		<option value="0"><?php _e( 'Select...', 'leading-university' ); ?></option>
		<?php
		$query_args = array(
			'post_type'      => 'program',
			'posts_per_page' => -1,
			'post_status'    => 'publish' );
		if( !current_user_can( 'edit_others_programs' ) )
			$query_args['meta_query'] = array(
				array(
					'key'   => '_department',
					'value' => get_user_meta( get_current_user_id(), 'department', true ) ) );
					
		$programs = get_posts( $query_args );
		foreach( $programs as $program ):
		?>
		<option value="<?php echo $program->ID;?>" <?php echo $program->ID == $cur_program ? 'selected="selected"':''; ?>>
			<?php echo $program->post_title; ?>
		</option>
		<?php endforeach; ?>
	</select>
	<?php
}

/**
 * Add metaboxes for the evaluation summary page.
 */
add_action( 'add_meta_boxes_evaluation_report', function(){
    add_meta_box('filter', __( "Filter", 'leading-university'), function(){
        extract(shortcode_atts(array(
            'teacher_id' => 0,
            'semester_id' => 0,
            'course_id' => 0,
            'section' => ''
        ), $_GET));
        ?>
        <div class="notice notice-info" ng-if="updating"><?php _e( 'Updating...', 'leading-university' );?></div>

        <div>
            <p class="post-attributes-label-wrapper">
                <label class="post-attributes-label" for="teacher-id">Teacher</label>
            </p>
            <ui-select ng-model="teacher" theme="select2" ng-disabled="disabled" style="min-width: 150px; width:100%" append-to-body="true" on-select="updateFilter('teacher', $item, $model)">
                <ui-select-match placeholder="<?php _e( 'Select teacher...', 'leading-university' );?>">{{$select.selected.name}}</ui-select-match>
                <ui-select-choices repeat="teacher.ID as teacher in teachers | filter: $select.search">
                    {{teacher.name}} <br>
                    <small>{{teacher.designation}}, {{teacher.department.name}}</small>
                </ui-select-choices>
            </ui-select>

            <p class="post-attributes-label-wrapper">
                <label class="post-attributes-label" for="semester-id">Semester</label>
            </p>
            <ui-select ng-model="semester" theme="select2" ng-disabled="disabled" style="min-width: 150px; width:100%" append-to-body="true" on-select="updateFilter('semester', $item, $model)">
                <ui-select-match placeholder="<?php _e( 'Select semester...', 'leading-university' );?>">{{$select.selected.name}}</ui-select-match>
                <ui-select-choices repeat="semester.term_id as semester in semesters | filter: $select.search">
                    {{semester.name}}
                </ui-select-choices>
            </ui-select>

            <p class="post-attributes-label-wrapper">
                <label class="post-attributes-label" for="course-id">Course</label>
            </p>
            <ui-select ng-model="course" theme="select2" ng-disabled="disabled" style="min-width: 150px; width:100%" append-to-body="true" on-select="updateFilter('course', $item, $model)">
                <ui-select-match placeholder="<?php _e( 'Select course...', 'leading-university' );?>">{{$select.selected.isCustom ? $select.selected.title: $select.selected.code}}</ui-select-match>
                <ui-select-choices repeat="course.ID as course in courses | propsFilter: {title: $select.search, code: $select.search}">
                    <div ng-bind-html="course.code | highlight: $select.search"></div>
                    <small>
                        <?php _e( 'Title:', 'leading-university' );?> {{course.title}}<br>
                        <?php _e( 'Syllabus:', 'leading-university' );?> {{course.syllabus_name}}<br>
                        <?php _e( 'Credit:', 'leading-university' );?> <span ng-bind-html="''+course.credit | highlight: $select.search"></span>
                    </small>
                </ui-select-choices>
            </ui-select>

            <p class="post-attributes-label-wrapper">
                <label class="post-attributes-label" for="section">Section</label>
            </p>
            <ui-select ng-model="section" theme="select2" ng-disabled="disabled" style="min-width: 150px; width:100%" append-to-body="true" on-select="updateFilter('section', $item, $model)">
                <ui-select-match placeholder="<?php _e( 'Section...', 'leading-university' );?>">{{$select.selected}}</ui-select-match>
                <ui-select-choices repeat="section in sections | filter: $select.search">
                    {{section}}
                </ui-select-choices>
            </ui-select>
        </div>
        <?php
    }, 'evaluation_report', 'side');

    add_meta_box('summary', __( "Summary", 'leading-university' ), function(){
        $aspects = lute_aspects();
        ?>
        <table class="lute-evaluation-report-table" border="1">
            <?php
            foreach($aspects as $aspect) {
                echo '<tr><th colspan="2">'.$aspect->name.'</th></tr>';
                ?>
                <tr>
                    <th scope="col"><?php _e('Question', 'leading-university'); ?></th>
                    <th scope="col"><?php _e('Score', 'leading-university'); ?></th>
                </tr>
                <?php

                foreach($aspect->questions as $question) {
                    echo '<tr><td>'.$question->name.'</td><td>{{scores['.$question->ID.'] | number : 2}}</td></tr>';
                }
                echo '<tr><th scope="col">'.__('Average', 'leading-university').'</th><th scope="col">{{scores['.$aspect->ID.'] | number : 2}}</th></tr>';
                echo '<tr class="placeholder"></tr>';
            }
            echo '<tr><th scope="col">'.__('Overall', 'leading-university').'</th><td scope="col">{{scores[0] | number : 2}}</td></tr>';
            echo '<tr><th scope="col">'.__('Number of Evaluations', 'leading-university').'</th><td scope="col">{{count}}</td></tr>';
            echo '<tr><th scope="col">'.__('Points', 'leading-university').'</th><td scope="col">{{scores[0]*100 | number : 0}}</td></tr>';
            echo '<tr><th scope="col">'.__('Remark', 'leading-university').'</th><td scope="col">{{remark(scores[0]*100)}}</td></tr>';
            ?>
            </tbody>
        </table>
    <?php
    },'evaluation_report', 'normal');

    add_meta_box( 'scale', __( "Scale", 'leading-university' ), function(){
        ?>
        <table class="lute-evaluation-report-table" border="1">
            <thead>
            <tr>
                <th scope="col">Scale</th>
                <th scope="col">Points</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Unsatisfactory</td>
                <td>100 - 209</td>
            </tr>
            <tr>
                <td>Developing</td>
                <td>210 - 284</td>
            </tr>
            <tr>
                <td>Proficient</td>
                <td>285 - 339</td>
            </tr>
            <tr>
                <td>Excellent</td>
                <td>340 - 400</td>
            </tr>
            <tr>
                <td>Outstanding</td>
                <td>401 - 500</td>
            </tr>
            </tbody>
        </table>
        <?php
    }, 'evaluation_report', 'side');
});
