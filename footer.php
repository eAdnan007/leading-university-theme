<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Leading University
 */
?>

<?php get_sidebar( 'footer' ); ?>

<footer class="footer-bottom">
	<div class="container">
		
		
		<div class="row">
			<div class="footer-nav col-md-12 col-sm-12 col-xs-12">
				<?php
				wp_nav_menu( array(
					'theme_location'  => 'footer',
					'menu_class'      => 'nav navbar-nav',
					'container'       => 'nav',
					'container_class' => 'footer-navbar',
					'fallback_cb'     => false,
					'depth'           => 1,
					) );
				?>
			</div>

			<div class="copyright col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="container">
					<hr class="visible-print-block">
					<p>
						Copyright <?php echo date('Y'); ?> Leading University, all rights reserved. Developed by <a href="//deviserweb.com" target="_blank">DeviserWeb</a>.<br>
						This site is maintained by CSE & IT Department.
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
