<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Leading University
 */

if ( ! is_active_sidebar( 'sidebar' ) ) {
	return;
}
?>
<div class="sidebar sidebar-primary col-md-4 col-sm-12 col-xs-12 pull-right">
	<div class="row">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</div>
</div>
