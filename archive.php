<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Leading University
 */

get_header(); ?>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<?php if ( have_posts() ) : ?>
						<header class="page-header">
							<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="taxonomy-description">', '</div>' );
							?>
						</header><!-- .page-header -->
						
						<div class="archive">
							<?php while ( have_posts() ) : the_post(); ?>
					
								<?php
									/* Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'snippet', get_post_format() );
								?>
							<?php endwhile; ?>
					
							<?php wp_bootstrap_pagination(); ?>
						</div>
						
					<?php else : ?>
				
						<?php get_template_part( 'content', 'none' ); ?>
	
					<?php endif; ?>
				</div>
				
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
