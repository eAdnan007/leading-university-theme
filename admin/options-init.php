<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "lu_theme_conf";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'lu_theme_conf',
        'use_cdn' => false,
        'display_name' => 'Leading University - Additional Settings',
        'page_title' => 'Leading University Additional Configuration',
        'update_notice' => true,
        'menu_type' => 'submenu',
        'menu_title' => 'More Settings',
        'allow_sub_menu' => true,
        'page_parent' => 'options-general.php',
        'page_parent_post_type' => 'your_post_type',
        'default_mark' => '*',
        'dev_mode' => false,
        'hints' => array(
            'icon' => 'el el-bulb',
            'icon_position' => 'right',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output_tag' => true,
        'settings_api' => true,
        'cdn_check_time' => '1440',
        'global_variable' => 'lu_config',
        'page_permissions' => 'manage_options',
        'save_defaults' => true,
        'show_import_export' => true,
        'disable_save_warn' => true,
        'open_expanded' => false,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => true,
        'system_info' => true,
    );

    Redux::setArgs( $opt_name, $args );


    /*
     *
     * ---> START SECTIONS
     *
     */

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Result API', 'leading-university' ),
        'id'     => 'result_api',
        'desc'   => __( 'Configure connection with the result server.', 'leading-university' ),
        'icon'   => 'el el-file',
        'fields' => array(
            array(
                'id'       => 'result_request_url',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => __( 'Request URI', 'leading-university' )
            ),
            array(
                'id'       => 'result_public_key',
                'type'     => 'text',
                'title'    => __( 'Public Key', 'leading-university' )
            ),
            array(
                'id'       => 'result_private_key',
                'type'     => 'text',
                'title'    => __( 'Private Key', 'leading-university' )
            ),
            array(
                'id'       => 'result_request_timeout',
                'type'     => 'text',
                'title'    => __( 'Timeout', 'leading-university' ),
                'subtitle' => __( 'Request Timeout Duration in seconds.', 'leading-university' ),
                'default'  => 10
            )
        )
    ) );
    
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Academic Settings', 'leading-university' ),
        'id'     => 'academic',
        'desc'   => __( 'Define academic constraints.', 'leading-university' ),
        'icon'   => 'el el-file',
        'fields' => array(
            array(
                'id'       => 'retake_gpa_limit',
                'type'     => 'text',
                'title'    => __( 'Retake Maximum GPA', 'leading-university' ),
                'subtitle' => __( 'To take a retake, previous gpa for the same course should be below this level.', 'leading-university' )
            ),
            array(
                'id'       => 'registration_status_message',
                'type'     => 'editor',
                'title'    => __( 'Registration Status Change Notification', 'leading-university' ),
                'subtitle' => __( 'The notification message to be sent when registration status has been changed. You can use {status}, {semester}, {student_name} and {student_id} as placeholders.', 'leading-university' )
            )
        )
    ) );
    /*
     * <--- END SECTIONS
     */
