<?php
/**
 * @package Leading University
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'box body' ); ?>>
		<div class="row">
			<?php 
			if ( has_post_thumbnail() ):?>
			<div class="col-sm-4">
				<div class="featured-image-container">
					<?php 
					the_post_thumbnail( 'archive-feat-image-lg', array( 'class' => 'featured-image img-responsive visible-lg-block' ) );
					the_post_thumbnail( 'archive-feat-image-md', array( 'class' => 'featured-image img-responsive visible-md-block visible-sm-block' ) );
					the_post_thumbnail( 'archive-feat-image-xs', array( 'class' => 'featured-image img-responsive visible-xs-block' ) );
					?>
				</div>
			</div>
			<?php endif; ?>
			<div class="<?php echo has_post_thumbnail() ? 'col-sm-8' : 'col-sm-12';?>">
				<header class="entry-header">
					<a href="<?php the_permalink(); ?>"><?php the_title( '<h1 class="entry-title">', '</h1>' ); ?></a>
			
					<div class="entry-meta">
						<?php // leading_university_posted_on(); ?>
					</div><!-- .entry-meta -->
				</header><!-- .entry-header -->
			
				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div><!-- .entry-content -->
			
				<footer class="entry-footer">
					<?php // leading_university_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</div>
		</div>
	</article><!-- #post-## -->
