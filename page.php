<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Leading University
 */

get_header(); ?>
	
	<section class="intro">
		<div class="container">
			<div class="row">
				<?php while ( have_posts() ) : the_post(); ?>
			
					<?php get_template_part( 'content', 'page' ); ?>
			
				<?php endwhile; // end of the loop. ?>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
