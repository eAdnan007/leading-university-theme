(function($){

    var question_update_callback = function () {
        var aspect = $(this).closest('.aspect');
        var i = 0;
        var aspect_index = aspect.find('[name="aspect_index[]"]').val();

        // console.log(aspect);

        aspect.find('.question').each(function(){
            $(this).find('input[name="question_index[]"]').val(i++);
            $(this).find('input[name="question_aspect_index[]"]').val(aspect_index);
        })
    };

    $(document).ready(function(){
        $('.aspects').sortable({
            handle: '.aspect-move',
            update: function(e, ui) {
                console.log(ui);
                var i = 0;
                $('.aspects .aspect').each(function(){
                    $(this).find('input[name="aspect_index[]"]').val(i);

                    $(this).find('.question').each(function(){
                        $(this).find('input[name="question_aspect_index[]"]').val(i);
                    });

                    i++;
                });
            }
        });

        $('.aspects .questions').sortable({
            handle: '.question-move',
            connectWith: '.questions',
            update: question_update_callback
        });

        //Activate the aspect remove button
        $('.aspects').on('click', '.aspect-remove', function(){
            var aspect = $(this).closest('.aspect');

            if(aspect.hasClass('removed')){
                // Restore
                aspect.removeClass('removed');
                aspect.find('[name="aspect_removed[]"]').val('no');
                aspect.find('[type="text"]').removeAttr('readonly');
                aspect_body.find('button').removeAttr('disabled');
            }
            else {
                // Delete
                aspect.addClass('removed');
                aspect.find('[name="aspect_removed[]"]').val('yes');
                aspect.find('[type="text"]').attr('readonly', 'readonly');
                aspect_body.find('button').attr('disabled', 'disabled');
            }
        });

        // Activate add question button
        $('.aspects').on('click', '.add-question', function(){
            var aspect = $(this).closest('.aspect');
            var questions = aspect.find('.questions')

            var question_count = aspect.find('.questions .question').size();
            var aspect_index = aspect.find('input[name="aspect_index[]"]').val();

            var question =  $('<div class="question"></div>');
            var remove_question_btn = $('<button type="button" class="button button-default question-remove">' +
                '<span class="dashicons dashicons-trash"></span>' +
                '</button>');

            question.append('<span class="dashicons-move dashicons question-move"></span>'+
                '<input type="text" name="question_weight[]" value="1" class="question-weight" placeholder="Weight">' +
                '<input type="text" name="question_name[]" class="question-name" placeholder="The question...">');

            question.append(remove_question_btn);

            question.append('<input type="hidden" name="question_removed[]" value="no">' +
                '<input type="hidden" name="question_aspect_index[]" value="'+aspect_index+'">' +
                '<input type="hidden" name="question_index[]" value="'+question_count+'">' +
                '<input type="hidden" name="question_id[]" value="0">'
            );

            questions.append(question);

        });

        // Activate remove question button
        $('.aspects').on('click', '.question-remove', function(){
            var question = $(this).closest('.question');

            if(question.hasClass('removed')){
                // Restore
                question.removeClass('removed');
                question.find('[name="question_removed[]"]').val('no');
                question.find('[type="text"]').removeAttr('readonly');
            }
            else {
                // Delete
                question.addClass('removed');
                question.find('[name="question_removed[]"]').val('yes');
                question.find('[type="text"]').attr('readonly', 'readonly');
            }
        });
    });

    $('#add-aspect').click(function(){
        var count = $('.aspects .aspect').size();
        var aspect = $('<div class="aspect"></div>');
        var remove_aspect_btn = $('<button type="button" class="button button-default aspect-remove">' +
            '<span class="dashicons dashicons-trash"></span>' +
            '</button>');

        var aspect_header = $('<div class="aspect-header"/>');
        aspect_header.append('<span class="dashicons-move dashicons aspect-move"></span>'+
            '<input type="text" name="aspect_weight[]" value="1" class="aspect-weight" placeholder="Weight">' +
            '<input type="text" name="aspect_name[]" class="aspect-name" placeholder="Specify a name for the aspect...">');
        aspect_header.append(remove_aspect_btn);
        aspect_header.append('<input type="hidden" name="aspect_removed[]" value="no">' +
            '<input type="hidden" name="aspect_index[]" value="'+count+'">' +
            '<input type="hidden" name="aspect_id[]" value="0">' +
            '</div>');


        var aspect_body = $('<div class="aspect-body wp-clearfix"/>');
        var add_question_btn = $('<button type="button" class="button button-default add-question">' +
            'Add Question</button>');

        var questions = $('<div class="questions" />');
        aspect_body.append(questions);
        aspect_body.append(add_question_btn);


        aspect.append(aspect_header);
        aspect.append(aspect_body);

        $('.aspects').append(aspect);

        $(aspect).find('.questions').sortable({
            handle: '.question-move',
            connectWith: '.questions',
            update: question_update_callback
        });
    });
})(jQuery);