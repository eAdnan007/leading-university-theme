(function(){

	var Results = angular.module('LULogin', ['angular-loading-bar']);
	
	Results.controller('LoginCTRL', ['$scope', '$http', '$sce', '$window', function($scope, $http, $sce, $window){
		
		$scope.errorData = {};
		$scope.hasError = false;
		
		$scope.login = function(){
			
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: $.param({
					action: 'login',
					username: $scope.username,
					password: $scope.password,
					rememberme: $scope.rememberme
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				if( true == content.success ) {
					$window.location.href = $scope.redirect_to;
				}
				else {
					$scope.hasError = true;
					$scope.errorData = $sce.trustAsHtml(content.errors);
				}
			});
		}
	}]);
})();
