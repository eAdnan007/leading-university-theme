(function(){

    var evaluationSummaryApp = angular.module('evaluationSummeryApp', ['ui.select', 'ngSanitize']);
    evaluationSummaryApp.controller( 'FilterCtrl', [ '$scope', '$http', function( $scope, $http ){
        $scope.teacher = null;
        $scope.semester = null;
        $scope.course = null;
        $scope.section = null;
        $scope.updating = false;

        $scope.teachers = luh.teachers;
        $scope.semesters = luh.semesters;
        $scope.remark = function(score){
            if(score != null){

                if(score >= 100 && score < 210) return 'Unsatisfactory';
                else if(score < 285) return 'Developing';
                else if(score < 340) return 'Proficient';
                else if(score < 401) return 'Excellent';
                else if(score <= 500) return 'Outstanding';
            }
            return 'Undefined';

        };

        $scope.courses = [{
            ID: 1,
            code: 'CSE-1111',
            title: 'Introduction To Computers',
            credit: 3,
            sections: ['A', 'B', 'C']
        }];

        $scope.updateFilter = function(key, item, model){
            $scope.updating = true;

            switch (key) {
                case 'course':
                    $scope.sections = item.sections;
                    $scope.course = model;
                    break;
                case 'teacher':
                    $scope.teacher = model;
                    break;
                case 'semester':
                    $scope.semester = model;
                    break;
                case 'section':
                    $scope.section = model;
                    break;
            }

            $http({
                url: ajaxurl,
                method: 'POST',
                data: jQuery.param({
                    action: 'evaluation_report_filter_options',
                    _wpnonce: luer.nonce,
                    teacher_id: $scope.teacher,
                    semester_id: $scope.semester,
                    course_id: $scope.course,
                    section: $scope.section
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(content, status){
                $scope.updating = false;

                if( true === content.success ){
                    $scope.courses = content.courses;
                    $scope.scores = content.scores;
                    $scope.count = content.count;
                }
            });
        }
    }]);

    evaluationSummaryApp.filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function(item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });
})();
