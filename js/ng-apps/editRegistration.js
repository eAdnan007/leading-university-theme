// Hack to let authorised people of the department manupulate the registration.
jQuery(document).ready(function($){
	$('<input id="post_author_override" type="hidden" name="post_author_override" />')
		.val( $('#user-id').val() )
		.prependTo('form');
});


(function(){
	/*global angular*/
	var editRegistrationApp = angular.module('editRegistrationApp', []);
	
	editRegistrationApp.controller( 'RegistrationCTRL', [ '$scope', '$http', function( $scope, $http ){
		
		$scope.courses = {};
		$scope.updating_student = false;
		$scope.offerings = [];
		$scope.freez_registration = false;
		$scope.error = false;
		
		/* global luh */
		if( typeof luh.student_id != 'undefined' ) $scope.student_id = luh.student_id;
		if( typeof luh.completed_credits != 'undefined' ) $scope.completed_credits = luh.completed_credits;
		if( typeof luh.courses != 'undefined' ) $scope.courses = luh.courses;
		if( typeof luh.batch_meta != 'undefined' ){
			if( typeof luh.batch_meta.tution_fee != 'undefined' ) $scope.tution_fee = luh.batch_meta.tution_fee;
			if( typeof luh.batch_meta.other_fees != 'undefined' ) $scope.other_fees = luh.batch_meta.other_fees;
		}
		if( typeof luh.waiver != 'undefined' ) $scope.waiver = luh.waiver;
		if( typeof luh.offering != 'undefined' ) $scope.offering = luh.offering;
		if( typeof luh.change != 'undefined' ) $scope.can_change = luh.change == '1' ? true : false;
		if( typeof luh.freez_registration != 'undefined' ) $scope.freez_registration = luh.freez_registration;
		if( typeof luh.registration_status != 'undefined' ) $scope.registration_status = luh.registration_status;
		if( typeof luh.error != 'undefined' ) $scope.error = luh.error;
		if( typeof luh.credit_limit != 'undefined' ) $scope.credit_limit = luh.credit_limit;
		
		$scope.registerSectionCourses = function(main_section){
			angular.forEach($scope.courses, function(c, i){
				var found_section = false;
				angular.forEach(c.sections, function(s){
					if( null != s && s.name == main_section ){
						found_section = true;
					}
				});
				
				if( found_section ){
					$scope.addCourse(c, main_section);
				}
				else {
					$scope.removeCourse(i);
				}
			});
			
		};
		
		$scope.hasRegularCourses = function(){
			var r = false;
			angular.forEach($scope.courses, function(c){
				if( c.type == 'regular' && c.registered ) r = true;
			});
			
			return r;
		};
		
		$scope.hasRetakeCourses = function(){
			var r = false;
			angular.forEach($scope.courses, function(c){
				if( c.type == 'retake' && c.registered ) r = true;
			});
			
			return r;
		};
		
		$scope.creditExceed = function(){
			if( $scope.creditCount() > $scope.credit_limit ) return true;
			return false;
		};
		
		$scope.sections = function(){
			var all_sections = [];
			angular.forEach( $scope.offering, function(o) {
				if( null != o.sections && o.sections.length > 1 ){
					angular.forEach( o.sections, function( section ){
						all_sections.push( o.semester + ' ('+section+')' );
					});
				}
				else {
					all_sections.push( o.semester );
				}
			});
			
			return all_sections;
		};
		
		$scope.removeCourse = function( i ){
			$scope.courses[i].registered = false;
		};
		
		$scope.addCourse =  function(newCourse, newSection){
			if( typeof newCourse == 'undefined' || null == newCourse ) return;
			if( !newCourse.retakable ) return;
			
			$scope.courses[newCourse.ID].registered = true;
			$scope.courses[newCourse.ID].reged_section = newSection;
			
			delete $scope.newCourse;
			delete $scope.newSection;
			
			return;
		};
		
		$scope.updateStudent = function(){
			if( '' == $scope.student_id ) return;
			$scope.updating_student = true;
			
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: 'action=get-student-registration-feed&student_id='+$scope.student_id+'&registration_id='+angular.element('#post_ID').val()+'&semester='+$scope.semester,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				$scope.updating_student = false;
				if(content.success == false) {
					$scope.failed = true;
					$scope.freez_registration = true;
					$scope.can_change = false;
					return;
				}
				
				if( content.error != false ){
					$scope.freez_registration = true;
					$scope.error = content.error;
					$scope.failed = true;
					$scope.can_change = false;
				}
				
				if( content.error == false && content.success == true ){
					$scope.failed = false;
					$scope.error = false;
					$scope.freez_registration = false;
					if( typeof luh.change != 'undefined' ) $scope.can_change = luh.change == '1' ? true : false;
					if( typeof luh.freez_registration != 'undefined' ) $scope.freez_registration = luh.freez_registration;
				}
				$scope.courses = content.courses;
				$scope.offering = content.offering;
				$scope.tution_fee = content.batch_meta.tution_fee;
				$scope.other_fees = content.batch_meta.other_fees;
				$scope.waiver     = content.waiver;
				$scope.credit_limit = content.credit_limit;
				$scope.student_name = content.student_name;
				$scope.completed_credits = content.completed_credits;
				$scope.existing = content.existing;
				$scope.current_semester = content.current_semester;
			});
		};
		
		$scope.creditCount = function( type ){
			if( typeof type == 'undefined' ) type = 'all';
			
			var sum = 0;
			for( var course in $scope.courses ) {
				if( $scope.courses.hasOwnProperty( course ) ) {
					if( ( ( type == $scope.courses[course].type ) || 'all' == type ) && $scope.courses[course].registered )
						sum += parseFloat( $scope.courses[course].credit );
				}
			}
			
			return sum;
		};
		
		$scope.prerequisiteSeverity = function( course ){
			var s = 'success';
			if( course.gpa == 0 ) s = 'warning';
			if( course.gpa == -1 ) s = 'error';
			
			return s;
		}
		
		
		$scope.tutionFees = function(){
			var waived_percentile = 100 - parseFloat( $scope.waiver );
			var waived_multiplier = waived_percentile/100;
			return (
				parseFloat( $scope.creditCount('regular') * $scope.tution_fee * waived_multiplier ) + 
				parseFloat( $scope.creditCount('retake') * $scope.tution_fee ) );
		};
		
		$scope.totalFees = function(){
			return $scope.tutionFees() + parseFloat( $scope.other_fees ) ;
		};
	}]);
	
	// html filter (render text as html)
	editRegistrationApp.filter('html', ['$sce', function ($sce) { 
		return function (text) {
			return $sce.trustAsHtml(text);
		};
	}])
	
	editRegistrationApp.filter( 'toArray', function(){
		return function( courses ){
			var vc = [];
			angular.forEach(courses, function( rc ){
				vc.push( rc );
			});
			
			return vc;
		};
	});
})();
