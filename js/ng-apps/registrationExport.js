(function(){
	/* global angular luh */
	var registrationExport = angular.module('registration-export', ['ui.select', 'ngSanitize']);
	
	registrationExport.controller('ExportCTRL', ['$scope', '$http', function($scope, $http){
		$scope.program = 0;
		$scope.courses = [];
		
		$scope.t = { selected_courses: [] };
		
		$scope.program_changed = function(){
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: jQuery.param({
					action: 'get-program-courses',
					program_id: $scope.program
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				$scope.courses = content.courses;
			});
		}
	}]);
	
	registrationExport.filter('propsFilter', function() {
		return function(items, props) {
			var out = [];
			
			if (angular.isArray(items)) {
				items.forEach(function(item) {
					var itemMatches = false;
					
					var keys = Object.keys(props);
					for (var i = 0; i < keys.length; i++) {
						var prop = keys[i];
						var text = props[prop].toLowerCase();
						if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
							itemMatches = true;
							break;
						}
					}
					
					if (itemMatches) {
						out.push(item);
					}
				});
			} else {
				// Let the output be the input untouched
				out = items;
			}
			
			return out;
		};
	});
})();
