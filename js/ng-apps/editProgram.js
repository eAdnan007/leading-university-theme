// Hack to let authorised people of the department manupulate the registration.
jQuery(document).ready(function($){
	$('<input id="post_author_override" type="hidden" name="post_author_override" />')
		.val( $('#user-id').val() )
		.prependTo('form');
});


(function(){
	var editProgramApp = angular.module('editProgramApp', ['ui.select', 'ngSanitize', 'ui.timepicker', 'ui.date']);
	
	editProgramApp.filter('propsFilter', function() {
		return function(items, props) {
			var out = [];
			
			if (angular.isArray(items)) {
				items.forEach(function(item) {
					var itemMatches = false;
					
					var keys = Object.keys(props);
					for (var i = 0; i < keys.length; i++) {
						var prop = keys[i];
						var text = props[prop].toLowerCase();
						if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
							itemMatches = true;
							break;
						}
					}
					
					if (itemMatches) {
						out.push(item);
					}
				});
			} else {
				// Let the output be the input untouched
				out = items;
			}
			
			return out;
		};
	});
	
	editProgramApp.controller( 'offering', [ '$scope', 'sections', function( $scope, sections ){
		
		$scope.offering = luh.offering;
		$scope.courseList = luh.courses;
		$scope.availableSections = [];
		
		if( typeof $scope.offering == 'undefined' ) $scope.offering = new Array;
		if( typeof $scope.courseList == 'undefined' ) $scope.courseList = new Array;
		
		// Remove the comma from sections if the item has comma at the end (often not removed automatically).
		$scope.stripComma = function( item, model ){
			model = model.replace(",", "", -1);
			item = model;
		}
		
		$scope.removeSemester = function( i ){
			$scope.offering.splice( i, 1 );
		}
		
		$scope.addSemester = function( e ){
			e.stopPropagation();
			
			$scope.offering.push({});
		}
		
		$scope.$watch(function(s){ return JSON.stringify( s.offering ); }, function(nv){
			var batch_sections = new Array;
			var offering = JSON.parse(nv);
			
			angular.forEach( offering, function(o){
				if( Object.prototype.toString.call( o.sections ) == '[object Array]' && o.sections.length > 0 ) {
					angular.forEach( o.sections, function(sec){
						batch_sections.push(o.semester + ' (' + sec + ')');
					});
				}
				else{
					batch_sections.push(o.semester);
				}
			});
			
			sections.set(batch_sections);
		});
	}]);
	
	editProgramApp.controller( 'schedule', [ '$scope', 'sections', function( $scope, sections ){
		
		$scope.schedule =
		{
			saturday: [],
			sunday: [],
			monday: [],
			tuesday: [],
			wednesday: [],
			thursday: [],
			friday: [],
			one_off: []
		};
		
		$scope.courseList = new Array;
		$scope.teachers = new Array;
		$scope.rooms = new Array;
		if( typeof luh.courses != 'undefined' ) $scope.courseList = luh.courses;
		if( typeof luh.teachers != 'undefined' ) $scope.teachers = luh.teachers;
		if( typeof luh.rooms != 'undefined' ) $scope.rooms = luh.rooms;
		$scope.sections = function(){ return sections.get() };
		
		if( typeof luh.schedule != 'undefined' ){
			angular.forEach( luh.schedule, function( e ){
				e.start_time = new Date( e.start_time );
				$scope.schedule[e.day].push(e);
			});
		}
		
		$scope.removeRow = function( day, i ){
			$scope.schedule[day].splice(i, 1);
			
			setTimeout(function(){
				jQuery('div[ng-controller="schedule"]').accordion('refresh');
			}, 5);
		}
		
		$scope.addRow = function( day, e ){
			e.stopPropagation();
			$scope.schedule[day].unshift({
				ID : 0,
				day: day
			});
			
			setTimeout(function(){
				jQuery('div[ng-controller="schedule"]').accordion('refresh');
			}, 5);
		}
		
		$scope.tagTransform = function(newTag) {
			return {
				title: newTag,
				isCustom: true
			};
		}
		
		$scope.timePickerOptions = {
			step: 30,
			timeFormat: 'g:i A',
			appendTo: 'body'
		};
	}]);
	
	editProgramApp.controller( 'settings', [ '$scope', function( $scope ){
		$scope.fees = new Array;
		
		if( typeof luh.settings.fees != 'undefined' ) $scope.fees = luh.settings.fees;
		if( typeof luh.settings.registration_open != 'undefined' ) $scope.registration_open = luh.settings.registration_open;
		
		
		$scope.removeRow = function( i ){
			$scope.fees.splice( i, 1 );
		}
		
		$scope.addRow = function( e ){
			e.stopPropagation();
			$scope.fees.push({});
		}
		
	}]);
	
	editProgramApp.factory( 'sections', function(){
		var offering = new Array;
		
		return {
			get: function(){ return offering; },
			set: function( d ){ offering = d; }
		}
	});
})();
