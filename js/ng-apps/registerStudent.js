var isOperaMini = (navigator.userAgent.indexOf('Opera Mini') > -1);
jQuery(document).ready(function($){
	if( !isOperaMini ){
		$('.datemask').inputmask({
			alias: "yyyy-mm-dd"
		});
	}
});

(function(){
	angular.element(document).ready(function() {
		angular.element('#content').css('display', 'block');
	});

	var Results = angular.module('LURegister', ['angular-loading-bar']);
	
	Results.controller('StudentInfoCtrl', ['$scope', '$http', function($scope, $http){
		
		$scope.errorData = {};
		$scope.hasError = false;
		$scope.registered = false;
		
		$scope.register = function(){
			if( !$scope.enable ){
				$scope.updateResult();
				return;
			}
			
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: 'action=register-student&student_id='+$scope.studentID+'&birth_date='+$scope.DOB+'&email='+$scope.email,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				if( true == content.success ){
					$scope.registered = true;
				}
				else {
					$scope.hasError = true;
					$scope.errorData = content;
					$scope.enable = false;
				}
			});
		}
		
		$scope.updateResult = function(){
			
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: 'action=init-student-registration&student_id='+$scope.studentID+'&birth_date='+$scope.DOB,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				if( true == content.success ){
					$scope.name = content.name;
					$scope.errorData = {};
					$scope.hasError = false;
					$scope.enable = true;
				}
				else {
					$scope.name = '';
					$scope.hasError = true;
					$scope.errorData = content;
					$scope.enable = false;
				}
			});
		}
	}]);
})();
