(function(){
	/* global angular luh */
	var Results = angular.module('LUChangePassword', ['angular-loading-bar']);
	
	Results.controller('ChangePasswordCTRL', ['$scope', '$http', '$sce', '$window', function($scope, $http, $sce, $window){
		
		$scope.errorData = {};
		$scope.hasError = false;
		$scope.done = false;
		
		$scope.change_password = function(){
			
			$http({
				url: luh.ajaxurl,
				method: 'POST',
				data: $.param({
					action: 'change-password',
					current_password: $scope.current_password,
					new_password: $scope.new_password,
					confirm_password: $scope.confirm_password,
					nonce: $scope.nonce
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				if( true == content.success ) {
					$scope.done = true;
				}
				else {
					$scope.hasError = true;
					$scope.errorData = $sce.trustAsHtml(content.errors);
				}
			});
		};
	}]);
})();
