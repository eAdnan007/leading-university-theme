// Hack to let authorised people of the department manupulate the registration.
jQuery(document).ready(function($){
	$('<input id="post_author_override" type="hidden" name="post_author_override" />')
		.val( $('#user-id').val() )
		.prependTo('form');
});

(function(){
	var editSyllabusApp = angular.module('editSyllabusApp', ['ui.select', 'ngSanitize']);
	editSyllabusApp.controller( 'courses', [ '$scope', function( $scope ){
		
		$scope.courses = luh.courses;
		
		$scope.removeCourse = function( i ){
			$scope.courses.splice( i, 1 );
		}
		
		$scope.addCourse = function( e ){
			e.stopPropagation();
			
			$scope.courses.push({});
		}
	}]);
})();