(function($){
    
    var map_styles = [
      {"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},
      {"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},
      {"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-60},{"lightness":60},{"color":"#e9e7e4"}]},
      {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},
      {"featureType":"poi","elementType":"labels.text","stylers":[{"color":"#802728"},{"visibility":"on"}]},
      {"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"on"},
      {"color":"#d5e09d"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"}]},
      {"featureType":"poi.park","elementType":"labels.text","stylers":[{"visibility":"on"},{"color":"#6f9543"}]},
      {"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#6f9543"}]},
      {"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"color":"#ff0000"}]},
      {"featureType":"poi.park","elementType":"labels.icon","stylers":[{"visibility":"simplified"}]},
      {"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},
      {"featureType":"road.highway","elementType":"geometry","stylers":[{"weight":"1.00"}]},
      {"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":"2.21"}]},
      {"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"invert_lightness":true},{"visibility":"off"}]},
      {"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"off"}]},
      {"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#a7a9ac"},
      {"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},
      {"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#fffefe"}]},
      {"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"on"}]},
      {"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"on"},
      {"color":"#d9d7d6"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},
      {"saturation":-100},{"lightness":60}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},
      {"saturation":-10},{"lightness":30}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"}]},
      {"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#7db3ba"},{"visibility":"on"}]},
      {"featureType":"water","elementType":"geometry.stroke","stylers":[{"weight":"8.21"},{"visibility":"off"}]},
      {"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#f4f3f3"}]},
      {"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"73"},{"saturation":"0"},{"gamma":"1"},
      {"color":"#cdf2f7"},{"visibility":"on"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},
      {"color":"#7db3ba"},{"weight":"3.49"},{"lightness":"0"},{"gamma":"1"}]}
    ];

        
    
    
    $(document).ready(function(){

        $('select:not(.spare)').select2();
    
        var top_spacing = 0;
        if( $('body').hasClass('admin-bar') )
            top_spacing = 32;
        
        if( $(window).width() > 767 )
            $("#top-nav").sticky({ topSpacing:top_spacing, className: 'sticky-nav hidden-print' });
        
        $(window).load(function() {
            $("#campus-map .campus").first().trigger('click');
        });
        
        $("#campus-map .campus").click(function() {
            $(this).siblings('.campus').removeClass('active');
            $(this).addClass("active");
        });
        
        $("#map").goMap({
            markers: [{
              latitude: $("#campus-map .campus").first().data('lat'),
              longitude: $("#campus-map .campus").first().data('lng'),
              id: 'dragMarker',
              html: {
                content: $("#campus-map .campus").first().find('.mapcampustitle').prop('outerHTML') +  $("#campus-map .campus").first().children('address').html(),
                popup: true
              }
            }],
            
            hideByClick: true,
            scrollwheel: false,
            maptype: 'ROADMAP',
            zoom: 17
        
        });
        
        if( typeof $.goMap.map != 'undefined' )
            $.goMap.map.setOptions({styles: map_styles});
        
        //Triggers
        var offset = 0.0005;
        
        $("#campus-map .campus").on( 'click', function(e){
            e.preventDefault();
            
            $.goMap.setMap({
                latitude: $(this).data('lat') + offset,
                longitude: $(this).data('lng')
            });
        
            $.goMap.setMarker('dragMarker', {
                latitude: $(this).data('lat'),
                longitude: $(this).data('lng')
            });
            
            $.goMap.setInfo('dragMarker', $(this).find('.mapcampustitle').prop('outerHTML') + $(this).children('address').html() );
        });
        
        $('.class_schedule').each(function(){
            
            var el = this;
            
            $(this).find('select')
            .change(function(){
                $(el).find('.calendar').fullCalendar( 'refetchEvents' )
            })
            .select2();
            
            $(this).find('.program-el').change(function(){
                $.ajax({
                    url: luh.ajaxurl,
                    dataType: 'json',
                    data: {
                        action: 'get-program-meta',
                        program_id: $(this).val()
                    },
                    success: function( d ){
                        $(el).find('.section-el').select2('destroy').html('');
                        $(el).find('.room-el').select2('destroy').html('');
                        $(el).find('.course-el').select2('destroy').html('');
                        $(el).find('.section-el').select2({data: d.sections});
                        $(el).find('.room-el').select2({data: d.rooms});
                        $(el).find('.course-el').select2({data: d.courses});
                    }
                });
            });
            
            $(el).find('.calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultView: ($(window).width() <= 768) ? 'agendaDay' : 'agendaWeek',
                allDaySlot: false,
                disableResizing: true,
                axisFormat: 'H(:mm)' + ':00',
                columnFormat: 'dddd',
                editable: false,
                droppable: false,
                timezone: 'local',
                firstDay: 6,
                minTime: '06:00:00',
                windowResize: function (view) {
                    var ww = $(window).width();
                    view = (ww <= 768) ? 'agendaDay' : 'agendaWeek';
                    var currentView = $(el).find('.calendar').fullCalendar('getView');
                    if (view != currentView) {
                        $('#calendar').fullCalendar('changeView', view);
                    }
                    if (ww <= 768) {
                        $('.fc-header-right .fc-button').hide();
                    } else {
                        $('.fc-header-right .fc-button').show();
                    }
                },
                selectable: false,
                selectHelper: false,
                events: {
                    url: luh.ajaxurl,
                    type: 'GET',
                    data: function(){
                        return {
                            action: 'get-event-feed',
                            program: $(el).find('.program-el').val(),
                            room: $(el).find('.room-el').val(),
                            section: $(el).find('.section-el').val(),
                            course: $(el).find('.course-el').val(),
                            teacher: $(el).find('.teacher-el').val(),
                            feedType: 'json'
                        }
                    },
                    error: function() {
                        alert('There was an error while fetching events!');
                    },
                    color: '#455a64',
                    textColor: 'white'
                }
            });
        });
        
        $('#navbar-container .nav .menu-item').hover(function(){
            var el = $(this).find('.sub-menu');
            
            if( el.length < 1 ) return;
            
            if( $(window).width() - el.offset().left - el.width() < 0 ){
                el.css('right', '10px');
            }
            else {
                el.css('right', 'auto');
            }
            
        });
        
        $('#navbar-container .menu-item-has-children > a').click(function(e){
            e.preventDefault();
            
            if( $(window).width() > 767 ) return;
            $(this).parent().toggleClass('active');
            $(this).siblings('.sub-menu').slideToggle();
        });

        $('#evaluation-form').on('submit', function(e){

            var focus_el = null;

            $('#evaluation-form .answer-row').each(function(){
                if(!$(this).find('.answer:checked').length){
                    $(this).addClass('invalid');

                    if(null == focus_el) focus_el = $(this);

                    e.preventDefault();
                }
                else {
                    $(this).removeClass('invalid');
                }
            });

            if(null != focus_el) $(window).scrollTop(focus_el.offset().top-50);
        });

        $('#evaluation-form .answer-row .answer').on('change', function(){
            $(this).closest('.answer-row').removeClass('invalid');
        });
    });
    
    
})(jQuery);
