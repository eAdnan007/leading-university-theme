<?php
/**
 * The sidebar containing the footer widget area.
 *
 * @package Leading University
 */

if ( ! is_active_sidebar( 'footer' ) ) {
	return;
}
?>

<footer class="footer-sidebar">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'footer' ); ?>
		</div>
	</div>
</footer>
