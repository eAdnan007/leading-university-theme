<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Leading University
 */

get_header(); ?>

	<section class="intro">
		<div class="container">
			<div class="row">
				<article class="col-md-8 col-sm-12 col-xs-12 error-404 not-found box">
					<header class="entry-header">
						<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'leading-university' ); ?></h1>
					</header><!-- .entry-header -->
				
					<div class="entry-content">
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'leading-university' ); ?></p>

						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				
				</article><!-- #post-## -->
				
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
