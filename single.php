<?php
/**
 * The template for displaying all single posts.
 *
 * @package Leading University
 */

get_header(); ?>

	<section class="intro">
		<div class="container">
			<div class="row">
				<?php while ( have_posts() ) : the_post(); ?>
			
					<?php get_template_part( 'content', 'single' ); ?>

					<?php get_sidebar(); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
