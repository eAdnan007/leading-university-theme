<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Leading University
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<?php if( ( is_single() || is_page() )  && !is_front_page() ) { ?>
	<meta name="description" content="<?php the_excerpt();?>"/>
	<?php } else { ?>
	<meta name="description" content="Leading University or LU is a private university of Bangladesh. It was established in 2001 by the Private University Act 1992. The outreach campus of LU is located in Surma & Rangmahal Tower, Sylhet." />
	<?php } ?>
	
	<meta name="keywords" content="Leading University, University, Sylhet, Bangladesh University, Leading University Sylhet">
	<meta name="author" content="DeviserWeb">
	<meta name="viewport" content="width=device-width">
	
	<!-- =========================
	      FAVICON
	============================== -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/img/favicon.ico">
	
	<!-- =========================
	      FACEBOOK OPEN GRAPH DATA
	============================== -->
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?php wp_title('-','true','right'); ?>" />
	<?php if( ( is_single() || is_page() )  && !is_front_page() ) { ?>
	<meta property="og:description" content="<?php the_excerpt();?>"/>
	<?php } else { ?>
	<meta property="og:description" content="Leading University or LU is a private university of Bangladesh. It was established in 2001 by the Private University Act 1992. The outreach campus of LU is located in Surma & Rangmahal Tower, Sylhet." />
	<?php } ?>
	<meta property="og:url" content="<?php echo get_bloginfo('url') . $_SERVER['REQUEST_URI'];?>" />
	<meta property="og:site_name" content="<?php bloginfo('name');?>" />
	<meta property="og:locale" content="en_US" />
	
	<?php if( ( is_single() || is_page() ) && has_post_thumbnail() ): ?>
	<meta property="og:image" content="<?php print_r( wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] );?>" />
	<?php else : ?>
	<meta property="og:image" content="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" />
	<?php endif; ?>
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="head">
	<div class="navbar-header pull-right">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-container">
			<span class="sr-only"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="top container">
		<div class="logo">
			<a href="<?php bloginfo('url');?>">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/logo-white.png" alt="<?php bloginfo('name'); ?>" class="brand-img img-responsive hidden-print">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" alt="<?php bloginfo('name'); ?>" class="brand-img img-responsive visible-print-inline-block">
			</a> 
			<a href="<?php bloginfo('url');?>">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/label-white.png" alt="<?php bloginfo('name'); ?>" class="site-label img-responsive hidden-print">
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/label.png" alt="<?php bloginfo('name'); ?>" class="site-label img-responsive visible-print-inline-block">
			</a> 
			<div class="clearfix"></div>
			<hr class="visible-print-block">
		</div>
	</div>
	<nav id="top-nav" class="navbar navbar-fixed hidden-print">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="navbar-container">
			<?php
			wp_nav_menu( array(
				'theme_location'  => 'primary',
				'menu'            => lu_get_menu_by_location( 'primary' ),
				'menu_class'      => 'navbar-primary nav navbar-nav',
				'container'       => 'div',
				'container_class' => 'container',
				'fallback_cb'     => false,
				'depth'           => 2 ) );
			?>
		</div><!-- navbar-collapse -->	
	</nav>
</header>
<!-- end head -->