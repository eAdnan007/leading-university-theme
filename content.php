<?php
/**
 * @package Leading University
 */
?>
<div class="col-md-8 col-sm-12 col-xs-12">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'box body' ); ?>>
		<div class="featured-image-container">
			<?php 
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				the_post_thumbnail( 'single-feat-image', array( 'class' => 'featured-image img-responsive' ) );
			} 
			?>
		</div>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	
			<div class="entry-meta">
				<?php // leading_university_posted_on(); ?>
			</div><!-- .entry-meta -->
		</header><!-- .entry-header -->
	
		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'leading-university' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	
		<footer class="entry-footer">
			<?php // leading_university_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->

	<?php
		// If comments are open or we have at least one comment, load up the comment template
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	?>
</div>
