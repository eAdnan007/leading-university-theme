<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Leading University
 */

get_header(); ?>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<?php
					$teacher = $wp_query->get_queried_object();
					?>
					<?php if ( isset( $teacher->roles ) && in_array( 'teacher', $teacher->roles ) ) : ?>
						<div class="container-fluid">
							<div class="row box teacher-profile">
								<div class="row">
									<div class="teacher-img col-sm-2 col-xs-4">
										<?php 
										if( function_exists( 'get_wp_user_avatar' ) ){
											echo get_wp_user_avatar( $teacher->ID, 'original', null, $teacher->data->display_name );
										}
										else {
											echo get_avatar( $teacher->ID, 150, null, $teacher->data->display_name );
										}
										?> 
									</div>
									<div class="teacher-full-bio col-sm-10 col-xs-8">
										<div class="row">
											<div class="col-md-7">
												<h1><?php echo $teacher->data->display_name; ?></h1>
												<p><?php 
													echo get_user_meta( $teacher->ID, 'designation', true );
													
													echo '<br>';
													
													$additional_role = trim( get_user_meta( $teacher->ID, 'additional_role', true ) );
													if( '' != $additional_role ) echo "$additional_role <br>";
													
													$department = get_user_meta( $teacher->ID, 'department', true );
													if( '' != $department && 0 != $department ){
														$department = get_term( $department, 'department' );
														if( !is_wp_error( $department ) && null != $department ){
															echo $department->name;
														}
													}
													?>
												</p>
											</div>
											<div class="col-md-5 contact-info">
												<h2>Contact Information</h2>
												<?php
												$contact_info = array(
													'cell' => __( 'Cell Phone', 'leading-university' ),
													'phone' => __( 'Phone', 'leading-university' )
													);
												
												foreach( $contact_info as $ci => $ci_name ){
													$ci_value = trim( get_user_meta( $teacher->ID, $ci, true ) );
													if( '' != $ci_value ){
														echo '<p class="contact-method '.$ci.'"><strong>'.$ci_name.':</strong> '.$ci_value.'</p>';
													}
													$ci_value = get_user_meta( $teacher->ID, $ci, true );
												}
												$ci_value = $teacher->data->user_email;
												echo '<p class="contact-method email"><strong>E-mail:</strong> '.$ci_value.'</p>';
												?>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<?php 
										$bio = get_user_meta( $teacher->ID, 'description', true );
										if( '' != trim( strip_tags( $bio ) ) ): ?>
										<div class="author-paragraph">
											<h2><?php _e( 'Biography', 'leading-university' ); ?></h2>
											<?php echo wpautop( $bio ); ?>
										</div>
										<?php endif; ?>
										<?php 
										$area_of_study = get_user_meta( $teacher->ID, 'area_of_study', true );
										if( '' != trim( strip_tags( $area_of_study ) ) ): ?>
										<div class="author-paragraph">
											<h2><?php _e( 'Area of Study', 'leading-university' ); ?></h2>
											<?php echo wpautop( $area_of_study ); ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="row">
								<?php
								$paged = get_query_var('paged') ? get_query_var('paged') : 1;
								query_posts( 
									array(
										'post_type'      => 'publication',
										'author'         => $teacher->ID,
										'post_status'    => 'publish',
										'paged'          => $paged,
										'posts_per_page' => -1
										));
								?>
								<?php if( have_posts() ):?>
								<h2><?php _e( 'Publications', 'leading-univerity' );?></h2>
								<div class="teacher-publication container-fluid box">
									<?php while( have_posts() ): the_post(); ?>
										<article class="media row">
											<?php if( has_post_thumbnail() ) : ?>
											<div class="media-left col-sm-4">
												<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'media-object img-responsive' ) ); ?>
											</div>
											<?php endif; ?>
								
											<div class="publication-content <?php echo has_post_thumbnail() ? 'col-sm-8' : 'col-sm-12';?>">
												<?php echo sprintf( '<h1 class="media-heading"><a href="%s">%s</a></h1>', get_permalink(), get_the_title() );?>
												<p><?php the_excerpt(); ?></p>
												<a href="<?php the_permalink(); ?>"><?php _e( 'Read More...', 'leading-university' ); ?></a>
											</div>
										</article>
									<?php endwhile; ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
					<?php else : ?>
				
						<?php get_template_part( 'content', 'none' ); ?>
	
					<?php endif; ?>
				</div>
				
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
