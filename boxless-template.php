<?php
/**
 * Template Name: Boxless Template
 *
 * Page template identical to regular page except it does not have box for the content.
 *
 * @package Leading University
 */

get_header(); ?>
	
	<section class="intro">
		<div class="container">
			<?php while ( have_posts() ) : the_post(); ?>
		
				<?php get_template_part( 'content', 'nobox' ); ?>
		
			<?php endwhile; // end of the loop. ?>
			<?php get_sidebar(); ?>
		</div>
	</section>

<?php get_footer(); ?>
