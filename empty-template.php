<?php
/**
 * Template Name: Empty Template
 *
 * Page template without sidebar or box effect. Useful for pagebuilders.
 *
 * @package Leading University
 */

get_header(); ?>
	
	<section class="intro">
		<div class="container">
			<?php while ( have_posts() ) : the_post(); ?>
		
				<?php the_content(); ?>
		
			<?php endwhile; // end of the loop. ?>
		</div>
	</section>

<?php get_footer(); ?>
