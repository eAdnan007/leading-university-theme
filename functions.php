<?php
/**
 * Leading University functions and definitions
 *
 * @package Leading University
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 690; /* pixels */
}

define( 'LU_THEME_VERSION', '1.3.1' );

global $dept_based_cap_post_types;
$dept_based_cap_post_types = array( 'news', 'notice', 'syllabus', 'registration', 'program' );

/**
 * Load TGM  Plugin Activation
 */
require_once get_template_directory() . '/inc/tgm.php';

/**
 * Register Custom Navigation Walker
 */
require_once get_template_directory() . '/inc/wp_bootstrap_pagination.php';

/**
 * Option panel
 */
require_once get_template_directory() . '/admin/admin-init.php';

/**
 * Result related functions and actions
 */
require_once get_template_directory() . '/inc/results.php';

/**
 * Shortcodes
 */
require_once get_template_directory() . '/inc/shortcodes.php';

/**
 * Ajax responders
 */
require_once get_template_directory() . '/inc/ajax-response.php';

/**
 * Metaboxes and there contents for all custom post types.
 */
require_once get_template_directory() . '/inc/metaboxes.php';

/**
 * Utility functions.
 */
require_once get_template_directory() . '/inc/utilities.php';

/**
 * Change the gravityform markups to match form elements with rest of the site.
 */
require_once get_template_directory() . '/inc/gravityform-branding.php';

/**
 * Post meta manipulation.
 */
require_once get_template_directory() . '/inc/post-meta.php';

/**
 * Hooks to implement backend-interface and functionality.
 */
require_once get_template_directory() . '/inc/backend-hooks.php';

/**
 * Allow remote origins to request through XHR
 */
add_filter( 'allowed_http_origin', '__return_true' );

/**
 * Custom pagination system.
 */
add_filter('wp_bootstrap_pagination_defaults', function( $args ) {
	$args['previous_string'] = '<i class="fa fa-chevron-left"></i>';
	$args['next_string'] = '<i class="fa fa-chevron-right"></i>';
	
	return $args;
});

/**
 * Make user avatars responsive.
 */
function lu_make_avatar_responsive( $class ){
	if( strpos( $class, 'img-responsive' ) ) return $class;
	
	if( isset( $GLOBALS['comment_list_started'] ) && $GLOBALS['comment_list_started'] ) return $class;

    return str_replace( 'class="', 'class="img-responsive ', $class );
}
add_filter( 'get_avatar', 'lu_make_avatar_responsive' );
add_filter( 'get_wp_user_avatar', 'lu_make_avatar_responsive' );

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', function() {
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js' );
	
	wp_enqueue_script( 'google-maps', '//maps.google.com/maps/api/js?sensor=false' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js' );
	wp_enqueue_script( 'stickyjs', get_template_directory_uri() . '/js/jquery.sticky.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-gomap', get_template_directory_uri() . '/js/jquery.gomap-1.3.1.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-animate-enhanced', get_template_directory_uri() . '/js/jquery.animate-enhanced.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'materialize', get_template_directory_uri() . '/js/materialize.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'moment', get_template_directory_uri() . '/js/moment.min.js' );
	wp_enqueue_script( 'fullcalendar', get_template_directory_uri() . '/js/fullcalendar.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'select2', get_template_directory_uri() . '/js/select2.full.js', array( 'jquery' ) );
	wp_register_script( 'lu', get_template_directory_uri() . '/js/custom.js', array( 'google-maps', 'jquery-gomap', 'stickyjs' ) );
	
	$help_data = array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
	
	if( is_page() ){
		global $post;
		$student_id = get_user_meta( get_current_user_id(), 'student_id', true );
		$program_id = get_user_meta( get_current_user_id(), 'program', true );
		
		if( has_shortcode( $post->post_content, 'lu_login_form' ) ){
			wp_enqueue_script( 'login-module', get_template_directory_uri() . '/js/ng-apps/login.js', array( 'loading-bar', 'lu' ) );
		}
		if( has_shortcode( $post->post_content, 'lu_change_password_form' ) ){
			wp_enqueue_script( 'change-password-module', get_template_directory_uri() . '/js/ng-apps/changePassword.js', array( 'loading-bar', 'lu' ) );
		}
		if( has_shortcode( $post->post_content, 'lu_semester_registration_form' ) && '' != trim( $student_id ) ){
			/**
			 * Save the registration to database if it is already posted.
			 */
			lu_save_front_registration();
			
			
			$help_data['student_id'] = $student_id;
			$help_data['offering']   = lu_get_program_offerings( get_user_meta( get_current_user_id(), 'program', true ) );
			$help_data['courses']    = lu_student_available_courses( $student_id,  lu_get_student_registration_id( $student_id ) );
			$help_data['batch_meta'] = lu_get_user_batch_meta( get_current_user_id() );
			$help_data['waiver']     = lu_get_student_waiver( $student_id );
			$help_data['credit_limit'] = (int) get_post_meta( $program_id, '_credit_limit', true );
			
			$reg = lu_get_student_registration_id( $student_id );
			if( 0 != $reg ){
				$reg = get_post( $reg );
				if( null != $reg && in_array( $reg->post_status, array( 'review', 'approved', 'registered' ) ) ){
					$help_data['freez_registration'] = true;
				}
				if( null != $reg ){
					$post_statuses = get_post_stati( array(), 'objects' );
					$help_data['registration_status'] = $post_statuses[$reg->post_status]->label;
				}
			}
			
			if( is_wp_error( lu_fetch_result( $student_id ) ) ){
				$help_data['error'] = __( 'Temporary technical problem occurred. You will not be able to make changes and displayed data may not be correct.', 'leading-university' );
				$help_data['freez_registration'] = true;
			}
			
		}
	}
	
	wp_localize_script( 'lu', 'luh', $help_data );
	wp_enqueue_script( 'lu' );
	
	wp_enqueue_script( 'angular', get_template_directory_uri() . '/js/angular.min.js', array(), '1.4.0' );
	wp_enqueue_script( 'loading-bar', get_template_directory_uri() . '/js/ng-apps/loading-bar.min.js', array( 'angular' ) );
	wp_enqueue_script( 'jquery-inputmask', get_template_directory_uri() . '/js/jquery.inputmask.bundle.js', array( 'jquery' ) );
	
	wp_register_script( 'lu-results-module', get_template_directory_uri() . '/js/ng-apps/getResults.js', array( 'angular', 'jquery', 'loading-bar', 'lu' ) );
	wp_localize_script( 'lu-results-module', 'lur', array( 'student_id' => get_user_meta( get_current_user_id(), 'student_id', true ) ) );
	wp_enqueue_script( 'lu-results-module' );
	
	wp_enqueue_script( 'registration-module', get_template_directory_uri() . '/js/ng-apps/registerStudent.js', array( 'loading-bar', 'lu' ) );
	
	wp_enqueue_script( 'edit-registration', get_template_directory_uri() . '/js/ng-apps/editRegistration.js', array( 'angular', 'lu' ), '1.0.0' );
	
	
	wp_enqueue_style( 'roboto-font', '//fonts.googleapis.com/css?family=Roboto:500,400,700' );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'fullcalendar', get_stylesheet_directory_uri() . '/css/fullcalendar.min.css' );
	wp_enqueue_style( 'fullcalendar-print', get_stylesheet_directory_uri() . '/css/fullcalendar.print.css', array(), '2.3.2', 'print' );
	wp_enqueue_style( 'select2', get_stylesheet_directory_uri() . '/css/select2.legacy.min.css' );
	wp_enqueue_style( 'loading-bar', get_stylesheet_directory_uri() . '/css/loading-bar.min.css' );
	wp_enqueue_style( 'leading-university-style', get_stylesheet_uri(), array(), LU_THEME_VERSION );
	
});
