<form role="search" method="get" class="search-form" action="<?php bloginfo('url'); ?>">
	<div class="input-field">
		<input type="search" class="search-field" value="" name="s" title="<?php _e( 'Search for', 'leading-university' ); ?>">
		<label><?php _e( 'Search for:', 'leading-university' ); ?></label>
	</div>
	<button type="submit" class="btn btn-custom waves-effect waves-light"><?php _e( 'Search', 'leading-university' ); ?></button>
</form>